<?php

namespace BSSystem\LIBMateri\Repositories;

use BSSystem\LIBMateri\Models\Materi;
use BSSystem\MODEcourse\Models\Ecourse;
use BSSystem\Core\Base\BaseRepository;

class MateriRepository extends BaseRepository
{
    public function __construct(Materi $model)
    {
        $this->model = $model;
    }

    public function getEcourse(){
        return Ecourse::all();
    }
}