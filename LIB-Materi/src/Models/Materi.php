<?php

namespace BSSystem\LIBMateri\Models;

use Illuminate\Database\Eloquent\Model;

class Materi extends Model
{
    /**
     * The table associated with the model.
     *
     * @var string
     */
    protected $table = 'materi';
    
    /**
     * The attributes that aren't mass assignable.
     *
     * @var array
     */
    protected $guarded = ['id','created_at','updated_at'];

    public function ecourse()
    {
        return $this->belongsTo('BSSystem\MODEcourse\Models\Ecourse');
    }

    public function modul()
    {
        return $this->hasMany('BSSystem\LIBModul\Models\Modul');
    }
    // public function modul()
    // {
    //     return $this->hasMany('BSSystem\LIBMateri\Models\Materi');
    // }
}