<?php

namespace BSSystem\LIBCdn\Facades;

class CDNStorage extends \Illuminate\Support\Facades\Storage {
    
    protected static function getFacadeAccessor()
    {
        return 'CDNStorage';
    }
}