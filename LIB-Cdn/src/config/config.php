<?php
/*
 * ENV Var :
 *  
 * CDN_DEFAULT_DISK = sftp / local
 * 
 * CDN_URL = 'http://{{FILEGROUP}}.cdn.billionairestore.co.id' --> boleh ada {{FILEGROUP}}
 * 
 * CDN_LOCAL_PATH = local path --> boleh ada {{FILEGROUP}}
 * 
 * CDN_ASSETS_URL = URL Asset
 * 
 * CDN_SFTP_DEFAULT_HOST
 * CDN_SFTP_DEFAULT_USERNAME
 * CDN_SFTP_DEFAULT_PASSWORD
 * 
 * CDN_SFTP_BS_HOST
 * CDN_SFTP_BS_USERNAME
 * CDN_SFTP_BS_PASSWORD
 * 
 * CDN_SFTP_AC_HOST
 * CDN_SFTP_AC_USERNAME
 * CDN_SFTP_AC_PASSWORD
 * 
 * CDN_SFTP_PORTAL_HOST
 * CDN_SFTP_PORTAL_USERNAME
 * CDN_SFTP_PORTAL_PASSWORD
 * 
 * CDN_SFTP_ECOURSE_HOST
 * CDN_SFTP_ECOURSE_USERNAME
 * CDN_SFTP_ECOURSE_PASSWORD
 * 
 */

$__config_CDN['CDN_URL'] = env('CDN_URL', 'http://{{FILEGROUP}}.cdn.billionairestore.co.id');
$__config_CDN['CDN_LOCAL_PATH'] = env('CDN_LOCAL_PATH', base_path('').DIRECTORY_SEPARATOR.'..'.DIRECTORY_SEPARATOR.'CDN'.DIRECTORY_SEPARATOR.'upload'.DIRECTORY_SEPARATOR.'{{FILEGROUP}}');

return [
    'cdn_url' => $__config_CDN['CDN_URL'],
    'asset_url' => env('CDN_ASSETS_URL', 'http://assets.cdn.billionairestore.co.id/'),
    
    'file_group' => [
        //Account Center CDN
        'ac' => [
            //cdn_url akan mengikuti format cdn_url global, jika berbeda maka bisa langsung set disini
            //cdn_url' => 'http://ac.cdn.billionairestore.co.id',
            'sftp' => [
                'driver' => 'sftp',
                'host' => env('CDN_SFTP_AC_HOST', 'ac.cdn.billionairestore.co.id'),
                'username' => env('CDN_SFTP_AC_USERNAME', 'your-username'),
                'password' => env('CDN_SFTP_AC_PASSWORD', 'your-password'),

                // Settings for SSH key based authentication...
                // 'privateKey' => '/path/to/privateKey',
                // 'password' => 'encryption-password',

                // Optional SFTP Settings...
                // 'port' => 22,
                // 'root' => '',
                // 'timeout' => 30,
            ]
        ],
        //Market Place CDN
        'bs' => [
            //'cdn_url' => 'http://bs.cdn.billionairestore.co.id',
            'sftp' => [
                'driver' => 'sftp',
                'host' => env('CDN_SFTP_BS_HOST', 'bs.cdn.billionairestore.co.id'),
                'username' => env('CDN_SFTP_BS_USERNAME', 'your-username'),
                'password' => env('CDN_SFTP_BS_PASSWORD', 'your-password'),
            ]
        ],
        //portal CDN
        'portal' => [
            //'cdn_url' => 'http://portal.cdn.billionairestore.co.id',
            'sftp' => [
                'driver' => 'sftp',
                'host' => env('CDN_SFTP_PORTAL_HOST', 'portal.cdn.billionairestore.co.id'),
                'username' => env('CDN_SFTP_PORTAL_USERNAME', 'your-username'),
                'password' => env('CDN_SFTP_PORTAL_PASSWORD', 'your-password'),
            ]
        ],
        //ecourse CDN
        'ecourse' => [
            //'cdn_url' => 'http://ecourse.cdn.billionairestore.co.id',
            'sftp' => [
                'driver' => 'sftp',
                'host' => env('CDN_SFTP_ECOURSE_HOST', 'ecourse.cdn.billionairestore.co.id'),
                'username' => env('CDN_SFTP_ECOURSE_USERNAME', 'your-username'),
                'password' => env('CDN_SFTP_ECOURSE_PASSWORD', 'your-password'),
            ]
        ]
    ],
    
    'disk_default' => env('CDN_DEFAULT_DISK', 'sftp'),
    'disks' => [
        
        'local' => [
            'driver' => 'local',
            'root' => $__config_CDN['CDN_LOCAL_PATH'],
            'url' => $__config_CDN['CDN_URL'],
        ],

        'public' => [
            'driver' => 'local',
            'root' => $__config_CDN['CDN_LOCAL_PATH'],
            'url' => $__config_CDN['CDN_URL'],
            'visibility' => 'public',
        ],
        
        'sftp' => [
            'driver' => 'sftp',
            'host' => env('CDN_SFTP_DEFAULT_HOST', 'cdn.billionairestore.co.id'),
            'username' => env('CDN_SFTP_DEFAULT_USERNAME', 'your-username'),
            'password' => env('CDN_SFTP_DEFAULT_PASSWORD', 'your-password'),

            // Settings for SSH key based authentication...
            // 'privateKey' => '/path/to/privateKey',
            // 'password' => 'encryption-password',

            // Optional SFTP Settings...
            // 'port' => 22,
            // 'root' => '',
            // 'timeout' => 30,
        ]
    ]
];