<?php

if (!function_exists('cdn_asset')) {
    
    function cdn_asset($filename)
    {
        return config('bssystem.cdn.asset_url').$filename;
 
    }
}