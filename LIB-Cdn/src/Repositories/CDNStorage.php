<?php

namespace BSSystem\LIBCdn\Repositories;

class CDNStorage extends \Illuminate\Filesystem\FilesystemManager
{
    private $file_group = 'bs';//current file group
    
    public $cdn_upload_url = '';
    public $cdn_api_url = '';
    public $cdn_assets_url = '';

    /**
     * @OVERRIDE
     * Create a new filesystem manager instance.
     *
     * @param \Illuminate\Contracts\Foundation\Application $app
     * @return void
     */
    public function __construct($app,$fileGroup)
    {
        $this->app = $app;
        
        $this->file_group = $fileGroup;
        $this->cdn_upload_url = str_replace('{{FILEGROUP}}', $fileGroup, config('bssystem.cdn.cdn_url'));
        $this->cdn_api_url = $this->cdn_upload_url . '/api';
    }      
    public function url($filename,$size=false)
    {
        $filename = ltrim($filename,'/');
        //jika gambar dan mencantumkan size
        if($size && strpos($filename,'images/')===0){            
            $filename = $this->processImageSizePath($filename,$size);
        }
        return parent::url($filename);
    }
        
    /**
     * generate/format image url menggunakan size
     * 
     * @param string $filename
     * @param string $size
     * @return string
     */
    private function processImageSizePath($filename,$size)
    {
        $pathInfo = pathinfo($filename);
        $filename = $pathInfo;        
        return $filename['dirname'].'/'.$filename['filename'].'/'.$filename['filename'].'_'.$size.'.'.$filename['extension'];
    }
    /**
     * 
     * @return string group file yg digunakan saat ini di CDNStorage
     */
    public function getFileGroup()
    {
        return $this->file_group;
    }
    
    /**
     * @OVERRIDE
     * Get the filesystem connection configuration.
     *
     * @param  string  $name
     * @return array
     */
    protected function getConfig($name)
    {
        //load config per file group terlebih dahulu jika ada, jika tidak ada gunakan global config
        if(isset($this->app['config']["bssystem.cdn.file_group.{$this->file_group}.{$name}"])){
            $config = $this->app['config']["bssystem.cdn.file_group.{$this->file_group}.{$name}"];
        }else{
            $config = $this->app['config']["bssystem.cdn.disks.{$name}"];
        }        
        
        //jika di local maka ubah file group nya menyesuakan dengan file group yg online
        if($config['driver']=='local'){
            $config['root'] = str_replace('{{FILEGROUP}}', $this->file_group, $config['root']);
            $config['url'] = str_replace('{{FILEGROUP}}', $this->file_group, $config['url']);
        }
        return $config;
    }    
    
    /**
     * @OVERRIDE
     * Get the default driver name.
     *
     * @return string
     */
    public function getDefaultDriver()
    {
        return $this->app['config']['bssystem.cdn.disk_default'];
    }


}
