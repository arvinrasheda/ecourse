<?php

namespace BSSystem\LIBCdn\Providers;

use Illuminate\Support\ServiceProvider;
use Illuminate\Foundation\AliasLoader;
use BSSystem\LIBCdn\Repositories\CDNStorage;

class AppServiceProvider extends ServiceProvider
{
    /**
     * Bootstrap any application services.
     *
     * @return void
     */
    public function boot()
    {
        $this->app->bind('CDNStorage',function($app){
            $cdn = new CDNStorage($app,config('bssystem.cur_apps.apps_code'));
            return $cdn;
         });
    }

    /**
     * Register any application services.
     *
     * @return void
     */
    public function register()
    {             
        $this->app->booting(function() {
            $loader = AliasLoader::getInstance();
            $loader->alias('CDNStorage', \BSSystem\LIBCdn\Facades\CDNStorage::class);
        });
        
        $this->loadConfig();
        require_once __DIR__. '/../Helpers/Helper.php';
    }
    
    private function loadConfig()
    {
        
        /** @var \Illuminate\Config\Repository $config */
        $config = $this->app['config'];
        
        //jika config sudah diset di aplikasi
        if(isset($config['bssystem']['cdn'])){
            
            $config1 = require __DIR__ . '/../config/config.php';
            $config2 = $config['bssystem']['cdn'];
            $config->set('bssystem.cdn', array_merge($config1,$config2));          
        }else{     
            $config->set('bssystem.cdn', require __DIR__ . '/../config/config.php');
            
        }
    }
}
