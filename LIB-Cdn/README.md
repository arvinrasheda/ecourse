# BS System - LIB-Cdn

BSSYSTEM - LIB-Cdn

## .env Variable

 * `CDN_DEFAULT_DISK = "sftp"` *hanya bisa local dan sftp, untuk dionline gunakan sftp*
 * `CDN_URL = 'http://{{FILEGROUP}}.cdn.billionairestore.co.id'` *boleh ada {{FILEGROUP}}*
 * `CDN_LOCAL_PATH = local path`  *boleh ada {{FILEGROUP}}*
 * `CDN_ASSETS_URL = ""` *URL Asset*
 * `CDN_SFTP_DEFAULT_HOST = ""`
 * `CDN_SFTP_DEFAULT_USERNAME = ""`
 * `CDN_SFTP_DEFAULT_PASSWORD = ""`
 * `CDN_SFTP_BS_HOST = ""`
 * `CDN_SFTP_BS_USERNAME = ""`
 * `CDN_SFTP_BS_PASSWORD = ""`
 * `CDN_SFTP_AC_HOST = ""`
 * `CDN_SFTP_AC_USERNAME = ""`
 * `CDN_SFTP_AC_PASSWORD = ""`
 * `CDN_SFTP_PORTAL_HOST = ""`
 * `CDN_SFTP_PORTAL_USERNAME = ""`
 * `CDN_SFTP_PORTAL_PASSWORD = ""`
 * `CDN_SFTP_ECOURSE_HOST = ""`
 * `CDN_SFTP_ECOURSE_USERNAME = ""`
 * `CDN_SFTP_ECOURSE_PASSWORD = ""`
