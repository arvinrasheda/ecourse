<?php

namespace BSSystem\LIBAccount\Notifications;

use Illuminate\Bus\Queueable;
use Illuminate\Notifications\Notification;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Notifications\Messages\MailMessage;

class NotifCommissionPaid extends Notification
{
    use Queueable;
    protected $commission;
    /**
     * Create a new notification instance.
     *
     * @return void
     */
    public function __construct($commission)
    {
        return $this->commission = $commission;
        $this->connection = config('bssystem.queue_connection_ac');
    }

    /**
     * Get the notification's delivery channels.
     *
     * @param  mixed  $notifiable
     * @return array
     */
    public function via($notifiable)
    {
        return ['mail','database'];
    }

    /**
     * Get the mail representation of the notification.
     *
     * @param  mixed  $notifiable
     * @return \Illuminate\Notifications\Messages\MailMessage
     */
    public function toMail($notifiable)
    {
        return (new CommissionPaid($this->commission))->to($this->user->email);
    }
    
    public function toDatabase($notifiable)
    {
        return [
            //
        ];
    }

    /**
     * Get the array representation of the notification.
     *
     * @param  mixed  $notifiable
     * @return array
     */
    public function toArray($notifiable)
    {
        return [
            //
        ];
    }
}
