<?php

namespace BSSystem\LIBAccount\Notifications;

use Illuminate\Bus\Queueable;
use Illuminate\Notifications\Notification;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Notifications\Messages\MailMessage;

use BSSystem\LIBAccount\Mail\Invoice as MailInvoice;

/**
 * new invoice created
 */
class Invoice extends Notification implements ShouldQueue
{
    use Queueable;
    
    protected $invoice;

    /**
     * Create a new notification instance.
     *
     * @return void
     */
    public function __construct($invoice)
    {
        $this->invoice = $invoice;
        $this->connection = config('bssystem.queue_connection_ac');
    }

    /**
     * Get the notification's delivery channels.
     *
     * @param  mixed  $notifiable
     * @return array
     */
    public function via($notifiable)
    {
        return ['mail','database'];
    }

    /**
     * Get the mail representation of the notification.
     *
     * @param  mixed  $notifiable
     * @return \Illuminate\Notifications\Messages\MailMessage
     */
    public function toMail($notifiable)
    {
        $email = new MailInvoice($this->invoice);
        return $email->to($notifiable->email);
    }
    
    public function toDatabase($notifiable)
    {
        return [
            'id_si_user' => $notifiable->id,
            'nama_si_user' => $notifiable->name,
            'invoice' => $this->invoice,
            'pertaeunan' => 'nya lah bebas'
        ];
    }


    /**
     * Get the array representation of the notification.
     *
     * @param  mixed  $notifiable
     * @return array
     */
    public function toArray($notifiable)
    {
        return [
            'id_si_user' => $notifiable->id,
            'nama_si_user' => $notifiable->name,
            'invoice' => $this->invoice,
            'pertaeunan' => 'nya lah bebas'
        ];
    }
}
