<?php

namespace BSSystem\LIBAccount\Repositories;

use BSSystem\LIBAccount\Models\User;
use BSSystem\LIBAccount\Models\AppsClient;//apps
use BSSystem\LIBAccount\Models\ApiToken;

trait ApiTokenTraits
{
    
    public function tokenType()
    {
        return $this->tokenType == 'apps'?'apps':'user';
    }
    
    public function isTokenValid($tokenData)
    {        
        if(is_array($tokenData)){
            $lastUpdate = $tokenData['updated_at'];
        }else{
            $tokenData = $this->getToken($tokenData);
            if(!$tokenData){
                $this->error = 'Token Not Found.';
                return false;
            }
            $lastUpdate = $tokenData['updated_at'];
        }
        
        //jika sudah melebihi batas waktu
        if(now() > now()->addHours(config('bssystem.sso.session_lifetime'))){
            $this->deleteToken($tokenData['api_token']);
            return false;
        }
        
        return true;
    }
    /**
     * 
     * @param type $dataId
     * @return type
     */
    public function getToken($token)
    {        
        $apiToken = ApiToken::where('api_token',$token)->first();
        if(!$apiToken){
            $this->error = 'Token Not Found.';
            return false;
        }
        return $apiToken->toArray();
    }
    
    public function getUserTokenBySSOId($ssoId)
    {        
        $apiToken = ApiToken::where('is_apps_token',0)->where('sso_id',$ssoId)->first();
        if(!$apiToken){
            $this->error = 'Token Not Found.';
            return false;
        }
        return $apiToken->toArray();
    }
    
    /**
     * 
     * @param type $dataId
     */
    public function generateToken($dataId,$ssoId=0,$isMobileAppsToken=0)
    {
        $data['api_token'] = bcrypt('token'.$this->tokenType().$dataId.'.'.now());
        $data['is_valid'] = 1;
        $data['sso_id'] = $ssoId;
        $data['session_id'] = session()->exists('_token')?session()->getId():'';
        $data['is_mobileapps_token'] = $isMobileAppsToken;
        if($this->tokenType()=='apps'){
            $data['is_apps_token'] = 1;
            $data['user_id'] = 0;
            $data['apps_id'] = $dataId;
        }else{
            $data['is_apps_token'] = 0;
            $data['user_id'] = $dataId;
            $data['apps_id'] = 0;
        }        
        $apiTokenData = ApiToken::create($data);
        return $apiTokenData->toArray();
    }
    
    /** 
     * update sso id dari token tertentu
     * 
     * @param string $token token yang akan didelete
     * @param bigint $token token yang akan didelete
     */     
    public function updateTokenSSOid($token,$id)
    {
        ApiToken::where('api_token',$token)->update(['sso_id'=>$id]);
        return true;
    }
    /**
     * ubah session id di table api token berdasarkan sso id yg diinputkan
     * 
     * @param type $ssoId
     * @param type $sessionId
     */
    public function updateSSOSessionId($ssoId,$sessionId=false)
    {
        if(!$sessionId)$sessionId=session()->getId();
        ApiToken::where('sso_id',$ssoId)->update(['session_id'=>$sessionId]);
        return true;
    }
    /**
     * update updated_at
     * 
     * @param type $ssoId
     * @param type $dateTime
     * @return boolean
     */
    public function setTokenLastUpdate($ssoId,$dateTime)
    {
        ApiToken::where('sso_id',$ssoId)->update(['updated_at'=>$dateTime]);
        return true;
    }
    public function replaceTokenSSOid($ssoIdOld,$ssoIdnew)
    {
        ApiToken::where('sso_id',$ssoIdOld)->update(['sso_id'=>$ssoIdnew]);
        return true;
    }
    /** 
     * delete api token
     * 
     * @param strung $token token yang akan didelete
     */     
    public function deleteToken($token)
    {        
        ApiToken::where('api_token',$token)->delete();
        return true;
    }
    
    
    /** 
     * delete api token
     * 
     * @param strung $token token yang akan didelete
     */     
    public function deleteUserTokenBySSOId($ssoId)
    {
        ApiToken::where('sso_id',$ssoId)->where('is_apps_token',0)->delete();
        return true;
    }
}