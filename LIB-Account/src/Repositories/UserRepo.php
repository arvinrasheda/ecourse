<?php

namespace BSSystem\LIBAccount\Repositories;

use Illuminate\Support\Facades\DB;

use Facades\BSSystem\LIBAccount\Repositories\AppsRepo;
use Facades\BSSystem\LIBAccount\Repositories\UserLogRepo;
use Carbon\Carbon;
//use semua model yg diperlukan
use BSSystem\LIBAccount\Models\User;
use BSSystem\LIBAccount\Models\UserProfile;
use BSSystem\LIBAccount\Models\UserUpdateQueue;
use BSSystem\LIBAccount\Models\UserApp;
use BSSystem\LIBAccount\Models\UserRole;
use BSSystem\LIBAccount\Models\Role;
use BSSystem\LIBAccount\Models\ApiToken;

use Validator;

use BSSystem\Core\Base\BaseRepository;

class UserRepo extends BaseRepository
{
    use ApiTokenTraits,UserMessageTraits;
    
    public $error = '';
    
    protected $tokenType = 'users';//posisi library

    public function __construct(User $model)
    {
        $this->model = $model;        
    }   
    
    public function error()
    {
        return $this->error;
    }
        /**
     * 
     * @param string $key
     * @param type $value
     * @return boolean : true jika ada, false jiak tidak ada
     */
    public function isUserExist($key,$value=NULL)
    {
        //jika tidak menyertakan value berarti default nya by apps code
        if(is_null($value)){
            $value = $key;
            $key = 'id';
        }
        
        return $this->model->where($key, $value)->exists();
    }

    public function activateUser($user_id)
    {
        $userData = $this->model->find($user_id);
        
        if(!$userData){
            $this->error = 'User not found.';
            return false;
        }
        
        UserLogRepo::addActivityLog($user_id,'activate');
        
        return $userData->update(['status'=>1]);
    }
    /**
     * Digunakan untuk ambil 1 record user berbentuk model elequent, biasanya
     * digunakan untuk keperluan fitur-fitur yg berkaitan dengan user model
     * 
     * @param type $user_id
     * @return type
     */
    public function getUserModel($user_id)
    {
        return $this->model->find($user_id);
    }
    
    public function getOneBySocnetId($id, $provide)
    {
        $userData = User::where('socialauth_'.$provider.'_id',$id)
            ->first();
        
        if(!$userData)return false;
        return $userData->toArray();
    }
    
    public function getOneBySocnetIdOrEmail($id,$email,$provider)
    {
        $userData = User::where('email', $email)
            ->orWhere('socialauth_'.$provider.'_id',$id)
            ->first();
        
        if(!$userData)return false;
        return $userData->toArray();
    }
    
    /**
     * rigistrasi user baru
     * 
     * @param array $userData : seluruh field di table user dan :
     *      role_id
     *      apps_id
     *      has_auth_grant
     * @param boolean $generateToken 1 jika generate token, 0 jika tidak
     * 
     * @return array : seluruh field di table user dan :
     *      token : api_token dari table api_tokens yg digenerate saat registrasi
     *      token_id : id dari table api_tokens nhya
     *      
     */
    public function register(Array $userData,$generateToken=true)
    {
        $userData = $this->registerFilter($userData);
        
        if(!$userData){
            return false;
        }
        $roleData = Role::find($userData['role_id']);        
        if(!$roleData){
            $this->error = 'Role tidak terdaftar';
            return false;
        }
        $userData['password']=bcrypt($userData['password']);
                
        //refferal code aplikasi
        $userData['reff_code'] = 'APPS'.$this->nextUserId();
        $userData['user_idcode'] = $this->generateUserIdcode();
        $userData['role'] = ';'.$roleData['id'].':'.$roleData['role_code'].';';
                
        $data = $this->model->create($userData);
        $data = $data->toArray();
        
        if($generateToken){
            $apiTokenData = $this->generateToken($data['id']);
            $data['token'] = $apiTokenData['api_token'];
            $data['token_id'] = $apiTokenData['id'];
        }
        
        $profileData = $data;
        $profileData['user_id'] = $data['id'];
        unset($profileData['id']);
        
        $this->registerProfile($profileData);
                           
        UserRole::create([
            'user_id' => $data['id'],
            'role_id' => $userData['role_id'],
            'is_main_role' => 1,
            'has_auth_grant' => 0
        ]);
        
        AppsRepo::addUserToApps($data['id'],$userData['apps_id']);        
        
        return $data;
    }
    
    public function registerFilter(Array $userData)
    {
        $validatorRule = [
            'name' => 'required|min:5|max:255',
        ];
        if(isset($userData['password'])){
            $validatorRule['password'] = 'required|min:5|max:255';
        }
        if(isset($userData['email'])){
            $validatorRule['email'] = 'required|email|min:5|max:255';
        }
        $validator = Validator::make($userData, $validatorRule);
        
        if ($validator->fails()) {
            $errors = $validator->errors();
            $err[] = '<ul>';
            foreach ($errors->all() as $message) {
                $err[] = '<li>'.$message.'</li>';
            }
            $err[] = '</ul>';
            $this->error = implode('', $err);
            return false;
        }        
        
        //jika tidak menyertakan apps_id
        if(!isset($userData['apps_id'])){
            if(config('cur_apps.id')){
                $userData['apps_id'] = config('cur_apps.id');
            }else{
                $userData['apps_id'] = config('bssystem.default_apps.id');
            }
        }
        
        //jika tidak menyertakan role_id maka set default
        if(!isset($userData['role_id'])){
            if(config('cur_apps.default_role')){
                 $userData['role_id'] = config('cur_apps.default_role');
             }else{
                 $userData['role_id'] = config('bssystem.default_apps.default_role');
             }
        }
        
        //pastikan rule nya ada
        if(!Role::where('id',$userData['role_id'])->exists()){
            $this->error = 'Role not defined.';
            return false;
        }
        
        if(isset($userData['email']) && $this->isEmailRegistered($userData['email'])){
            $this->error = 'Email already registered.';
            return false;
        }
        
        if(isset($userData['phone']) && $this->isPhoneRegistered($userData['phone'])){
            $this->error = 'Phone already registered.';
            return false;
        }
        return $userData;
    }
    
    /**
     * 
     * @param array $userData gabungan data users & user_profile
     */
    public function registerProfile(Array $userData)
    {
        $userData = $this->registerProfileFilter($userData);
        
        if(!$userData){
            return false;
        }
        
        if(isset($userData['name'])){            
            $name = explode(' ', $userData['name']);
            $count = count($name); 
            $userData['last_name'] = $name[$count-1];           
            if($count==1){
                $userData['last_name'] = '';
                $userData['first_name'] = $name[0];
            }else{
                unset($name[$count-1]);         
                $userData['first_name'] = implode(' ', $name);
                unset($userData['name']);
            }
        }
        
        UserProfile::create($userData);
    }
    
    public function registerProfileFilter(Array $userData)
    {
        $validatorRule = [
            'user_id' => 'required',
        ];
        if(isset($userData['email2'])){
            $validatorRule['email2'] = 'email|min:5|max:255';
        }
        $validator = Validator::make($userData, $validatorRule);
        
        if ($validator->fails()) {
            $errors = $validator->errors();
            $err[] = '<ul>';
            foreach ($errors->all() as $message) {
                $err[] = '<li>'.$message.'</li>';
            }
            $err[] = '</ul>';
            $this->error = implode('', $err);
            return false;
        }
        
        if(isset($userData['email2']) && $this->isEmailRegistered($userData['email2'])){
            $this->error = 'Email already registered.';
            return false;
        }
        
        if(isset($userData['phone2']) && $this->isPhoneRegistered($userData['phone2'])){
            $this->error = 'Phone already registered.';
            return false;
        }
        return $userData;
    }
    /**
     * cek apakah email sudah terdaftar sebelumnya
     * 
     * @param type $email
     * @param type $except
     * @return boolean
     */
    public function isEmailRegistered($email,$except_user_id=false)
    {
        $user = User::where('email',$email);
        
        if($except_user_id){
            $user = $user->where('id','!=',$except_user_id);
        }
        
        if($user->exists()){
            return true;
        }
        
        $userProfile = UserProfile::where('email2',$email);
        
        if($except_user_id){
            $userProfile = $userProfile->where('user_id','!=',$except_user_id);
        }
        
        if($userProfile->exists()){
            return true;
        }
        
        return false;
    }
    /**
     * cek apakah email sudah terdaftar sebelumnya
     * 
     * @param string $phone
     * @param type $except
     * @return boolean
     */
    public function isPhoneRegistered($phone,$except_user_id=false)
    {
        $user = User::where('phone',$phone);
        
        if($except_user_id){
            $user = $user->where('id','!=',$except_user_id);
        }
        
        if($user->exists()){
            return true;
        }
        
        $userProfile = UserProfile::where('phone2',$phone);
        
        if($except_user_id){
            $userProfile = $userProfile->where('user_id','!=',$except_user_id);
        }
        
        if($userProfile->exists()){
            return true;
        }
        
        return false;
    }
    
    /**
     * generate perkiraan user id selanjutanya
     * 
     * @return int perkiraan user id selanjutnya
     */
    public function nextUserId()
    {
        $nextId = $this->model->select('id')->orderBy('id','DESC')->first()->toArray();
        $nextId = $nextId['id'] + 1;
        return $nextId;
    }
    /**
     * generate user_idcode
     * format YYYYMMDD[USER_ID][KARAKTER 0 HINGGA 8 DIGIT][NO URUT PENDAFTARAN HARI INI]
     * 
     * @return int perkiraan user_idcode
     */
    public function generateUserIdcode()
    {
        $count = $this->model->whereDate('created_at', '=', Carbon::today()->toDateString())->count()+1;
        $nextId = $this->nextUserId();
        
        $zero = str_repeat('0',8-strlen($count.$nextId));
        
        $idcode = Carbon::today()->format('Ymd').$nextId.$zero.$count;
        return $idcode;
    }

    public function getAll() { 
        // DB::enableQueryLog();
        $data = User::all();
        // $query = DB::getQueryLog();
        // dd($query);
        // dd($data->toJSON());
        return $data;
    }    
        
}