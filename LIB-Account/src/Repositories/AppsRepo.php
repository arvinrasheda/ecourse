<?php

namespace BSSystem\LIBAccount\Repositories;

use BSSystem\LIBAccount\Models\AppsClient;//apps
use BSSystem\LIBAccount\Models\AppsTrack;
use BSSystem\LIBAccount\Models\UserApp;
use BSSystem\LIBAccount\Models\ApiToken;

use Facades\BSSystem\LIBAccount\Repositories\UserRepo;
use Facades\BSSystem\LIBAccount\Repositories\UserLogRepo;

use BSSystem\Core\Base\BaseRepository;

class AppsRepo extends BaseRepository
{
    use ApiTokenTraits;
    
    public $error = '';
    
    protected $tokenType = 'apps';//posisi library

    public function __construct(AppsClient $model)
    {
        $this->model = $model;
        
    }
    
    public function UserApp()
    {
        if(!isset($this->UserApp)) $this->UserApp = new UserApp();
        return $this->UserApp;            
        
    }
    
    public function AppsTrack()
    {
        if(!isset($this->AppsTrack)) $this->AppsTrack = AppsTrack::on($this->connection);
        return $this->AppsTrack;            
        
    }
    
    public function ApiToken()
    {
        if(!isset($this->ApiToken)) $this->ApiToken = ApiToken::on($this->connection);
        return $this->ApiToken;
    }
    
    /*
     * =========================================================================
     */
    
    /*
     * daftarkan user ke apps
     */
    public function addUserToApps($user_id,$apps_id)
    {
        if(!(UserRepo::isUserExist($user_id) && $this->isAppsExist($apps_id))){
            return false;
        }
        
        $return = UserApp::create([
            'user_id'=>$user_id,
            'apps_id'=>$apps_id
        ]);
        
        $appsData = $this->getApps($apps_id);
        UserLogRepo::addActivityLog($user_id,'added-to-apps',['id'=>$apps_id,'name' => $appsData['name'],'apps_code'=> $appsData['apps_code']]);
        
        return $return;
        
    }
    
    /**
     * check apps apakah ada atau tidak, jika ada akan direturn data apps nya
     * 
     * @param type $apps_code
     * @param type $value
     * 
     * @return array : record apps nya
     */
    public function getApps($key,$value=NULL)
    {
        //jika tidak menyertakan value berarti default nya by apps code
        if(is_null($value)){
            $value = $key;
            $key = 'id';
        }
        
        $apps = $this->model->where($key, $value)->first();
        
        if($apps)return $apps->toArray();
        
        return false;
    }
        
    /**
     * cek apakah user tertentu bagian dari apps
     */
    public function isUserOnApps($userId,$appsId)
    {        
        return UserApp::where('user_id',$userId)->where('apps_id',$appsId)->exists();
    }
    
    /**
     * 
     * @param string $key
     * @param type $value
     * @return boolean : true jika ada, false jiak tidak ada
     */
    public function isAppsExist($key,$value=NULL)
    {
        //jika tidak menyertakan value berarti default nya by apps code
        if(is_null($value)){
            $value = $key;
            $key = 'id';
        }
        
        return $this->model->where($key, $value)->exists();
    }
    /**
     * list seluruh apps yang user terdaftar didalamnya
     * @param type $param
     */
    public function getUserApps($user_id)
    {
        $userAppData = UserApp::where('user_id',$user_id)->get();
        if(!$userAppData)return false;
        foreach ($userAppData as $value) {
            $return[] = $value->apps->toArray();
        }
        return $return;
    }
    
}