<?php

namespace BSSystem\LIBAccount\Repositories;

use BSSystem\LIBAccount\Models\Session;

use BSSystem\Core\Base\BaseRepository;

class SessionRepo extends BaseRepository
{
    public $error = '';
    
    public function __construct(Session $model)
    {        
        $this->model = $model;        
    }
    
    /**
     * update last_activity timestamp ke waktu saat ini.
     * @param type $sessionId
     */
    public function updateLastActivity($sessionId)
    {
        $session = Session::find($sessionId);
        if($session){
            $session->update(['last_activity' => now()->getTimestamp()]);
        }
    }
}