<?php
namespace BSSystem\LIBAccount\Repositories;

use Illuminate\Notifications\Notification;
use BSSystem\LIBAccount\Models\User;

trait UserMessageTraits 
{
    public function sendEmail($userId,$email)
    {        
        $user = User::find($userId);
        if($user)return false;
        Mail::to($user->email)->send($email);
        return true;
    }
    
    public function sendVerificationEmail($userId)
    {
        $user = User::find($userId);
        if($user)return false;
        
        $email = new \BSSystem\LIBAccount\Mail\EmailVerification($user->toArray()); 
        Mail::to($user->email)->send($email);
        return true;
    }
    
    
    /**
     * send notifkasi 
     * 
     * @param type $userId
     * @param Notification $notification
     * @return type
     */
    public function notify($userId,Notification $notification)
    {
        $user = User::find($userId);
        return $user->notify($notification);
    }
    
    public function notifyInvoice($userId,$invoice)
    {
        return $this->notify(
            $userId, 
            new \BSSystem\LIBAccount\Notifications\Invoice($invoice)
            );
    }
    
    public function notifyInvoicePaid($userId,$invoice)
    {
        return $this->notify(
            $userId, 
            new \BSSystem\LIBAccount\Notifications\InvoicePaid($invoice)
            );
    }
}