<?php

namespace BSSystem\LIBAccount\Repositories;

use BSSystem\LIBAccount\Models\LocationCountry;
use BSSystem\LIBAccount\Models\LocationProvince;
use BSSystem\LIBAccount\Models\LocationCity;
use BSSystem\LIBAccount\Models\LocationSubdistrict;
use BSSystem\LIBAccount\Models\LocationVillage;

use BSSystem\LIBAccount\Models\Rule;

use BSSystem\Core\Base\BaseRepository;

class LocationRepo extends BaseRepository
{
    public $error = '';
        
    public function __construct(LocationCountry $model)
    {
        $this->model = $model;        
    }
    
    //buat semua fungsi untuk setiap models location
        
}