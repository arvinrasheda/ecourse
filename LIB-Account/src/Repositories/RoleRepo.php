<?php

namespace BSSystem\LIBAccount\Repositories;

use BSSystem\LIBAccount\Models\Role;
use BSSystem\LIBAccount\Models\Rule;
use BSSystem\LIBAccount\Models\RoleRule;
use BSSystem\LIBAccount\Models\RuleGroup;
use BSSystem\LIBAccount\Models\UserRole;


use BSSystem\Core\Base\BaseRepository;

class RoleRepo extends BaseRepository
{
    public $error = '';
    
    public function __construct(Role $model)
    {        
        $this->model = $model;        
    }
    
    /**
     * 
     * @param type $userId
     * @return boolean|array format mirip dataRole di SSOSession
     */
    public function getRoleByUserId($userId)
    {
        $roleData = [];
        $userRoleData = UserRole::where('user_id',$userId)->get();
        if(!$userRoleData)return false;
        foreach ($userRoleData as $key => $value) {
            $roleData = Role::find($value['role_id'])->toArray();
            
            $roleData['is_main_role'] = $value['is_main_role'];
            $roleData['has_auth_grant'] = $value['has_auth_grant'];            
            
            $rules = Rule::get();
            $roleData[$value['role_id']] = $roleData;
        }
        
        return $roleData;
    }
        
}