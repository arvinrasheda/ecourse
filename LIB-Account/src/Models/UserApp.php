<?php

namespace BSSystem\LIBAccount\Models;

class UserApp extends BaseModel
{
    /**
     * The table associated with the model.
     *
     * @var string
     */
    protected $table = 'user_apps';
    
    /**
     * The attributes that aren't mass assignable.
     *
     * @var array
     */
    protected $guarded = ['id','created_at','updated_at'];
    
    public function user()
    {
        return $this->hasOne('BSSystem\LIBAccount\Models\User', 'id','user_id');
    }
    
    public function apps()
    {
        return $this->hasOne('BSSystem\LIBAccount\Models\AppsClient', 'id','apps_id');
    }
}