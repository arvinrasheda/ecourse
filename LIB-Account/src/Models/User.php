<?php

namespace BSSystem\LIBAccount\Models;

//use Laravel\Passport\HasApiTokens;//comment jika tidak menggunakan passport
use Illuminate\Notifications\Notifiable;
use Illuminate\Contracts\Auth\MustVerifyEmail;
use Illuminate\Foundation\Auth\User as Authenticatable;

class User extends Authenticatable implements MustVerifyEmail
{
    //comment HasApiTokens jika tidak menggunakan passport
    use Notifiable;//HasApiTokens,


    protected $table = "Users";
    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'user_idcode','name', 'email', 'phone', 'password','auth_password', 'is_admin',
        'socialauth_facebook_id','socialauth_facebook_token','socialauth_facebook_data',
        'socialauth_google_id','socialauth_google_token','socialauth_google_data',
        'reff_code', 'note', 'role', 'registration_reff', 'status', 'banned_note'
    ];
    
    /**
     * The attributes that should be hidden for arrays.
     *
     * @var array
     */
    protected $hidden = [
        'password', 'remember_token', 'auth_password'
    ];
    
    public function getConnectionName()
    {
        $this->connection = config('bssystem.connection_ac');
        
        return parent::getConnectionName();        
    }
}
