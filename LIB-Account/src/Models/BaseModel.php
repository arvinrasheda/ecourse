<?php

namespace BSSystem\LIBAccount\Models;

use Illuminate\Database\Eloquent\Model;

class BaseModel extends Model
{
    
    public function getConnectionName()
    {
        $this->connection = config('bssystem.connection_ac');
        return parent::getConnectionName();        
     }
}