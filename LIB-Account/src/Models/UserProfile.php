<?php

namespace BSSystem\LIBAccount\Models;

class UserProfile extends BaseModel
{
    /**
     * The table associated with the model.
     *
     * @var string
     */
    protected $table = 'user_profiles';
    
    /**
     * The attributes that aren't mass assignable.
     *
     * @var array
     */
    protected $guarded = ['id','created_at','updated_at'];
    protected $fillable = [
        'user_id',
        'first_name',
        'last_name',
        'avatar',
        'email2',
        'email2_verified_at',
        'phone2',
        'phone2_verified_at',
        'gender',
        'date_of_birth',
        'socnet_facebook',
        'socnet_instagram',
        'address',
        'country_id',
        'province_id',
        'city_id',
        'subdistrict_id',
        'village_id',
        'postal_code'
        ];
}