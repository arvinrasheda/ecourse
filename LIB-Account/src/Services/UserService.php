<?php

namespace BSSystem\LIBAccount\Services;

use BSSystem\LIBAccount\Repositories\UserRepo;

class UserService
{
    protected $repo;

    public function __construct(UserRepo $repo)
    {
        $this->repo = $repo;
    }

    public function all()
    {
        $data = $this->repo->getAll();
        return $data;
    }
}