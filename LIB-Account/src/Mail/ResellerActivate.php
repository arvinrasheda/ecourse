<?php

namespace BSSystem\LIBAccount\Mail;

use Illuminate\Bus\Queueable;
use Illuminate\Mail\Mailable;
use Illuminate\Queue\SerializesModels;
use Illuminate\Contracts\Queue\ShouldQueue;
// use App\Mailling;

class ResellerActivate extends Mailable
{
    use Queueable, SerializesModels;
    protected $reseller;
    /**
     * Create a new message instance.
     *
     * @return void
     */
    public function __construct($reseller)
    {
        $this->reseller = $reseller;
    }

    /**
     * Build the message.
     *
     * @return $this
     */
    public function build()
    {
        return $this->view('emails.auth.resellerActivate')->with([
            'reseller' => $this->reseller
        ]);
    }
}
