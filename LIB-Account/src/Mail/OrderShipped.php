<?php

namespace BSSystem\LIBAccount\Mail;

use Illuminate\Bus\Queueable;
use Illuminate\Mail\Mailable;
use Illuminate\Queue\SerializesModels;
use Illuminate\Contracts\Queue\ShouldQueue;
// use App\Mailling;

class OrderShipped extends Mailable
{
    use Queueable, SerializesModels;
    protected $orderShipped;

    /**
     * Create a new message instance.
     *
     * @return void
     */
    public function __construct($orderShipped)
    {
        $this->orderShipped = $orderShipped;
    }

    /**
     * Build the message.
     *
     * @return $this
     */
    public function build()
    {
        return $this->view('emails.commerce.orderShipped')->with([
            'orderShipped' => $this->orderShipped
        ]);
    }
}
