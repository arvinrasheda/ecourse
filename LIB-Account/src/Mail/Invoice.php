<?php

namespace BSSystem\LIBAccount\Mail;

use Illuminate\Bus\Queueable;
use Illuminate\Mail\Mailable;
use Illuminate\Queue\SerializesModels;
use Illuminate\Contracts\Queue\ShouldQueue;

class Invoice extends Mailable
{

    use Queueable,
        SerializesModels;

    protected $invoice;
    
    /**
     * Create a new message instance.
     *
     * @return void
     */
    public function __construct($invoice)
    {
        $this->invoice = $invoice;
    }

    /**
     * Build the message.
     *
     * @return $this
     */
    public function build()
    {
        return $this->view('emails.commerce.invoce')->with([
                'invoice' => $this->invoice
        ]);
//        ->attach(
//            '/path/to/file', [
//                'as' => 'name.pdf',
//                'mime' => 'application/pdf',
//        ]);
    }

}
