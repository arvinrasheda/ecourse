<?php

namespace BSSystem\LIBAccount\Mail;

use Illuminate\Bus\Queueable;
use Illuminate\Mail\Mailable;
use Illuminate\Queue\SerializesModels;
use Illuminate\Contracts\Queue\ShouldQueue;
// use App\Mailling;

class CommissionPaid extends Mailable
{
    use Queueable, SerializesModels;
    protected $reseller;

    /**
     * Create a new message instance.
     *
     * @return void
     */
    public function __construct($reseller)
    {
        $this->reseller = $reseller;
    }

    /**
     * Build the message.
     *
     * @return $this
     */
    public function build()
    {
        // return $this->from('noreply@billionairestore.co.id')
        //             ->view('email.EmailCommissionPaid');
        return $this->view('emails.commerce.commissionPaid')->with([
            'reseller' => $this->reseller
        ]);
    }
}
