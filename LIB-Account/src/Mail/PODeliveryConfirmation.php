<?php

namespace BSSystem\LIBAccount\Mail;

use Illuminate\Bus\Queueable;
use Illuminate\Mail\Mailable;
use Illuminate\Queue\SerializesModels;
use Illuminate\Contracts\Queue\ShouldQueue;
// use App\Mailling;

class PODeliveryConfirmation extends Mailable
{
    use Queueable, SerializesModels;
    protected $po;

    /**
     * Create a new message instance.
     *
     * @return void
     */
    public function __construct($po)
    {
        $this->po = $po;
    }

    /**
     * Build the message.
     *
     * @return $this
     */
    public function build()
    {
        return $this->view('emails.commerce.poDeliveryConfirmation')->with([
            'po' => $this->po
        ]);
    }
}
