<?php

namespace BSSystem\MODDashboard\Controllers;

use Illuminate\Http\Request;
use BSSystem\MODDashboard\Repositories\DashboardRepository;
use BSSystem\MODDashboard\Responses\DashboardResponse;

use BSSystem\Core\Base\BaseController;

class DashboardController extends BaseController
{
    
    private $repo;
    
    public function __construct(DashboardRepository $repo)
    {
        \Breadcrumb::add('Dashboard',route('dashboard.index'));
       
        $this->repo = $repo;

        $this->middleware('auth');
       }
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $data['ecourse'] = $this->repo->countEcourse();
        $data['materi'] = $this->repo->countMateri();
        $data['modul'] = $this->repo->countModul();
        
        return view('dashboard.index', $data);
    }

    public function ecourse()
    {
        \Breadcrumb::add('Ecourse',route('dashboard.ecourse'));
        
        $data['data'] = $this->repo->getEcourse();

        return view('dashboard.ecourse', $data);
    }

    public function groupEcourse()
    {
        \Breadcrumb::add('Group Ecourse',route('dashboard.groupecourse'));

        $data['data'] = $this->repo->getEcourse();

        return view('dashboard.groupecourse', $data);
    }

    public function group($id)
    {
        \Breadcrumb::add('Group',route('dashboard.group', $id));

        $data['data'] = $this->repo->getGroup($id);

        return view('dashboard.group', $data);
    }

    public function showEcourse($id){

        $data['data'] = $this->repo->getMateri($id);
        $data['materi'] = $this->repo->getMateriUser($id);

        \Breadcrumb::add('Ecourse ',route('dashboard.showEcourse', $id));
        
        return view('dashboard.materi', $data);
    }

    public function showMateri($id){
        $data['data'] = $this->repo->getModul($id);

        \Breadcrumb::add('Materi ',route('dashboard.showMateri', $id));
        return view('dashboard.modul', $data);
    }

    public function showVideo($id){
        $data['data'] = $this->repo->getVideo($id);
        
        \Breadcrumb::add('Video ',route('dashboard.showVideo', $id));
        return view('dashboard.video', $data);
    }
}
