@extends(ACAuth::user('is_admin') == 0 ? 'layouts-ecourse.member' : 'layouts-ecourse.home')

@section('content')
<div class="container">
    <section class="content-header">
        <?php echo resolve('breadcrumb')->generate(); ?>
    </section>
    <section class="content">
        <div class="row">
            <div class="col-md-12">
                <!-- general form elements -->
                <div class="box box-primary">
                    <div class="box-header with-border">
                        <h3 class="box-title">Detail</h3>
                    </div>
                    <!-- /.box-header -->
                    <!-- form start -->
                    <div>
                        <div class="box-body">
                            <div class="form-group">
    <label for="input-{{NAME}}">Name</label>
    <input type="text"  class="form-control" id="input-{{NAME}}" value="{{ $data ? $data->{{NAME}} : '' }}" readonly>
</div><div class="form-group">
    <label for="input-{{NAME}}">Description</label>
    <textarea id="input-{{NAME}}"  class="form-control" readonly>{{ $data ? $data->{{NAME}} : '' }}</textarea>
</div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </section>
    
</div>
@endsection