@extends(ACAuth::user('is_admin') == 0 ? 'layouts-ecourse.member' : 'layouts-ecourse.home')

@section('content')
<h4 class="d-flex flex-wrap justify-content-between align-items-center w-100 font-weight-bold pt-2 mb-4">
    <div class="col-12 col-md px-0 pb-2">
        <?php echo \Breadcrumb::generate(); ?>
    </div>
</h4>
<div class="card mb-4">
    <div class="card-body demo-inline-spacing">
        @foreach ($data as $item)
        <a href="{{ $item->telegram }}" class="btn btn-lg btn-blue"><span class="far fa-paper-plane"></span>&nbsp;&nbsp;Telegram</a>
        <a href="{{ $item->facebook }}" class="btn btn-lg btn-facebook"><span class="ion ion-logo-facebook"></span>&nbsp;&nbsp;Facebook</a>
        @endforeach
    </div>
</div>
@endsection