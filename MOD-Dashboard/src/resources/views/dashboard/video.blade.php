@extends(ACAuth::user('is_admin') == 0 ? 'layouts-ecourse.member' : 'layouts-ecourse.home')

@section('content')
<h4 class="d-flex justify-content-between align-items-center w-100 font-weight-bold mb-2">
    <div>
        <?php echo \Breadcrumb::generate(); ?>
    </div>
</h4>

<div class="embed-responsive embed-responsive-16by9">
    <iframe class="embed-responsive-item" src="https://www.youtube.com/embed/{{ $data->link }}" allowfullscreen></iframe>
</div>
@endsection