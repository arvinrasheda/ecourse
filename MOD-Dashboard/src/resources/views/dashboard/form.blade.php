@extends(ACAuth::user('is_admin') == 0 ? 'layouts-ecourse.member' : 'layouts-ecourse.home')

@section('content')
<div class="container">
    <section class="content-header">
        <h1>
            FORM
        </h1>
        <?php echo \Breadcrumb::generate(); ?>
    </section>
    <section class="content">
        <div class="row">
            <div class="col-md-12">
                <!-- general form elements -->
                <div class="box box-primary">
                    <div class="box-header with-border">
                        <h3 class="box-title">{{ $mode=='create'?'Tambah Baru':'Edit Data' }}</h3>
                    </div>
                    <!-- /.box-header -->
                    <!-- form start -->
                    <form role="form" method="post" action="{{ $mode=='create'?route('dashboard.store'):route('dashboard.update',$data->id) }}">
                        {{ csrf_field() }}
                        {{ $mode=='edit'?method_field('PATCH'):'' }}
                        <div class="box-body">
                            <div class="form-group">
    <label for="input-name">Name</label>
    <input type="text" name="name" class="form-control" id="input-name" value="{{ $data ? $data->name : old('name') }}">
</div><div class="form-group">
    <label for="input-description">Description</label>
    <textarea name="description" id="input-description"  class="form-control">{{ $data ? $data->description : old('description') }}</textarea>
</div>
                        </div>
                        <!-- /.box-body -->

                        <div class="box-footer">
                            <button type="submit" class="btn btn-primary">Save</button>
                        </div>
                    </form>
                </div>
            </div>
        </div>
    </section>
    
</div>
@endsection