@extends(ACAuth::user('is_admin') == 0 ? 'layouts-ecourse.member' : 'layouts-ecourse.home')

@section('content')
<h4 class="d-flex flex-wrap justify-content-between align-items-center w-100 font-weight-bold mb-2">
    <div class="col-12 col-md px-0 pb-2">
        <?php echo \Breadcrumb::generate(); ?>
    </div>
</h4>

<div class="card mb-4">
    <ul class="list-group list-group-flush">
        @foreach ($data as $item)
        <li class="list-group-item py-4">
            <div class="media flex-wrap">
                <div class="media-body ml-sm-4">
                    <h5 class="mb-2">
                        <div class="float-right font-weight-semibold ml-3">Rp. {{ $item->price }},-</div>
                        @if (in_array($item->id, $materi))
                        <a href="{{route('dashboard.showMateri', $item->id)}}" class="text-dark">{{ $item->tittle }}</a>&nbsp;
                        @else
                        <a href="javascript:void(0)" class="text-dark">{{ $item->tittle }}</a>&nbsp;
                        @endif
                    </h5>
                    <div>{{ $item->description }}</div>
                    <div class="mt-2">
                        <span class="badge badge-outline-default text-muted font-weight-normal">Java</span>
                    </div>
                </div>
            </div>
        </li>
        @endforeach
    </ul>
</div>

<nav>
    {{ $data->links() }}
</nav>
@endsection
