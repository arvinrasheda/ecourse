@extends(ACAuth::user('is_admin') == 0 ? 'layouts-ecourse.member' : 'layouts-ecourse.home')

@section('content')
<h4 class="d-flex flex-wrap justify-content-between align-items-center w-100 font-weight-bold mb-2">
    <div class="col-12 col-md px-0 pb-2">
        <?php echo \Breadcrumb::generate(); ?>
    </div>
</h4>

<ul class="nav bg-lighter container-p-x py-1 container-m--x mb-4">
    <li class="nav-item">
        <a class="nav-link text-dark font-weight-bold pl-0" href="javascript:void(0)">Ecourses</a>
    </li>
    <li class="nav-item">
        <a class="nav-link text-dark font-weight-bold pl-0" href="javascript:void(0)">Ecourse you might like</a>
    </li>
</ul>

<div class="row">
    @foreach ($data as $item)
    <div class="col-sm-6 col-xl-4">
        <div class="card mb-4">
            <div class="w-100">
                <a href="javascript:void(0)" class="card-img-top d-block ui-rect-60 ui-bg-cover" style="background-image: url('{{ url('storage/'.$item->thumbnail) }}');">
                    <div class="d-flex justify-content-between align-items-end ui-rect-content p-3">
                        <div class="text-big">
                            <div class="badge badge-dark font-weight-bold">Rp. {{ $item->price }}</div>
                        </div>
                    </div>
                </a>
            </div>
            <div class="card-body">
                <h5 class="mb-3"><a href="{{ route('dashboard.showEcourse', $item->id) }}" class="text-dark">{{ $item->tittle}}</a></h5>
                <p class="text-muted mb-3">{{ $item->description }}</p>
            </div>
        </div>
    </div>
    @endforeach
</div>

<hr class="border-light mt-2 mb-4">

<nav>
    <ul class="pagination justify-content-center">
        <li class="page-item disabled">
            <a class="page-link" href="javascript:void(0)">«</a>
        </li>
        <li class="page-item active">
            <a class="page-link" href="javascript:void(0)">1</a>
        </li>
        <li class="page-item">
            <a class="page-link" href="javascript:void(0)">2</a>
        </li>
        <li class="page-item">
            <a class="page-link" href="javascript:void(0)">3</a>
        </li>
        <li class="page-item">
            <a class="page-link" href="javascript:void(0)">4</a>
        </li>
        <li class="page-item">
            <a class="page-link" href="javascript:void(0)">5</a>
        </li>
        <li class="page-item">
            <a class="page-link" href="javascript:void(0)">»</a>
        </li>
    </ul>
</nav>
@endsection