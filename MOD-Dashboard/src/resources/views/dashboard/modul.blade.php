@extends(ACAuth::user('is_admin') == 0 ? 'layouts-ecourse.member' : 'layouts-ecourse.home')

@section('styles')
<link rel="stylesheet" href="{{ asset('/vendor/libs/datatables/datatables.css') }}">
@endsection

@section('scripts')
<!-- Dependencies -->
<script src="{{ asset('/vendor/libs/datatables/datatables.js') }}"></script>
<script src="{{ asset('/js/tables_datatables.js') }}"></script>
@endsection

@section('content')
<h4 class="d-flex justify-content-between align-items-center w-100 font-weight-bold mb-2">
    <div>
        <?php echo \Breadcrumb::generate(); ?>
    </div>
</h4>

<div class="card">
    <div class="card-datatable table-responsive">
        <table class="datatables-demo table table-striped table-bordered">
            <thead>
                <tr>
                    <th width="20">No</th>
                    <th>Tittle</th>
                    <th width="70">Type</th>
                    <th width="70">Published</th>
                    <th width="70">Action</th>
                </tr>
            </thead>
            <tbody>
                @php(
                $no = 1 {{-- buat nomor urut --}}
                )
                @foreach($data as $item)
                <tr class="odd">
                    <td>{{ $no++ }}</td>
                    <td>{{($item->tittle)}}</td>
                    <td>
                        @if ($item->link)
                        <span class="badge badge-outline-danger">Video</span>
                        @else
                        <span class="badge badge-outline-secondary">PDF</span>
                        @endif
                    </td>
                    <td>{{ $item->created_at->format('d M Y') }}</td>
                    <td>
                        @if ($item->link)
                        <a href="{{ route('dashboard.showVideo', $item->id) }}" class="btn btn-default btn-xs icon-btn md-btn-flat article-tooltip" data-toggle="tooltip"
                            title data-original-title="View"><i class="ion ion-ios-eye"></i></a>
                        @else
                        <a href="{{ url('storage/'.$item->file) }}" class="btn btn-default btn-xs icon-btn md-btn-flat article-tooltip" data-toggle="tooltip"
                            title data-original-title="Dwonload"><i class="ion ion-md-cloud-download"></i></a>
                        @endif
                    </td>
                </tr>
                @endforeach
            </tbody>
        </table>
    </div>
</div>
@endsection
