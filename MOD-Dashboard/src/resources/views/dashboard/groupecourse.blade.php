@extends(ACAuth::user('is_admin') == 0 ? 'layouts-ecourse.member' : 'layouts-ecourse.home')

@section('styles')
<link rel="stylesheet" href="{{ asset('/vendor/libs/datatables/datatables.css') }}">
@endsection

@section('scripts')
<!-- Dependencies -->
<script src="{{ asset('/vendor/libs/datatables/datatables.js') }}"></script>
<script src="{{ asset('/js/tables_datatables.js') }}"></script>
@endsection

@section('content')
<h4 class="d-flex justify-content-between align-items-center w-100 font-weight-bold mb-2">
    <div>
        <?php echo \Breadcrumb::generate(); ?>
    </div>
</h4>

<div class="card">
    <div class="card-datatable table-responsive">
        <table class="datatables-demo table table-striped table-bordered">
            <thead>
                <tr>
                    <th width="20">No</th>
                    <th>Ecourse</th>
                    <th width="70">Action</th>
                </tr>
            </thead>
            <tbody>
                @php(
                $no = 1 {{-- buat nomor urut --}}
                )
                @foreach($data as $item)
                <tr class="odd">
                    <td>{{ $no++ }}</td>
                    <td>{{($item->tittle)}}</td>
                    <td>
                        <a href="{{ route('dashboard.group', $item->id) }}" class="btn btn-default btn-xs icon-btn md-btn-flat article-tooltip" data-toggle="tooltip"
                            title data-original-title="View"><i class="ion ion-ios-eye"></i></a>
                    </td>
                </tr>
                @endforeach
            </tbody>
        </table>
    </div>
</div>
@endsection
