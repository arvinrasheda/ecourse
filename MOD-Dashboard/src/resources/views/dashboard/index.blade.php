@extends(ACAuth::user('is_admin') == 0 ? 'layouts-ecourse.member' : 'layouts-ecourse.home')

@section('content')
<h4 class="d-flex flex-wrap justify-content-between align-items-center w-100 font-weight-bold pt-2 mb-4">
    <div class="col-12 col-md px-0 pb-2">
        <?php echo \Breadcrumb::generate(); ?>
    </div>
</h4>
@if (auth()->user()->is_admin == 1)
<div class="card mb-4">
    <div class="row no-gutters row-bordered">
        <div class="col-md-6 col-lg-3">

            <div class="card-body d-flex align-items-center">
                <span class="ui-icon ui-statistics-icon ion ion-ios-easel text-primary"></span>
                <div class="ml-3">
                    <div class="text-xlarge">{{ $ecourse }}</div>
                    <div class="text-muted small">Ecourse</div>
                </div>
            </div>

        </div>
        <div class="col-md-6 col-lg-3">

            <div class="card-body d-flex align-items-center">
                <span class="ui-icon ui-statistics-icon ion ion-ios-albums text-success"></span>
                <div class="ml-3">
                    <div class="text-xlarge">{{ $materi }}</div>
                    <div class="text-muted small">Materi</div>
                </div>
            </div>

        </div>
        <div class="col-md-6 col-lg-3">

            <div class="card-body d-flex align-items-center">
                <span class="ui-icon ui-statistics-icon ion ion-md-filing text-info"></span>
                <div class="ml-3">
                    <div class="text-xlarge">{{ $modul }}</div>
                    <div class="text-muted small">Modul</div>
                </div>
            </div>

        </div>
        <div class="col-md-6 col-lg-3">

            <div class="card-body d-flex align-items-center">
                <span class="ui-icon ui-statistics-icon ion ion-md-eye text-danger"></span>
                <div class="ml-3">
                    <div class="text-xlarge">521,332</div>
                    <div class="text-muted small">Total visits</div>
                </div>
            </div>

        </div>
    </div>
</div>
@endif
@endsection