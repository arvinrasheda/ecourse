<?php

namespace BSSystem\MODDashboard\Repositories;

use BSSystem\MODDashboard\Models\Dashboard;
use BSSystem\MODEcourse\Models\Ecourse;
use BSSystem\MODMember\Models\Member;
use BSSystem\LIBModul\Models\Modul;
use BSSystem\LIBMateri\Models\Materi;
use Illuminate\Support\Facades\DB;
use BSSystem\LIBMateriMember\Models\MateriMember;
use BSSystem\LIBGroup\Models\Group;

use BSSystem\Core\Base\BaseRepository;
use BSSystem\LIBGroup\Models\GroupMember;

class DashboardRepository extends BaseRepository
{
    public function __construct(Dashboard $model)
    {
        $this->model = $model;
    }

    public function getEcourse()
    {
        $userId = \ACAuth::user('id');
        $ecourse = DB::select('select ecourse.id, ecourse.tittle, ecourse.description, ecourse.thumbnail, ecourse.price, member.user_id, member.ecourse_id FROM ecourse, member WHERE ecourse.id = member.ecourse_id AND member.user_id = '.$userId.'');
        return $ecourse;
    }

    public function getGroup($id)
    {
        $group = Ecourse::where('id', $id)->get();
        return $group;
    }

    public function getMateri($ecourse_id)
    {
        $materi = Materi::where('ecourse_id', $ecourse_id)->paginate(5);
        return $materi;
    }

    public function getMateriUser($ecourse_id)
    {
        $userId = \ACAuth::user('id');;
        $materi = MateriMember::where([['user_id', $userId],['ecourse_id', $ecourse_id]])->pluck('materi_id')->toArray();
        return $materi;        
    }

    public function getModul($id)
    {
        $modul = Modul::where('materi_id', $id)->paginate(5);
        return $modul;
    }

    public function getVideo($id)
    {
        $video = Modul::where('id', $id)->first();
        return $video;
    }

    public function countEcourse()
    {
        $ecourse = Ecourse::all();
        return count($ecourse);
    }

    public function countMateri()
    {
        $materi = Materi::all();
        return count($materi);
    }

    public function countModul()
    {
        $modul = Modul::all();
        return count($modul);
    }
}