<?php

namespace BSSystem\MODDashboard\Models;

use Illuminate\Database\Eloquent\Model;

class Dashboard extends Model
{
    /**
     * The table associated with the model.
     *
     * @var string
     */
    protected $table = 'dashboard';
    
    /**
     * The attributes that aren't mass assignable.
     *
     * @var array
     */
    protected $guarded = ['id','created_at','updated_at'];
}