<?php
Route::get('/', 'DashboardController@index')->name('home');

Route::prefix('dashboard')->group(function(){
    Route::get('/', 'DashboardController@index')->name('dashboard.index');
    Route::get('/ecourse', 'DashboardController@ecourse')->name('dashboard.ecourse');
    Route::get('/materi', 'DashboardController@materi')->name('dashboard.materi');
    Route::get('/materi/{id}', 'DashboardController@showMateri')->name('dashboard.showMateri');
    Route::get('/group', 'DashboardController@groupEcourse')->name('dashboard.groupecourse');
    Route::get('/group/{id}', 'DashboardController@group')->name('dashboard.group');
    Route::get('/ecourse/{id}', 'DashboardController@showEcourse')->name('dashboard.showEcourse');
    Route::get('/modul/{id}', 'DashboardController@showVideo')->name('dashboard.showVideo');
});