<?php

return [
    'cur_apps' => [
        'id' => env('CUR_APPS_ID',7),
        'name' => 'Billionaire Store Ecourse',
        'apps_code' => 'ecourse',
        'default_role' => '1',
        'home_url' => env('CUR_APPS_HOME_URL','http://localhost/Laravel/BS-System/APPS-Ecourse/Base/public/'),
        'update_url' => env('CUR_APPS_UPDATE_URL','http://localhost/Laravel/BS-System/APPS-Ecourse/Base/public/webauth/update'),
        'session_grab_url' => env('CUR_APPS_SESSIONGRAB_URL','http://localhost/Laravel/BS-System/APPS-Ecourse/Base/public/webauth/grab'),
        'api_key' => env('CUR_APPS_API_KEY','$2y$10$w8c6gRVPkvg/XGGnk/JJ7uz1eBdzLhEO5Vo.YMPOQdM1KtncGJOwi'),
        'allow_register' => 1,
        'permission' => []
    ]
];
