<?php

namespace App\Modules\Ecourse\Models;

use Illuminate\Database\Eloquent\Model;

class Ecourse extends Model
{
    /**
     * The table associated with the model.
     *
     * @var string
     */
    protected $table = 'ecourse';
    
    /**
     * The attributes that aren't mass assignable.
     *
     * @var array
     */
    protected $guarded = ['id','created_at','updated_at'];
}