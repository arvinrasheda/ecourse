<?php

namespace App\Modules\Ecourse\Repositories;

use App\Modules\Ecourse\Models\Ecourse;

use BSSystem\Core\Base\BaseRepository;

class EcourseRepo extends BaseRepository
{
    public function __construct(Ecourse $model)
    {
        $this->model = $model;
    }
}