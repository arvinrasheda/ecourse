<?php

Route::prefix('ecourse')->group(function(){
    Route::get('/', 'EcourseController@index')->name('ecourse.index');
    Route::post('/', 'EcourseController@store')->name('ecourse.store');
    Route::get('/create', 'EcourseController@create')->name('ecourse.create');
    Route::delete('/{id}', 'EcourseController@destroy')->name('ecourse.destroy');
    Route::put('/{id}', 'EcourseController@update')->name('ecourse.update');
    Route::get('/{id}', 'EcourseController@show')->name('ecourse.show');
    Route::get('/{id}/edit', 'EcourseController@edit')->name('ecourse.edit');
});