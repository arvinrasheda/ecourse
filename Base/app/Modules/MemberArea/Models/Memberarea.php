<?php

namespace App\Modules\MemberArea\Models;

use Illuminate\Database\Eloquent\Model;

class Memberarea extends Model
{
    /**
     * The table associated with the model.
     *
     * @var string
     */
    protected $table = 'memberarea';
    
    /**
     * The attributes that aren't mass assignable.
     *
     * @var array
     */
    protected $guarded = ['id','created_at','updated_at'];
}