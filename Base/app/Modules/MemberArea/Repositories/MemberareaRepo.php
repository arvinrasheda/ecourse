<?php

namespace App\Modules\MemberArea\Repositories;

use App\Modules\MemberArea\Models\Memberarea;

use BSSystem\Core\Base\BaseRepository;

class MemberareaRepo extends BaseRepository
{
    public function __construct(Memberarea $model)
    {
        $this->model = $model;
    }
}