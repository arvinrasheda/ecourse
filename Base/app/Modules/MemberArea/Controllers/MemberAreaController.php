<?php

namespace App\Modules\MemberArea\Controllers;

use Illuminate\Http\Request;
use App\Modules\MemberArea\Repositories\MemberareaRepo;
use App\Modules\MemberArea\Responses\MemberareaResponse;

use BSSystem\Core\Base\BaseController;

class MemberAreaController extends BaseController
{
    
    private $repo;
    
    public function __construct(MemberareaRepo $repo)
    {
        
        \Breadcrumb::add('memberarea',route('memberarea.index'));
        
        $this->repo = $repo;
    }
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $data['data'] = $this->repo->all();
        return new MemberareaResponse($data,'memberarea.list');
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        \Breadcrumb::add('Tambah Baru',route('memberarea.create'));        
        
        $data['mode'] = 'create';
        $data['data'] = [];

        return view('memberarea.form', $data);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $data = $request->all();
//        $request->validate([
//            'name' => 'required|string|max:255'
//        ]);        
        $this->repo->create($data);
        return redirect()->route('memberarea.index')->with('alert', ['type' => 'success', 'message' => 'Data inserted successfully']);
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $data['data'] = $this->repo->find($id);
        
        \Breadcrumb::add('View id : <b>'.$data['data']['ig'].'</b>',route('memberarea.show',$data['data']['id']));
        
        return new MemberareaResponse($data,'memberarea.detail');
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $data['data'] = $this->repo->find($id);
        
        \Breadcrumb::add('Edit di : <b>'.$data['data']['id'].'</b>',route('memberarea.edit',$data['data']['id']));
        
        $data['mode'] = 'edit';

        return view('memberarea.form', $data);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request,$id)
    {
        $input = $request->all();
        
//        //pastikan sudah
//        if($request->input('name')){
//            $validate['name'] = 'required|string|max:255';
//        }
//        
//        if(isset($validate)){
//            $request->validate($validate);
//        }
        
        $this->repo->update($input,$id);
        
        return redirect()->route('memberarea.index')->with('alert', ['type' => 'success', 'message' => 'Data updated successfully']);

    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $this->repo->delete($id);
        return redirect()->route('memberarea.index')->with('alert', ['type' => 'success', 'message' => 'Data Deleted !']);

    }    
    
}
