<?php

Route::prefix('memberarea')->group(function(){
    Route::get('/', 'MemberAreaController@index')->name('memberarea.index');
    Route::post('/', 'MemberAreaController@store')->name('memberarea.store');
    Route::get('/create', 'MemberAreaController@create')->name('memberarea.create');
    Route::delete('/{id}', 'MemberAreaController@destroy')->name('memberarea.destroy');
    Route::put('/{id}', 'MemberAreaController@update')->name('memberarea.update');
    Route::get('/{id}', 'MemberAreaController@show')->name('memberarea.show');
    Route::get('/{id}/edit', 'MemberAreaController@edit')->name('memberarea.edit');
});