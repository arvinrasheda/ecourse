<!-- Layout sidenav -->
<div id="layout-sidenav" class="{{ isset($layout_sidenav_horizontal) ? 'layout-sidenav-horizontal sidenav-horizontal container-p-x flex-grow-0' : 'layout-sidenav sidenav-vertical' }} sidenav bg-sidenav-theme">
    @empty($layout_sidenav_horizontal)
    <!-- Brand demo (see assets/css/demo/demo.css) -->
    <div class="app-brand demo">
        <a href="/" class="app-brand-text demo sidenav-text font-weight-normal ml-2">Admin</a>
        <a href="javascript:void(0)" class="layout-sidenav-toggle sidenav-link text-large ml-auto">
            <i class="ion ion-md-menu align-middle"></i>
        </a>
    </div>

    <div class="sidenav-divider mt-0"></div>
    @endempty

    <!-- Links -->
    <ul class="sidenav-inner{{ empty($layout_sidenav_horizontal) ? ' py-1' : '' }}">

        <!-- Dashboards -->
        <li class="sidenav-item">
            <a href="{{ route('dashboard.index') }}" class="sidenav-link"><i class="sidenav-icon ion ion-md-speedometer"></i>
                <div>Dashboards</div>
            </a>
        </li>
        <!-- Ecourse -->
        <li class="sidenav-item">
            <a href="" class="sidenav-link sidenav-toggle"><i class="sidenav-icon ion ion-ios-desktop"></i>
                <div>Ecourse</div>
            </a>
            <ul class="sidenav-menu">
                <li class="sidenav-item">
                    <a href="{{ route('ecourse.index') }}" class="sidenav-link">
                        <i class="sidenav-icon ion ion-ios-easel"></i>List Ecourse
                    </a>
                </li>
                <li class="sidenav-item">
                    <a href="{{ route('member.index') }}" class="sidenav-link">
                        <i class="sidenav-icon ion ion-ios-walk"></i>Member Ecourse
                    </a>
                </li>
            </ul>
        </li>
        <!-- Materi -->
        <li class="sidenav-item">
            <a href="" class="sidenav-link sidenav-toggle"><i class="sidenav-icon ion ion-ios-albums"></i>
                <div>Materi</div>
            </a>
            <ul class="sidenav-menu">
                <li class="sidenav-item">
                    <a href="{{ route('materi.index') }}" class="sidenav-link">
                        <i class="sidenav-icon ion ion-ios-albums"></i>List Materi
                    </a>
                </li>
                <li class="sidenav-item">
                    <a href="{{ route('materimember.index') }}" class="sidenav-link">
                        <i class="sidenav-icon ion ion-ios-walk"></i>Member Materi
                    </a>
                </li>
            </ul>
        </li>
        <!-- Modul -->
        <li class="sidenav-item">
            <a href="{{ route('modul.index') }}" class="sidenav-link">
                <i class="sidenav-icon ion ion-md-filing"></i>Modul
            </a>
        </li>
        <!-- User -->
        <li class="sidenav-item">
            <a href="{{ route('userEcourse.index') }}" class="sidenav-link">
                <i class="sidenav-icon ion ion-ios-contact"></i>User
            </a>
        </li>
    </ul>
</div>
<!-- / Layout sidenav -->
