<nav class="navbar navbar-expand-lg bg-white sticky-top shadow-sm">
    <div class="container">

        <a href="#" class="navbar-brand text-large font-weight-bolder line-height-1">Member</a>

        <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarSupportedContent"
            aria-controls="navbarSupportedContent" aria-expanded="false" aria-label="Toggle navigation">
            <span class="navbar-toggler-icon"></span>
        </button>

        <div class="collapse navbar-collapse" id="navbarSupportedContent">
            <ul class="navbar-nav mr-auto d-block d-lg-none">
                <li class="nav-item active">
                    <a class="nav-link" href="{{ route('dashboard.index') }}">
                        <i class="ion ion-md-speedometer mr-2"></i>
                        Dashboard
                    </a>
                </li>
                <li class="nav-item">
                    <a class="nav-link" href="{{ route('dashboard.ecourse') }}">
                        <i class="ion ion-ios-easel mr-2"></i>
                        Ecourse
                    </a>
                </li>
                <li class="nav-item">
                    <a class="nav-link" href="{{ route('dashboard.groupecourse') }}">
                        <i class="ion ion-ios-contacts mr-2"></i>
                        Group
                    </a>
                </li>
            </ul>
            <ul class="navbar-nav ml-auto">
                <li class="nav-item dropdown">
                    <a class="nav-item nav-link dropdown-toggle" href="javascript:void(0)" data-toggle="dropdown">
                        <span class="d-inline align-items-center align-middle">
                            <i class="ion ion-md-person navbar-icon align-middle"></i>
                            <span class="px-1 mx-2">{{ ACAuth::user('name') }}</span>
                        </span>
                    </a>
                    <div class="dropdown-menu">
                        <a class="dropdown-item" href="{{ route('auth.logout',['apps_code' => config('bssystem.cur_apps.apps_code')]) }}">Keluar</a>
                    </div>
                </li>
            </ul>
        </div>

    </div>
</nav>
