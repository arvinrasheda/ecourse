<div class="card d-none d-sm-none d-md-none d-lg-block">
    <div class="side-menu my-2">
        <a href="{{ route('dashboard.index') }}" class="list-side-menu list-side-menu-action active">
            <i class="ion ion-md-speedometer mr-2"></i>
            Dashboard
        </a>
        <a href="{{ route('dashboard.ecourse')}}" class="list-side-menu list-side-menu-action">
            <i class="ion ion-ios-easel mr-2"></i>
            Ecourse
        </a>
        <a href="{{ route('dashboard.groupecourse') }}" class="list-side-menu list-side-menu-action">
            <i class="ion ion-ios-contacts mr-2"></i>
            Group
        </a>
        <a href="{{ route('profile.index') }}" class="list-side-menu list-side-menu-action">
            <i class="ion ion-md-settings mr-2"></i>
            Pengaturan Akun
        </a>
        <a href="{{ route('auth.logout',['apps_code' => config('bssystem.cur_apps.apps_code')]) }}" class="list-side-menu list-side-menu-action">
            <i class="ion ion-md-power mr-2"></i>
            Keluar
        </a>
    </div>
</div>
