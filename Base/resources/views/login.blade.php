@extends('layouts-ecourse.blank')

@section('styles')
<!-- Page -->
<link rel="stylesheet" href="{{ asset('/vendor/css/pages/authentication.css') }}">
@endsection

@section('content')
<div class="authentication-wrapper authentication-1 px-4">
    <div class="authentication-inner py-5">

        <!-- Logo -->
        <div class="d-flex justify-content-center align-items-center">
            <div class="ui-w-60">
                <div class="w-100 position-relative" style="padding-bottom: 54%">

                </div>
            </div>
        </div>
        <!-- / Logo -->

        <!-- Form -->
        <form class="my-5" action="{{ route('auth.login',['apps_code' => config('bssystem.cur_apps.apps_code')]) }}" method="GET" enctype="multipart/form-data">
            @csrf
            <div class="d-flex justify-content-between align-items-center m-0">
                <button type="submit" class="btn btn-primary">Sign In</button>
            </div>
        </form>
        <!-- / Form -->

        {{--  <div class="text-center text-muted">
            Dont have an account yet? <a href="{{ route('auth.register') }}">Sign Up</a>
        </div>  --}}

    </div>
</div>
@endsection
