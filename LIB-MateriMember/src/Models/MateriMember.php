<?php

namespace BSSystem\LIBMateriMember\Models;

use Illuminate\Database\Eloquent\Model;

class MateriMember extends Model
{
    /**
     * The table associated with the model.
     *
     * @var string
     */
    protected $table = 'materi_member';
    
    /**
     * The attributes that aren't mass assignable.
     *
     * @var array
     */
    protected $guarded = ['id','created_at','updated_at'];

    public function materi()
    {
        return $this->belongsTo('BSSystem\LIBMateri\Models\Materi');
    }
    public function user()
    {
        return $this->belongsTo('BSSystem\MODUser\Models\Users');
    }
}