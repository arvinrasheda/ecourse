<?php

namespace BSSystem\LIBMateriMember\Repositories;

use BSSystem\LIBMateriMember\Models\MateriMember;
use BSSystem\LIBMateri\Models\Materi;
use BSSystem\MODMember\Models\Member;
use BSSystem\MODUser\Models\Users;
use BSSystem\LIBProfile\Models\UserProfiles;

use BSSystem\Core\Base\BaseRepository;

class MateriMemberRepository extends BaseRepository
{
    public function __construct(MateriMember $model)
    {
        $this->model = $model;
    }

    public function getMateri()
    {
        return Materi::all();
    }

    public function createMember($data){
        $findUser = Users::where('email', $data['email'])->first();

        if(!empty($findUser)){
            $userId = $findUser->id;

            $materi = Materi::where('id', $data['materi_id'])->first();
            $ecourse_id = $materi->ecourse_id;

            $member = new Member;
            $member->user_id = $userId;
            $member->ecourse_id = $ecourse_id;
            $member->save();

            $materi = Materi::where('ecourse_id', $ecourse_id)->get();
            foreach ($materi as $key => $materiId) {
               $memberMateri = new MateriMember;
               $memberMateri->user_id = $userId;
               $memberMateri->materi_id = $data['materi_id'];
               $memberMateri->ecourse_id = $ecourse_id;
               $memberMateri->save();
            }
            
            return true;

        } elseif(empty($findUser)) {
            $user = new Users;
            $user->name = $data['name'];
            $user->email = $data['email'];
            $user->phone = $data['phone'];
            $user->password = bcrypt(substr($data['phone'], -5));
            $user->status = $data['status'];
            $user->save();

            $findUser = Users::where('email', $data['email'])->first();
            $userId = $findUser->id;

            $profiles = new UserProfiles;
            $profiles->name = $data['name'];
            $profiles->user_id = $userId;
            $profiles->email2 = $data['email'];
            $profiles->phone2 = $data['phone'];
            $profiles->save();

            $materi = Materi::where('id', $data['materi_id'])->first();
            $ecourse_id = $materi->ecourse_id;

            $member = new Member;
            $member->user_id = $userId;
            $member->ecourse_id = $ecourse_id;
            $member->save();

            $materi = Materi::where('ecourse_id', $ecourse_id)->get();
            foreach ($materi as $key => $materiId) {
               $memberMateri = new MateriMember;
               $memberMateri->user_id = $userId;
               $memberMateri->materi_id = $data['materi_id'];
               $memberMateri->ecourse_id = $ecourse_id;
               $memberMateri->save();
            }
            
            return true;

        }

    }

    public function insertData($data, $materi_id,  $status){
        foreach ($data as $key => $value) {
            $findUser = Users::where('email', $value->email)->first();

            if(!empty($findUser)){
                $userId = $findUser->id;

                $materi = Materi::where('id', $materi_id)->first();
                $ecourse_id = $materi->ecourse_id;

                $member = new Member;
                $member->user_id = $userId;
                $member->ecourse_id = $ecourse_id;
                $member->save();

                $materi = Materi::where('ecourse_id', $ecourse_id)->get();
                foreach ($materi as $key => $materiId) {
                    $memberMateri = new MateriMember;
                    $memberMateri->user_id = $userId;
                    $memberMateri->materi_id = $materi_id;
                    $memberMateri->ecourse_id = $ecourse_id;
                    $memberMateri->save();
                }
                
                return true;
            } elseif (empty($findUser)){
                $user = new Users;
                $user->name = $value->name;
                $user->email = $value->email;
                $user->phone = $value->phone;
                $user->password = bcrypt(substr($value->phone, -5));
                $user->status = $status;
                $user->save();

                $findUser = Users::where('email', $value->email)->first();
                $userId = $findUser->id;

                $profiles = new UserProfiles;
                $profiles->name = $value->name;
                $profiles->user_id = $userId;
                $profiles->email2 = $value->email;
                $profiles->phone2 = $value->phone;
                $profiles->save();

                $materi = Materi::where('id', $materi_id)->first();
                $ecourse_id = $materi->ecourse_id;

                $member = new Member;
                $member->user_id = $userId;
                $member->ecourse_id = $ecourse_id;
                $member->save();

                $materi = Materi::where('ecourse_id', $ecourse_id)->get();
                foreach ($materi as $key => $materiId) {
                    $memberMateri = new MateriMember;
                    $memberMateri->user_id = $userId;
                    $memberMateri->materi_id = $materi_id;
                    $memberMateri->ecourse_id = $ecourse_id;
                    $memberMateri->save();
                }
                
                return true;
            }
        }
        
    }
}