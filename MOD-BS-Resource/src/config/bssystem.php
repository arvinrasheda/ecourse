<?php
//default bssystem config
return [
    'dev_package_path' => env('DEV_PACKAGE_PATH', '../'),
    'connection' => env('DB_CONNECTION', 'mysql'),
    'connection_ac' => env('DB_CONNECTION_AC', 'mysql_ac'),
    'queue_connection_ac' => env('QUEUE_CONNECTION_AC', 'database_ac'),
    'default_apps' => [
        'id' => 2,
        'apps_code' => 'bs',
        'default_role' => 9
    ],
    'url' => [
        'bs' => env('BS_URL', 'https://billionairestore.co.id'),
        'bsapi' => env('BSAPI_URL', 'https://api.billionairestore.co.id'),
        'member' => env('MEMBER_URL', 'https://member.billionairestore.co.id'), 
        'memberapi' => env('MEMBERAPI_URL', 'https://member.billionairestore.co.id'),        
        'ac' => env('AC_URL', 'https://account.billionairestore.co.id'),
        'acapi' => env('ACAPI_URL', 'https://api.account.billionairestore.co.id'),
        'portal' => env('PORTAL_URL', 'https://portal.billionairestore.co.id'),
        'ecourse' => env('ECOURSE_URL', 'https://ecourse.billionairestore.co.id'),
    ]
];
