<?php

namespace BSSystem\MODBSResource\Providers;

use Illuminate\Support\ServiceProvider;
use Illuminate\Foundation\AliasLoader;
use Facades\BSSystem\MODBSResource\Services\Breadcrumb;

class AppServiceProvider extends ServiceProvider
{
    /**
     * Bootstrap any application services.
     *
     * @return void
     */
    public function boot()
    {
        //
    }

    /**
     * Register any application services.
     *
     * @return void
     */
    public function register()
    {                
        require_once __DIR__. '/../Helpers/Helper.php';
        
        $this->app->booting(function() {
            $loader = AliasLoader::getInstance();
            $loader->alias('Breadcrumb', Breadcrumb::class);
        });
        
        /** @var \Illuminate\Config\Repository $config */
        $config = $this->app['config'];
        
        //jika config sudah diset di aplikasi
        if(isset($config['bssystem'])){            
            $config1 = require __DIR__ . '/../config/bssystem.php';
            $config2 = $config['bssystem'];
            $config->set('bssystem', array_merge($config1,$config2));          
        }else{     
            $config->set('bssystem', require __DIR__ . '/../config/bssystem.php');            
        }
    }
}
