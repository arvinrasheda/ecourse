<?php
$summary = ShoppingCartSrv::getSummary();
$products = ShoppingCartSrv::getProduct();
?>
<div class="shop-cart-dropdown nav-item dropdown">
    <a href="javascript:void(0)" class="dropdown-toggle nav-link nav-billy hide-arrow text-nowrap" data-toggle="dropdown">
        <i class="lnr lnr-cart navbar-icon align-middle"></i>
        <span class="ml-1 d-none d-lg-inline d-xl-inline font-weight-semibold">{{__('marketplace.shoppingcart')}}</span>
        @if($summary['item_qty'])
        <span class="badge badge-danger indicator">{{$summary['item_qty']}}</span>
        @endif
    </a>
    <div class="dropdown-menu dropdown-menu-right">

        <div class="media align-items-center py-2 px-3">
            <i class="lnr lnr-cart d-block" style="font-size:23px"></i>
            <div class="media-body mx-3">
                <div class="text-dark font-weight-semibold">{{__('marketplace.shoppingcart')}}</div>
            </div>
            <span class="badge badge-pill badge-primary">{{$summary['item_qty']}}</span>
        </div>
        <hr class="m-0">

        @foreach($products as $product)
        <div class="media align-items-center p-3">
            <img src="{{CDNStorage::url($product['image'])}}" class="d-block ui-w-40" alt="">
            <div class="media-body small mx-3">
                <a href="javascript:void(0)" class="text-dark font-weight-semibold">{{$product['name']}}</a>
                <div class="text-muted">{{$product['price_per_item']?'Rp. '.number_format($product['price_per_item']):0}} &times; {{$product['qty']}}</div>
            </div>
            <a href="javascript:void(0)" class="text-danger text-xlarge font-weight-light">
                &times;
            </a>
        </div>
        <hr class="m-0">
        @endforeach
        
        <div class="d-flex justify-content-between align-items-center p-3">
            <div>
                <span class="text-muted">Total:</span><br>
                <span class="font-weight-bold">{{$summary['total_amount']?'Rp. '.number_format($summary['total_amount']):0}}</span>
            </div>
            <div>
                <a href="{{route('mp.order.cart')}}" class="btn btn-default btn-sm text-expanded">{{__('marketplace.shoppingcart')}}</a>
                @if($summary['item_qty']>0)
                <a href="{{route('mp.order.checkout')}}" class="btn btn-primary btn-sm text-expanded ml-1">{{__('marketplace.checkout')}}</a>
                @endif
            </div>
        </div>
    </div>
</div>