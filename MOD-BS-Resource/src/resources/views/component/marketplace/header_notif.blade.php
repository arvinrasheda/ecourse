<div class="nav-item notifikasi-billy dropdown">
    <a href="javascript:void(0)" class="nav-link nav-billy dropdown-toggle hide-arrow text-nowrap" data-toggle="dropdown">
        <i class="pe-7s-bell navbar-icon align-middle"></i>
        <span class="badge badge-primary badge-dot indicator"></span>
    </a>
    <div class="dropdown-menu dropdown-menu-right">
        <div class="font-weight-bold px-3 py-2">
            Notifikasi                  
        </div>
        <div class="list-group list-group-flush">
            <a href="detail-notifikasi.html" class="list-group-item list-group-item-action media d-flex align-items-center">
                <div class="text-nowrap">
                    <div class="ui-icon ui-icon-sm ion ion-md-notifications bg-light border-0 text-dark"></div>
                    <span class="badge badge-primary badge-dot indicator"></span>
                </div>                    
                <div class="media-body line-height-condenced ml-3">
                    <div class="font-weight-semibold text-dark">Promo Black Friday</div>
                    <div class="text-muted">
                        Lorem ipsum dolor sit amet consectetur adipisicing elit.
                    </div>
                    <div class="text-light small mt-1">
                        23 November 2018 20:24 WIB
                    </div>
                </div>
            </a>
            <a href="detail-notifikasi.html" class="list-group-item list-group-item-action media d-flex align-items-center bg-light">
                <div class="ui-icon ui-icon-sm ion ion-md-notifications bg-light border-0 text-dark"></div>
                <div class="media-body line-height-condenced ml-3">
                    <div class="font-weight-semibold text-dark">Promo Black Friday</div>
                    <div class="text-muted">
                        Lorem ipsum dolor sit amet consectetur adipisicing elit.
                    </div>
                    <div class="text-light small mt-1">
                        23 November 2018 20:24 WIB
                    </div>
                </div>
            </a>
            <a href="detail-notifikasi.html" class="list-group-item list-group-item-action media d-flex align-items-center bg-light">
                <div class="ui-icon ui-icon-sm ion ion-md-notifications bg-light border-0 text-dark"></div>
                <div class="media-body line-height-condenced ml-3">
                    <div class="font-weight-semibold text-dark">Promo Black Friday</div>
                    <div class="text-muted">
                        Lorem ipsum dolor sit amet consectetur adipisicing elit.
                    </div>
                    <div class="text-light small mt-1">
                        23 November 2018 20:24 WIB
                    </div>
                </div>
            </a>
        </div>
        <a href="notifikasi.html" class="d-block text-center text-primary font-weight-semibold small p-2 my-1">Lihat Semua Notifikasi</a>
    </div>
</div>