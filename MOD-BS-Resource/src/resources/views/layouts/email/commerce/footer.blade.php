<table cellpadding="0" cellspacing="0" style="margin: auto;" width="600">
  <tbody>
    <tr>
      <td>
        <table cellpadding="0" cellspacing="0" style="margin: 20px 0 0 0;border-bottom:1px solid #ccc;" width="100%">
			<table style="margin: 10px 0 10px;font-size: 14px; line-height: normal; color: #4a4a4a;">
				<tr>
					<td>
						<table style="background-color: #efefef">
							<tbody>
								<tr>
									<td style="width:7%;text-align:center;"><img alt="" src="http://billionairestore.co.id/static/img/warning-mail-icon.png" width="24" /></td>
									<td style="font-size: 14px; line-height: normal; color: #4a4a4a;padding:7px;">Segala bentuk informasi seperti&nbsp;<strong>nomor kontak, alamat e-mail, atau password Anda bersifat rahasia.</strong>&nbsp;Jangan menginformasikan data-data tersebut kepada siapa pun, termasuk kepada pihak yang mengatasnamakan Billionaire Store.</td>
								</tr>
							</tbody>
						</table>

						<table style="margin-top:15px;">
							<tbody>
								<tr>
									<td width="53%">
									<p style="font-size:14px;margin:0;">Copyright © 2018 CV. Billionaire Sinergi Korpora. All Rights Reserved</p>

									<p style="font-size:12px;margin:0;color:#999">Apartemen Gateway Ruko D3 Jl.Ahmad Yani Cicadas Bandung<br />
									Cibeunying Kidul Bandung<br />
									Jawa Barat – 40125<br />
									(022) 205 22224</p>
									</td>
									<td style="text-align:center;"><a href="https://www.facebook.com/BillionaireStore.co.id/"><img alt="" src="http://billionairestore.co.id/static/img/facebook-mail-icon.png" width="24" /></a> <a href="https://www.instagram.com/billionairestorecoid/" style="margin:0 20px;"><img alt="" src="http://billionairestore.co.id/static/img/ig-mail-icon.png" width="24" /></a> <a href="https://www.youtube.com/channel/UCdJzNQFDePtiSr9778I_ezw"><img alt="" src="http://billionairestore.co.id/static/img/youtube-mail-icon.png" width="24" /></a></td>
								</tr>
							</tbody>
						</table>

						<table style="margin-top:20px;">
							<tbody>
								<tr>
									<td style="text-align:center;border-top:1px solid #ccc;font-size:12px;padding-top:10px;">Anda memperoleh e-mail ini karena pemesanan Anda di Billionairestore.co.id. Anda bisa mengubah&nbsp;pengaturan notifikasi kapanpun.<br />
									Harap jangan membalas e-mail ini, karena e-mail ini dikirimkan secara otomatis oleh sistem.</td>
								</tr>
							</tbody>
						</table>
					</td>
				</tr>
			</table>
		</table>
	</td>
</tr>
</tbody>
</table>
