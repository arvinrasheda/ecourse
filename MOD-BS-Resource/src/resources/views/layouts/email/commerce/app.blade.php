<!DOCTYPE html>
<html lang="en" class="default-style">

<body>
    <div class="page-loader">
        <div class="bg-primary"></div>
    </div>

    <!-- Layout wrapper -->
    <div class="layout-wrapper layout-2">
        <div class="layout-inner">
            <!-- Layout container -->
            <div class="layout-container">
                <!-- Layout navbar -->
                @include('layouts.email.commerce.header')
                <!-- / Layout navbar -->

                <!-- Layout content -->
                <div class="layout-content">
                    <!-- Content -->
                        @yield('content')
                    <!-- / Content -->

                    <!-- Layout footer -->
                    @include('layouts.email.commerce.footer')
                    <!-- / Layout footer -->
                </div>
                <!-- Layout content -->
            </div>
            <!-- / Layout container -->
        </div>
        <!-- Overlay -->
        <div class="layout-overlay layout-sidenav-toggle"></div>
    </div>
    <!-- / Layout wrapper -->
</body>
</html>