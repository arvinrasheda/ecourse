<title>DataTables - Tables - Appwork</title>

<meta charset="utf-8">
<meta http-equiv="x-ua-compatible" content="IE=edge,chrome=1">
<meta name="description" content="">
<meta name="viewport" content="width=device-width, initial-scale=1.0, user-scalable=no, minimum-scale=1.0, maximum-scale=1.0">
<link rel="icon" type="image/x-icon" href="favicon.ico">

<link href="https://fonts.googleapis.com/css?family=Roboto:300,300i,400,400i,500,500i,700,700i,900" rel="stylesheet">

<!-- Icon fonts -->
<link rel="stylesheet" href="{{ cdn_asset('admin/assets/vendor/fonts/fontawesome.css') }}">
<link rel="stylesheet" href="{{ cdn_asset('admin/assets/vendor/fonts/ionicons.css') }}">
<link rel="stylesheet" href="{{ cdn_asset('admin/assets/vendor/fonts/linearicons.css') }}">
<link rel="stylesheet" href="{{ cdn_asset('admin/assets/vendor/fonts/open-iconic.css') }}">
<link rel="stylesheet" href="{{ cdn_asset('admin/assets/vendor/fonts/pe-icon-7-stroke.css') }}">

<!-- Core stylesheets -->
<link rel="stylesheet" href="{{ cdn_asset('admin/assets/vendor/css/rtl/bootstrap.css') }}" class="theme-settings-bootstrap-css">
<link rel="stylesheet" href="{{ cdn_asset('admin/assets/vendor/css/rtl/appwork.css') }}" class="theme-settings-appwork-css">
<link rel="stylesheet" href="{{ cdn_asset('admin/assets/vendor/css/rtl/theme-corporate.css') }}" class="theme-settings-theme-css">
<link rel="stylesheet" href="{{ cdn_asset('admin/assets/vendor/css/rtl/colors.css') }}" class="theme-settings-colors-css">
<link rel="stylesheet" href="{{ cdn_asset('admin/assets/vendor/css/rtl/uikit.css') }}">
<link rel="stylesheet" href="{{ cdn_asset('admin/assets/css/demo.css') }}">

<script src="{{ cdn_asset('admin/assets/vendor/js/material-ripple.js') }}"></script>
<script src="{{ cdn_asset('admin/assets/vendor/js/layout-helpers.js') }}"></script>

<!-- Theme settings -->
<!-- This file MUST be included after core stylesheets and layout-helpers.js in the <head> section -->
<script src="{{ cdn_asset('admin/assets/vendor/js/theme-settings.js') }}"></script>
<script>
    window.themeSettings = new ThemeSettings({
        cssPath: '{{ cdn_asset("admin/assets/vendor/css/rtl/") }}',
        themesPath: '{{ cdn_asset("admin/assets/vendor/css/rtl/") }}'
    });
</script>

<!-- Core scripts -->
<script src="{{ cdn_asset('admin/assets/vendor/js/pace.js') }}"></script>
<script src="https://ajax.googleapis.com/ajax/libs/jquery/3.2.1/jquery.min.js"></script>

<!-- Libs -->
<link rel="stylesheet" href="{{ cdn_asset('admin/assets/vendor/libs/perfect-scrollbar/perfect-scrollbar.css') }}">
<link rel="stylesheet" href="{{ cdn_asset('admin/assets/vendor/libs/datatables/datatables.css') }}">

@stack('styles')