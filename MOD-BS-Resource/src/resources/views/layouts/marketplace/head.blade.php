
        <title>{{ config('app.name', 'BS System') }}</title>

        <meta charset="utf-8">
        <meta http-equiv="x-ua-compatible" content="IE=edge,chrome=1">
        <meta name="description" content="">
        <meta name="viewport" content="width=device-width, initial-scale=1.0, user-scalable=no, minimum-scale=1.0, maximum-scale=1.0">
        <link rel="icon" type="image/x-icon" href="favicon.ico">

        <!-- Icon fonts -->
        <link rel="stylesheet" href="<?=cdn_asset('vendor/fonts/fontawesome.css')?>">
        <link rel="stylesheet" href="<?=cdn_asset('vendor/fonts/ionicons.css');?>">
        <link rel="stylesheet" href="<?=cdn_asset('vendor/fonts/linearicons.css');?>">
        <link rel="stylesheet" href="<?=cdn_asset('vendor/fonts/pe-icon-7-stroke.css');?>">

        <!-- Core stylesheets -->
        <link rel="stylesheet" href="<?=cdn_asset('vendor/css/bootstrap.css');?>" class="theme-settings-bootstrap-css">
        <link rel="stylesheet" href="<?=cdn_asset('vendor/css/appwork.css');?>" class="theme-settings-appwork-css">
        <link rel="stylesheet" href="<?=cdn_asset('vendor/css/theme-billy.css');?>" class="theme-settings-theme-css">
        <link rel="stylesheet" href="<?=cdn_asset('vendor/css/colors.css');?>" class="theme-settings-colors-css">
        <link rel="stylesheet" href="<?=cdn_asset('vendor/css/uikit.cs');?>s">

        <!-- Core scripts -->
        <script src="<?=cdn_asset('vendor/js/pace.js');?>"></script>

        <!-- Page -->
        <link rel="stylesheet" href="<?=cdn_asset('css/shop.css');?>">

        <!-- Libs -->
        <link rel="stylesheet" href="<?=cdn_asset('vendor/libs/typeahead-js/typeahead.css');?>">
        
        
        @section('head_script')
        @show