<div class="card mb-3">
    <div class="card-body">
        <div class="media align-items-center">
            <img src="../assets/img/no-profile.png" alt="" class="ui-w-60 rounded-circle">
            <div class="media-body ml-3">
                <h5 class="mb-2">Adi Rizky</h5>
                <a href="pengaturan-akun.html" class="small text-muted">
                    <i class="ion ion-md-create"></i>
                    Ubah Profil
                </a>
            </div>
        </div>
        <hr>
        <div class="media align-items-start mb-3">
            <a href="komisi.html">
                <img src="<?= cdn_asset('img/ranking.png'); ?>" alt="" class="ui-w-30">
                <div class="media-body ml-3">
                    <div class="small text-muted">Peringkat</div>
                    <a href="komisi.html" class="text-primary font-weight-semibold">Bronze</a>
                </div>
            </a>              
        </div>
        <div class="media align-items-start mb-3">
            <a href="poin.html">
                <img src="<?= cdn_asset('img/rich.png'); ?>" alt="" class="ui-w-30">
                <div class="media-body ml-3">
                    <div class="small text-muted">Poin User</div>
                    <a href="poin.html" class="text-primary font-weight-semibold">800</a>
                </div>
            </a>
        </div>
        @if(ACAuth::isReseller())
        <div class="media align-items-start">
            <a href="poin.html">
                <img src="<?= cdn_asset('img/money.png'); ?>" alt="" class="ui-w-30">
                <div class="media-body ml-3">
                    <div class="small text-muted">Poin Reseller</div>
                    <a href="poin.html" class="text-primary font-weight-semibold">2000</a>
                </div>
            </a>
        </div>
        @endif
    </div>
</div>
<div class="card">
    <div class="side-menu my-2">
        <a href="{{ACAuth::isReseller()?route('reseller.dashboard'):route('member.dashboard')}}" class="list-side-menu list-side-menu-action {{is_route(['member.dashboard','reseller.dashboard'])}}">
            <i class="ion ion-md-speedometer mr-2"></i>
            {{__('marketplace.dashboard')}}
        </a>            
        <a href="transaksi.html" class="list-side-menu list-side-menu-action">
            <i class="ion ion-md-paper mr-2"></i>
            Transaksi
        </a>
        <a href="notifikasi.html" class="list-side-menu list-side-menu-action {{is_route('member.notification')}}">
            <i class="ion ion-md-notifications mr-2"></i>
            Notifikasi
        </a>
        <a href="poin.html" class="list-side-menu list-side-menu-action">
            <i class="fas fa-coins mr-2"></i>
            Poin
        </a>        
        <a href="my-library.html" class="list-side-menu list-side-menu-action">
            <i class="ion ion-md-bookmark mr-2"></i>
            My Library
        </a>
        @if(ACAuth::isReseller())
        <a href="leaderboard.html" class="list-side-menu list-side-menu-action">
            <i class="ion ion-md-trophy mr-2"></i>
            Leaderboard
        </a>            
        <a href="link-produk.html" class="list-side-menu list-side-menu-action">
            <i class="ion ion-ios-link mr-2"></i>
            Link
        </a>
        <a href="input-order.html" class="list-side-menu list-side-menu-action">
            <i class="ion ion-ios-log-in mr-2"></i>
            Input Order
        </a>
        <a href="#rekapitulasi-dashboard" class="list-side-menu list-side-menu-action d-flex justify-content-between" data-toggle="collapse" aria-expanded="true">                
            <span>
                <i class="ion ion-md-analytics mr-2"></i>
                Rekapitulasi
            </span>
            <div class="collapse-icon"></div>
        </a>
        <div class="collapse show" id="rekapitulasi-dashboard">
            <a href="rekapitulasi-omset.html" class="list-side-menu list-side-menu-action">
                <span class="pl-3">
                    Omset
                </span>
            </a>
            <a href="rekapitulasi-produk-terjual.html" class="list-side-menu list-side-menu-action">
                <span class="pl-3">
                    Produk Terjual
                </span>                  
            </a>
            <a href="rekapitulasi-transaksi.html" class="list-side-menu list-side-menu-action">
                <span class="pl-3">
                    Transaksi
                </span>
            </a>
        </div>
        <a href="komisi.html" class="list-side-menu list-side-menu-action">
            <i class="ion ion-md-wallet mr-2"></i>
            Komisi                
        </a>
        <a href="marketing-kit.html" class="list-side-menu list-side-menu-action">
            <i class="ion ion-md-briefcase mr-2"></i>
            Marketing Kit
        </a>
        @endif
        <a href="pengaturan-akun.html" class="list-side-menu list-side-menu-action">
            <i class="ion ion-md-settings mr-2"></i>
            Pengaturan Akun           
        </a>
        <a href="{{route('auth.logout',['apps_code' => config('bssystem.cur_apps.apps_code')])}}" class="list-side-menu list-side-menu-action">
            <i class="ion ion-md-power mr-2"></i>
            Keluar   
        </a>
    </div>
</div>