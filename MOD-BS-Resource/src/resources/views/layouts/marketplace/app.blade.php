<!DOCTYPE html>
<html lang="{{ str_replace('_', '-', app()->getLocale()) }}" class="default-style">
    <head>
        @include('layouts.marketplace.head')
    </head>
    <body>
        
        @include('layouts.marketplace.header')
        @include('layouts.marketplace.sidebarmenu_mobile')
        
        @yield('content')
        
        @include('layouts.marketplace.footer')
    </body>
</html>
