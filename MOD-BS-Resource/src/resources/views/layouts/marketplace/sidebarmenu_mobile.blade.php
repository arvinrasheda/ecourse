<!-- START -- sidebar menu di mobile Apps -->
<div class="modal modal-full fade" id="menu">
    <div class="modal-dialog">

        <button type="button" class="close" data-dismiss="modal" aria-label="Close">×</button>

        <form class="modal-content" style="overflow-x: hidden">

            <div class="w-100" style="top: 0; position: absolute;">
                @if(ACAuth::isLogin())
                <div class="p-3 w-100 bg-white shadow-sm">
                    <div class="media align-items-center">
                        <img src="{{cdn_asset('img/avatars/1.png')}}" class="d-block ui-w-40 rounded-circle" alt>
                        <div class="media-body ml-3">
                            <div class="h5 mb-1">Adi Rizky</div>
                            <div class="text-primary"><i class="fas fa-coins mr-1"></i><span class="font-weight-semibold">800</span> Point</div>
                        </div>
                        <a href="pengaturan-akun.html" class="btn btn-round btn-outline-dark borderless icon-btn"><i class="ion ion-md-settings"></i></a>
                    </div>
                </div>
                @else
                <div class="p-3 w-100 bg-white shadow-sm">
                    <div class="d-flex align-items-center justify-content-around">
                        <a href="{{route('auth.login',['apps_code'=>config('bssystem.cur_apps.apps_code')])}}" class="btn btn-outline-primary btn-block">Masuk</a>
                        <span class="text-muted mx-4">atau</span>
                        <a href="{{route('auth.register',['apps_code'=>config('bssystem.cur_apps.apps_code')])}}" class="btn btn-info btn-block">Daftar</a>
                    </div>
                </div>
                @endif
                <div class="mt-1">
                    <h5 class="p-3 m-0">Menu</h5>
                    <div class="side-menu">
                        <a href="{{route('home')}}" class="list-side-menu list-side-menu-action">
                            <i class="ion ion-md-home mr-2"></i>
                            Beranda
                        </a>
                        <a href="#shop-filters-1" class="list-side-menu list-side-menu-action d-flex justify-content-between menu-head" data-toggle="collapse">
                            <span>
                                <i class="ion ion-md-apps mr-2"></i>
                                Kategori
                            </span>
                            <div class="collapse-icon"></div>
                        </a>
                        <div class="collapse" id="shop-filters-1">
                            <a href="kategori.html" class="list-side-menu list-side-menu-action">
                                <span class="pl-3">
                                    Promo
                                </span>                  
                            </a>
                            <a href="kategori.html" class="list-side-menu list-side-menu-action">
                                <span class="pl-3">
                                    Buku
                                </span>  
                            </a>
                            <a href="kategori.html" class="list-side-menu list-side-menu-action">
                                <span class="pl-3">
                                    Membership
                                </span>  
                            </a>
                        </div>

                        @if(ACAuth::isLogin())
                        <a href="{{ACAuth::isReseller()?route('reseller.dashboard'):route('member.dashboard')}}" class="list-side-menu list-side-menu-action active">
                            <i class="ion ion-md-speedometer mr-2"></i>
                            Dashboard
                        </a>            
                        <a href="transaksi.html" class="list-side-menu list-side-menu-action">
                            <i class="ion ion-md-paper mr-2"></i>
                            Transaksi
                        </a>
                        <a href="notifikasi.html" class="list-side-menu list-side-menu-action">
                            <i class="ion ion-md-notifications mr-2"></i>
                            Notifikasi
                        </a>                        
                        <a href="poin.html" class="list-side-menu list-side-menu-action">
                            <i class="fas fa-coins mr-2"></i>
                            Poin
                        </a>                       
                        <a href="my-library.html" class="list-side-menu list-side-menu-action">
                            <i class="ion ion-md-bookmark mr-2"></i>
                            My Library
                        </a>
                        
                        @if(ACAuth::isReseller())
                        <a href="leaderboard.html" class="list-side-menu list-side-menu-action">
                            <i class="ion ion-md-trophy mr-2"></i>
                            Leaderboard
                        </a>            
                        <a href="link-produk.html" class="list-side-menu list-side-menu-action">
                            <i class="ion ion-ios-link mr-2"></i>
                            Link
                        </a>
                        <a href="input-order.html" class="list-side-menu list-side-menu-action">
                            <i class="ion ion-ios-log-in mr-2"></i>
                            Input Order
                        </a>
                        <a href="#rekapitulasi-dashboard" class="list-side-menu list-side-menu-action d-flex justify-content-between" data-toggle="collapse">
                            <span>
                                <i class="ion ion-md-analytics mr-2"></i>
                                Rekapitulasi
                            </span>
                            <div class="collapse-icon"></div>
                        </a>
                        <div class="collapse" id="rekapitulasi-dashboard">
                            <a href="rekapitulasi-omset.html" class="list-side-menu list-side-menu-action">
                                <span class="pl-3">
                                    Omset
                                </span>
                            </a>
                            <a href="rekapitulasi-produk-terjual.html" class="list-side-menu list-side-menu-action">
                                <span class="pl-3">
                                    Produk Terjual
                                </span>                  
                            </a>
                            <a href="rekapitulasi-transaksi.html" class="list-side-menu list-side-menu-action">
                                <span class="pl-3">
                                    Transaksi
                                </span>
                            </a>
                        </div>
                        <a href="komisi.html" class="list-side-menu list-side-menu-action">
                            <i class="ion ion-md-wallet mr-2"></i>
                            Komisi
                        </a>
                        <a href="marketing-kit.html" class="list-side-menu list-side-menu-action">
                            <i class="ion ion-md-briefcase mr-2"></i>
                            Marketing
                        </a>
                        @endif
                        
                        @endif

                        <a href="pusat-bantuan.html" class="list-side-menu list-side-menu-action">
                            <i class="ion ion-md-help-circle mr-2"></i>
                            Pusat Bantuan
                        </a>

                        @if(ACAuth::isLogin())
                        <a href="{{route('auth.logout',['apps_code' => config('bssystem.cur_apps.apps_code')])}}" class="list-side-menu list-side-menu-action">
                            <i class="ion ion-md-power mr-2"></i>
                            Keluar   
                        </a>
                        @endif
                    </div>
                </div>  

            </div>

        </form>
    </div>
</div>
<!-- END -- sidebar menu di mobile Apps -->