@if(!isset($hideFooterContent))
<div class="py-3 py-lg-5 mt-5 keuntungan">
    <div class="container">
        <div class="row justify-content-center text-center">
            <div class="col-sm-6 col-md-6 col-lg-3 my-3">
                <img src="<?= cdn_asset('img/shield.png'); ?>" alt="Keamanan bertransaksi" class="img-fluid rounded-circle bg-white p-4">
                <h5 class="font-weight-bold my-2">100% Aman</h5>
                <div class="text-muted">Billionaire Store menjamin keamanan anda bertransaksi</div>
            </div>
            <div class="col-sm-6 col-md-6 col-lg-3 my-3">
                <img src="<?= cdn_asset('img/payment-method.png'); ?>" alt="Kemudahan Pembayaran" class="img-fluid rounded-circle bg-white p-4">
                <h5 class="font-weight-bold my-2">Kemudahan Pembayaran</h5>
                <div class="text-muted">Billionaire Store menyediakan berbagai metode pembayaran untuk bertransaks</div>
            </div>
            <div class="col-sm-6 col-md-6 col-lg-3 my-3">
                <img src="<?= cdn_asset('img/support.png'); ?>" alt="Customer Service" class="img-fluid rounded-circle bg-white p-4">
                <h5 class="font-weight-bold my-2">Customer Support yang Responsif</h5>
                <div class="text-muted">CS Billionaire Store siap membantu Anda melalui e-mail, media sosial dan call center</div>
            </div>
            <div class="col-sm-6 col-md-6 col-lg-3 my-3">
                <img src="<?= cdn_asset('img/delivery-truck.png'); ?>" alt="Delivery Truck" class="img-fluid rounded-circle bg-white p-4">
                <h5 class="font-weight-bold my-2">Berbagai Jasa Pengiriman</h5>
                <div class="text-muted">Billionaire Store menyediakan berbagai pilihan jasa pengiriman dengan jangkauan nasional</div>
            </div>      
        </div>
    </div>
</div>
@endif

<!-- Footer -->
<nav class="footer bg-white pt-2"> 
    <div class="container pt-4">
        <div class="row">  
            <div class="col-lg-3 pr-lg-4 pb-4">
                <a href="javascript:void(0)" class="footer-text d-block text-large font-weight-bolder mb-3">Billionaire Store</a>
                <p>
                    Billionaire Store adalah sebuah perusahaan yang bergerak di bidang penulisan. Percetakan dan Penerbitan buku buku bisnis
                </p>
            </div>  
            <div class="col">
                <div class="row">
                    <div class="col-6 col-sm-6 col-md-4 col-lg-4 pb-4">
                        <div class="footer-text small font-weight-bold mb-3">BILLIONAIRE STORE</div>
                        <a href="{{route('mp.page.about')}}" class="footer-link d-block pb-2">Tentang Kami</a>
                        <a href="{{route('mp.page',['pageSlug'=>'privacy'])}}" class="footer-link d-block pb-2">Kebijakan Privasi</a>
                        <a href="{{route('mp.page.affiliate')}}" class="footer-link d-block pb-2">Program Reseller/Affiliasi</a>
                        <a href="{{route('mp.page.about',['#hubungi-kami'])}}" class="footer-link d-block pb-2">Hubungi Kami</a>
                    </div>
                    <div class="col-6 col-sm-6 col-md-4 col-lg-3 pb-4">
                        <div class="footer-text small font-weight-bold mb-3">LAYANAN PELANGGAN</div>
                        <a href="{{route('member.helpcenter')}}" class="footer-link d-block pb-2">Pusat Bantuan</a>
                        <a href="{{route('mp.page.howToBuy')}}" class="footer-link d-block pb-2">Cara Belanja</a>
                        <a href="{{route('mp.page',['pageSlug'=>'term-condtion'])}}" class="footer-link d-block pb-2">Syarat & Ketentuan</a>
                    </div>
                    <div class="col-6 col-sm-6 col-md-4 col-lg-3 pb-4">
                        <div class="footer-text small font-weight-bold mb-3">PEMBAYARAN</div>
                        <img src="{{cdn_asset('img/bca.png')}}" alt="" class="ui-w-60">
                        <img src="{{cdn_asset('img/bri.png')}}" alt="" class="ui-w-60">
                        <img src="{{cdn_asset('img/bni.png')}}" alt="" class="ui-w-60">
                        <img src="{{cdn_asset('img/mandiri.png')}}" alt="" class="ui-w-60">
                        <div class="footer-text small font-weight-bold my-3">PENGIRIMAN</div>
                        <img src="{{cdn_asset('img/courier-jne.jpg')}}" alt="" class="ui-w-60">
                        <img src="{{cdn_asset('img/courier-pos.jpg')}}" alt="" class="ui-w-60">
                    </div>
                    <div class="col-6 col-sm-6 col-md-4 col-lg-2 pb-4">
                        <div class="footer-text small font-weight-bold mb-3">IKUTI KAMI</div>
                        <a href="https://www.facebook.com/BillionaireStore.co.id" target="_blank" class="footer-link d-block pb-2">
                            <i class="fab fa-facebook mr-1"></i>
                            Facebook
                        </a>
                        <a href="https://www.instagram.com/billionarestore.id/" target="_blank" class="footer-link d-block pb-2">
                            <i class="fab fa-instagram mr-1"></i>
                            Instagram
                        </a>
                        <a href="https://www.youtube.com/channel/UCdJzNQFDePtiSr9778I_ezw#" target="_blank" class="footer-link d-block pb-2">
                            <i class="fab fa-youtube mr-1"></i>
                            Youtube
                        </a>
                    </div>
                </div>
            </div>
        </div>
    </div>

    <hr class="m-0">
    <div class="container py-2">
        <div>© 2018. All rights reserved</div>
    </div>
</nav>
<!-- / Footer -->

<!-- Core scripts -->
<script src="<?= cdn_asset('vendor/libs/jquery/jquery-3.3.1.min.js'); ?>"></script>
<script src="<?= cdn_asset('vendor/libs/popper/popper.js'); ?>"></script>
<script src="<?= cdn_asset('vendor/js/bootstrap.js'); ?>"></script>

<!-- Libs -->
<script src="<?= cdn_asset('js/dropdown-hover.js'); ?>"></script>
<script src="<?= cdn_asset('vendor/libs/typeahead-js/typeahead.bundle.min.js'); ?>"></script>
<script src="<?= cdn_asset('vendor/libs/handlebars/handlebars.min.js'); ?>"></script>

<!-- Page -->
<script src="<?= cdn_asset('js/shop.js'); ?>"></script>
<script src="<?= cdn_asset('js/typeahead.js'); ?>"></script>
                
@section('footer_script')
@show