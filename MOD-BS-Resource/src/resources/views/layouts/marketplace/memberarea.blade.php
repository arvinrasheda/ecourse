<!DOCTYPE html>
<html lang="{{ str_replace('_', '-', app()->getLocale()) }}" class="default-style">
    <head>
        @include('layouts.marketplace.head')
    </head>
    <body>
        <div class="page-loader">
            <div class="bg-primary"></div>
        </div>
        
        @include('layouts.marketplace.header')
        @include('layouts.marketplace.sidebarmenu_mobile')
        
        <div class="container-fluid mt-lg-5 mt-4">
            <div class="row">
                <div class="col-lg-3 d-none d-lg-block">
                    @include('layouts.marketplace.memberarea_sidebarmenu_desktop')
                </div>
                <div class="col-lg-9">                  
                    @yield('content')
                </div>            
            </div>
        </div>
        
        @include('layouts.marketplace.footer')
    </body>
</html>
