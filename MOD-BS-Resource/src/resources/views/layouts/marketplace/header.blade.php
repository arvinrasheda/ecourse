<div class="page-loader">
    <div class="bg-primary"></div>
</div>

<!-- START -- header -->
<div class="sticky-top">
    <nav class="navbar navbar-expand-md bg-white d-none d-md-block d-lg-block d-xl-block p-0 shadow-sm">
        <div class="container-fluid flex-lg-wrap">
            
            <a href="{{route('home')}}" class="navbar-brand py-3">
                <img src="<?= cdn_asset('img/logo-billionairestore.png'); ?>" alt="Billionaire Store" class="header-brand-img">
            </a>

            <button class="navbar-toggler" type="button" data-toggle="collapse" data-target=".shop-header">
                <span class="navbar-toggler-icon"></span>
            </button>

            <div class="shop-header navbar-collapse justify-content-center collapse">
                
                <div class="navbar-nav d-none d-md-block d-lg-block d-xl-block">
                    <div class="btn-group">
                        <button type="button" class="btn btn-link nav-billy text-dark font-weight-semibold dropdown-toggle hide-arrow borderless" data-toggle="dropdown">
                            <i class="ion ion-md-list mr-1"></i>
                            Kategori
                        </button>
                        <div class="dropdown-menu">
                            <a class="dropdown-item" href="{{route('mp.product.promo')}}"><i class="ion ion-md-pricetags mr-1"></i> Promo</a>
                            <a class="dropdown-item" href="kategori.html"><i class="ion ion-md-book mr-1"></i> Buku</a>
                            <a class="dropdown-item" href="kategori.html"><i class="ion ion-md-people mr-1"></i> Membership</a>
                        </div>
                    </div>
                </div>

                <form action="search.html" method="POST" class="col-lg-6 col-xl-7 px-2 mr-1">
                    <div class="input-group" id="search-input">
                        <input type="text" class="form-control typeahead" placeholder="Cari 3 di Billionaire Store...">
                        <input type="submit" class="d-none"/>
                        <div class="input-group-append">
                            <button class="btn btn-primary" type="submit">
                                Cari
                            </button>
                        </div>
                    </div>
                </form>

                <script id="produk-template" type="text/x-handlebars-template">
                    <a href="detail.html" class="d-flex align-items-center">
                    <img src="../assets/img/@{{foto}}" class="ui-w-30 mr-2">
                    <div>
                    <div>@{{produk}}</div>
                    <small class="text-muted">Rp@{{harga}}</small>
                    </div>
                    </a>
                </script>

                <script id="penulis-template" type="text/x-handlebars-template">
                    <a href="penulis.html">
                    <div class="media align-items-center">
                    <img src="../assets/img/@{{foto}}" class="ui-w-40 rounded-circle mr-2">
                    <div class="media-body text-truncate">@{{penulis}}</div>
                    </div>
                    </a>
                </script>

                <script id="empty-produk-template" type="text/x-handlebars-template">
                    <div class="billy-dropdown">
                    <span class="font-weight-semibold">Produk</span>              
                    </div>
                    <a href="search.html" class="tt-suggestion tt-none text-truncate"><strong>@{{query}}</strong> dalam Semua Kategori</a>
                    <a href="search.html" class="tt-suggestion tt-none text-truncate"><strong>@{{query}}</strong> dalam Buku</a>
                    <a href="search.html" class="tt-suggestion tt-none text-truncate"><strong>@{{query}}</strong> dalam Membership</a>
                </script>

                <script id="empty-penulis-template" type="text/x-handlebars-template">
                    <div class="billy-dropdown d-flex justify-content-between align-items-center">
                    <span class="font-weight-semibold">Penulis</span>
                    <a href="penulis.html" class="text-primary">Lihat Semua</a>
                    </div>
                    <div class="tt-suggestion tt-none text-truncate">
                    <span>Penulis tidak ditemukan..</span>              
                    </div>            
                </script>
                
                <!-- ----------- START -- Group Desktop Header menu ----------- -->
                <div class="navbar-nav align-items-center py-lg-1 ml-auto">
                    
                    <!-- START -- menu SHOPPING CART -->
                    @include('component.marketplace.header_shoppingcart')
                    <!-- END -- menu SHOPPING CART -->
                    
                    @if(ACAuth::isLogin())
                    <!-- START -- menu TRANSACTION -->
                    <div class="nav-item mx-lg-2 mx-xl-1">
                        <a href="transaksi.html" class="nav-link nav-billy align-middle" data-toggle="tooltip" data-placement="bottom" title="Transaksi">
                            <i class="pe-7s-note2 navbar-icon align-middle"></i>
                        </a>
                    </div>
                    <!-- END -- menu TRANSACTION -->
                    
                    <!-- START -- menu NOTIFICATION -->
                    @include('component.marketplace.header_notif')
                    <!-- END -- menu NOTIFICATION -->
                                        
                    <!-- START -- menu MY ACCOUNT -->
                    <div class="nav-item dropdown mx-xl-1">
                        <a href="javascript:void(0)" class="nav-link dropdown-toggle hide-arrow" data-toggle="dropdown">
                            <img src="{{cdn_asset('img/no-profile.png')}}" alt class="ui-w-30 rounded-circle align-middle">
                        </a>
                        <div class="dropdown-menu dropdown-menu-right" style="width:230px">
                            <a href="{{ACAuth::isReseller()?route('reseller.dashboard'):route('member.dashboard')}}" class="dropdown-item">
                                <div class="text-muted small">Profil saya</div>
                                <span class="font-weight-semibold">{{ ACAuth::user('name') }}</span>
                            </a>
                            <div class="dropdown-divider"></div>
                            @if(ACAuth::isReseller())
                            <a href="leaderboard.html" class="dropdown-item">
                                <div class="small text-muted">Peringkat</div>
                                <span class="text-primary font-weight-semibold">Bronze5</span>                
                            </a>
                            @endif
                            <div class="dropdown-divider"></div>
                            <a href="poin.html" class="dropdown-item d-flex justify-content-between align-items-center">
                                <div class="font-weight-light">Poin User</div>
                                <span class="text-primary font-weight-semibold">800</span>                
                            </a>
                            @if(ACAuth::isReseller())
                            <a href="poin.html" class="dropdown-item d-flex justify-content-between align-items-center">
                                <div class="font-weight-light">Poin Reseller</div>
                                <span class="text-primary font-weight-semibold">2000</span>                
                            </a>
                            @endif
                            <div class="dropdown-divider"></div>                                                        
                            <a href="{{ACAuth::isReseller()?route('reseller.dashboard'):route('member.dashboard')}}" class="dropdown-item">
                                <i class="ion ion-md-speedometer mr-2"></i>
                                Dashboard
                            </a>
                            <a href="pengaturan-akun.html" class="dropdown-item">
                                <i class="ion ion-md-settings mr-2"></i>
                                Pengaturan Akun
                            </a>
                            <a href="{{route('auth.logout',['apps_code' => config('bssystem.cur_apps.apps_code')])}}" class="dropdown-item">
                                <i class="ion ion-md-power mr-2"></i>
                                Keluar
                            </a>
                        </div>
                    </div>
                    <!-- END -- menu MY ACCOUNT -->
                    
                    @if(ACAuth::isReseller())
                    <!-- START -- menu MEMBER AREA -->
                    <?php /*<div class="nav-item dropdown">
                        <a href="javascript:void(0)" class="nav-link nav-billy dropdown-toggle hide-arrow" data-toggle="dropdown">
                            <i class="lnr lnr-menu navbar-icon align-middle"></i>
                        </a>
                        <div class="dropdown-menu dropdown-menu-right" style="width:200px">                            
                            <a href="{{route('dashboard')}}" class="dropdown-item">
                                <i class="ion ion-md-speedometer mr-2"></i>
                                Dashboard
                            </a>
                            <a href="leaderboard.html" class="dropdown-item">
                                <i class="ion ion-md-trophy mr-2"></i>
                                Leaderboard
                            </a>
                            <a href="link-produk.html" class="dropdown-item">
                                <i class="ion ion-ios-link mr-2"></i>
                                Link
                            </a>
                            <a href="input-order.html" class="dropdown-item">
                                <i class="ion ion-ios-log-in mr-2"></i>
                                Input Order                
                            </a>
                            <div class="dropdown-toggle">
                                <div class="dropdown-item">
                                    <i class="ion ion-md-analytics mr-2"></i>
                                    Rekapitulasi
                                </div>
                                <div class="dropdown-menu">
                                    <a class="dropdown-item" href="rekapitulasi-omset.html">Omset</a>
                                    <a class="dropdown-item" href="rekapitulasi-produk-terjual.html">Produk Terjual</a>
                                    <a class="dropdown-item" href="rekapitulasi-transaksi.html">Transaksi</a>
                                </div>
                            </div>
                            <a href="komisi.html" class="dropdown-item">
                                <i class="ion ion-md-wallet mr-2"></i>
                                Komisi                
                            </a>
                            <a href="my-library.html" class="dropdown-item">
                                <i class="ion ion-md-bookmark mr-2"></i>
                                My Library
                            </a>
                            <a href="marketing-kit.html" class="dropdown-item">
                                <i class="ion ion-md-briefcase mr-2"></i>
                                Marketing Kit                
                            </a>
                        </div>
                    </div> */ ?>
                    <!-- END -- menu MEMBER AREA -->
                    @endif
                    @else
                    <div class="nav-item ml-lg-1 ml-xl-4">
                      <a href="{{route('auth.login',['apps_code' => config('bssystem.cur_apps.apps_code'),'backlink'=>url()->full()])}}" class="btn btn-outline-primary btn-block">Masuk</a>
                    </div>
                    
                    <div class="nav-item ml-3">
                      <a href="{{route('auth.register',['apps_code' => config('bssystem.cur_apps.apps_code'),'backlink'=>url()->full()])}}" class="btn btn-info btn-block">Daftar</a>
                    </div>
                    @endif
                </div>
                <!-- ----------- END -- Group Desktop Header menu ----------- -->
            </div>
        </div>
    </nav>
    
    <!-- START -- HEADER MOBILE -->
    <div class="d-flex justify-content-center align-items-center bg-primary d-block d-md-none d-lg-none d-xl-none p-2 px-3">
        <a href="#" class="text-white" data-toggle="modal" data-target="#menu">
            <i class="lnr lnr-menu navbar-icon align-middle"></i>
        </a>
        <a href="{{route('home')}}" class="navbar-brand mx-2 mx-sm-3">
            <img src="{{cdn_asset('img/icon-billionairestore.png')}}" alt="Billionaire Store" class="header-brand-img mr-0">
        </a>
        <form action="search.html" method="POST" class="w-100" id="search-input">
            <input class="form-control input-round typeahead" id="" type="text" placeholder="Cari di Billionaire Store..." autocomplete="off">
            <input type="submit" class="d-none"/>
        </form>
        <a href="cart.html" class="text-white text-nowrap mx-2 mx-sm-3">
            <i class="lnr lnr-cart navbar-icon align-middle"></i>
            <span class="badge bg-white text-dark indicator">1</span>
        </a>
        <a href="notifikasi.html" class="text-white text-nowrap ml-1 ml-sm-0">
            <i class="pe-7s-bell navbar-icon align-middle"></i>
            <span class="badge bg-white badge-dot indicator"></span>
        </a>
    </div>
    <!-- END -- HEADER MOBILE -->
</div>
<!-- END -- header -->
