@if(Session::has('success'))
    <div class="demo-vertical-spacing-sm col-sm-12 p-4">
        <div class="alert alert-success alert-dismissible fade show">
            <button type="button" class="close" data-dismiss="alert">×</button>
            {!! Session::get('success') !!}
        </div>
    </div>
@endif

@if(Session::has('failed'))
<div class="demo-vertical-spacing-sm col-sm-12 p-4">
    <div class="alert alert-danger alert-dismissible fade show">
        <button type="button" class="close" data-dismiss="alert">×</button>
        {!! Session::get('failed') !!}
    </div>
</div>
   
@endif

@if($errors->all())
<div class="demo-vertical-spacing-sm col-sm-12 p-4">
    <div class="alert alert-info alert-dismissible fade show">
        <button type="button" class="close" data-dismiss="alert">×</button>
        @lang('global.alert_warning_error')
    </div>
</div>
  
@endif