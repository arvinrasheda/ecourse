<!DOCTYPE html>

<html lang="en" class="default-style">

<head>
@include('layouts.admin_area.head')
</head>

<body>
    <div class="page-loader">
        <div class="bg-primary"></div>
    </div>

    <!-- Layout wrapper -->
    <div class="layout-wrapper layout-2">
        <div class="layout-inner">
            <!-- Layout sidenav -->
            @include('layouts.admin_area.sidebar')
            <!-- / Layout sidenav -->

            <!-- Layout container -->
            <div class="layout-container">
                <!-- Layout navbar -->
                @include('layouts.admin_area.header')
                <!-- / Layout navbar -->

                <!-- Layout content -->
                <div class="layout-content">
                    <!-- Content -->
                        @yield('content')
                    <!-- / Content -->

                    <!-- Layout footer -->
                    @include('layouts.admin_area.footer')
                    <!-- / Layout footer -->
                </div>
                <!-- Layout content -->
            </div>
            <!-- / Layout container -->
        </div>

        <!-- Overlay -->
        <div class="layout-overlay layout-sidenav-toggle"></div>
    </div>
    <!-- / Layout wrapper -->
    @include('layouts.admin_area.script')
</body>

</html>