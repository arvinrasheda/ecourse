<!-- Core scripts -->
<script src="{{ cdn_asset('admin/assets/vendor/libs/popper/popper.js') }}"></script>
<script src="{{ cdn_asset('admin/assets/vendor/js/bootstrap.js') }}"></script>
<script src="{{ cdn_asset('admin/assets/vendor/js/sidenav.js') }}"></script>

<!-- Libs -->
<script src="{{ cdn_asset('admin/assets/vendor/libs/perfect-scrollbar/perfect-scrollbar.js') }}"></script>
<script src="{{ cdn_asset('admin/assets/vendor/libs/datatables/datatables.js') }}"></script>

<!-- Demo -->
<script src="{{ cdn_asset('admin/assets/js/demo.js') }}"></script>
<script src="{{ cdn_asset('admin/assets/js/tables_datatables.js') }}"></script>

@stack('scripts')