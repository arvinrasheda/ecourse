<!DOCTYPE html>
<html lang="{{ str_replace('_', '-', app()->getLocale()) }}" class="default-style">
    <head>
        <title>{{ config('app.name', 'BS - System') }}</title>

        <meta charset="utf-8">
        <meta http-equiv="x-ua-compatible" content="IE=edge,chrome=1">
        <meta name="description" content="">
        <meta name="viewport" content="width=device-width, initial-scale=1.0, user-scalable=no, minimum-scale=1.0, maximum-scale=1.0">
        <link rel="icon" type="image/x-icon" href="favicon.ico">

        <!-- Icon fonts -->
        <link rel="stylesheet" href="{{cdn_asset('vendor/fonts/fontawesome.css')}}">
        <link rel="stylesheet" href="{{cdn_asset('vendor/fonts/ionicons.css')}}">

        <!-- Core stylesheets -->
        <link rel="stylesheet" href="{{cdn_asset('vendor/css/bootstrap.css')}}" class="theme-settings-bootstrap-css">
        <link rel="stylesheet" href="{{cdn_asset('vendor/css/appwork.css')}}" class="theme-settings-appwork-css">
        <link rel="stylesheet" href="{{cdn_asset('vendor/css/theme-billy.css')}}" class="theme-settings-theme-css">
        <link rel="stylesheet" href="{{cdn_asset('vendor/css/colors.css')}}" class="theme-settings-colors-css">
        <link rel="stylesheet" href="{{cdn_asset('vendor/css/uikit.css')}}">

        <!-- Core scripts -->
        <script src="{{cdn_asset('vendor/js/pace.js')}}"></script>

        <!-- Page -->
        <link rel="stylesheet" href="{{cdn_asset('css/authentication.css')}}">
    </head>
    <body>
        <!-- Pace.js loader -->
        <div class="page-loader"><div class="bg-primary"></div></div>
        <!-- Content -->
        <div class="authentication-wrapper authentication-2 px-4">
            <div class="ui-bg-overlay bg-dark opacity-25"></div>
            <div class="authentication-inner py-5">
                @yield('content')
            </div>
        </div>
        <!-- / Content -->

        <!-- Core scripts -->
        <script src="{{cdn_asset('vendor/libs/jquery/jquery-3.3.1.min.js')}}"></script>
        <script src="{{cdn_asset('vendor/libs/popper/popper.js')}}"></script>
        <script src="{{cdn_asset('vendor/js/bootstrap.js')}}"></script>
    </body>
</html>