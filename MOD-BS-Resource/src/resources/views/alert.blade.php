@if(Session::has('alert'))
<?php $alert = Session::get('alert') ?>
<div class="alert alert-dark-{{ $alert['type'] }} alert-dismissible fade show">
    <button type="button" class="close" data-dismiss="alert">×</button>
    {!! $alert['message'] !!}
</div>
@endif
@if ($errors->any())
<div class="alert alert-dark-warning alert-dismissible fade show">
    <button type="button" class="close" data-dismiss="alert">×</button>
    <ul>
        @foreach ($errors->all() as $error)
        <li>{{ $error }}</li>
        @endforeach
    </ul>
</div>
@endif
