@extends('layouts.email.auth.app')

@section('content')
<!-- <h1 style="font-size:0px;font-weight:400">Anda baru saja melakukan permintaan untuk melakukan reset password untuk akun Billionaire Store Anda</h1> -->

<table cellpadding="0" cellspacing="0" style="margin: auto;" width="600">
	<tbody>
		<tr>
			<td>
			<div style="margin-top: 20px;">Hi {{('full name user')}},</div>
			</td>
		</tr>
		<tr>
			<td>
			<div style="margin-top: 20px;">Anda baru saja melakukan permintaan untuk melakukan reset password untuk akun Billionaire Store Anda. Klik tombol dibawah ini untuk melakukan reset password dan masukan password baru anda.</div>
			</td>
		</tr>
		<tr>
			<td align="center"><a href="!reset_url" style="display: inline-block; margin: 40px 0 40px 0; padding: 20px 48px; font-family: 'Open Sans', sans-serif; font-size: 18px; line-height: normal; text-transform: uppercase; text-decoration: none; color: #ffffff; background-color: #ff3500; border-radius: 4px;">Reset Password</a></td>
		</tr>
		<tr>
			<td>
			<div style="margin-bottom: 20px;">Jika Anda tidak melakukan permintaan untuk mereset password, silahkan abaikan email ini atau hubungi CS Billionaire Store untuk melakukan pengecekan. Reset password ini hanya berlaku sampai 1 jam kedepan.</div>
			</td>
		</tr>
		<tr>
			<td>Terimakasih,<br />
			Billionaire Store</td>
		</tr>
		<tr>
			<td>
			<div style="margin-top: 20px">Jika Anda bermasalah dengan klik tombol Reset Password, copy and paste URL di bawah ini di web browser Anda.</div>
			</td>
		</tr>
		<tr>
			<td>
			<div style="margin-bottom: 20px;width: 600px"><a href="!reset_url">!reset_url</a></div>
			</td>
		</tr>
	</tbody>
</table>
@endsection