@extends('layouts.email.commerce.app')

@section('content')
		
<table cellpadding="0" cellspacing="0" style="margin: auto;" width="600">
  <tbody>
    <tr>
      <td>
      	<!-- -->
      	<tr>
			<td>
			<p style="margin:20px 0 0 0; font-size: 14px; line-height: normal; color: #4a4a4a;">Halo [message:field-order:commerce-customer-shipping:commerce-customer-address:first_name],</p>

			<p style="margin:10px 0 0 0; font-size: 14px; line-height: normal; color: #4a4a4a;">Terima kasih atas kepercayaan Anda telah memesan Produk Billionaire Store. Mohon segera lakukan pembayaran sebelum:</p>

			<p style="font-weight:bold;font-size: 20px; background-color: #ff9200; padding: 5px;color: #fff;text-align:center;">!expired_date</p>

			<div style="text-align:center;">
			<p style="margin:10px 0 0 0; font-size: 14px; line-height: normal; color: #4a4a4a;">Lakukan pembayaran sebesar:</p>

			<p style="margin:20px 0 20px 0; font-size: 40px; line-height: normal; color: #4a4a4a; font-weight:bold;letter-spacing: 2px;">[message:field-order:commerce_order_total]</p>

			<p style="margin:0; font-size: 14px; line-height: normal; color: #4a4a4a;">Transfer Harus <strong>TEPAT</strong>&nbsp;hingga&nbsp;<strong>3 digit terakhir</strong></p>

			<p style="margin:0; font-size: 14px; line-height: normal; color: #4a4a4a; font-weight:bold;">Perbedaan Nilai Transfer akan Menghambat dalam Proses Verifikasi</p>

			<p style="margin:10px 0 20px 0; font-size: 14px; line-height: normal; color: #4a4a4a;">Pembayaran dapat dilakukan ke salah satu nomor rekening a/n PT. Kiblat Pengusaha Indonesia:</p>
			</div>

			<div style="margin:0 auto 15px;width: 80%;">
			<table cellspacing="0" style="width:100%;">
				<tbody>
					<tr>
						<td style="text-align:center;padding: 7px;" valign="top"><img alt="" src="http://billionairestore.co.id/static/img/mandiri.png" style="height: 50px;width: auto;display:inline-block;margin-top: -5px;margin-bottom: 10px;" /></td>
						<td style="text-align:center;font-size: 14px; line-height: normal; color: #4a4a4a;" valign="middle">Bank Mandiri, Bandung<br />
						<span style="font-size:16px;">130000-61-77771</span></td>
					</tr>
					<tr>
						<td style="text-align:center;padding: 7px;" valign="middle"><img alt="" src="http://billionairestore.co.id/static/img/bca.png" style="height: 35px;width: auto;margin-left: -23px;margin-top: 2px;" /></td>
						<td style="text-align:center;font-size: 14px; line-height: normal; color: #4a4a4a;" valign="middle">Bank BCA, Bandung<br />
						<span style="font-size:16px;">4372342777</span></td>
					</tr>
				</tbody>
			</table>
			</div>

			<p style="margin:10px 0 10px; font-size: 14px; line-height: normal; color: #4a4a4a;">Berikut Rincian tagihan pembayaran Anda:</p>

			<table cellspacing="0" style="width:100%;margin-bottom:15px;">
				<tbody>
					<tr>
						<td colspan="2" style="text-align:center;background-color: #ff9200;width: 100%;padding: 7px;vertical-align:middle;" valign="middle"><strong>No. invoice</strong> <strong style="font-size:30px">[message:field-order:field-invoice-number]</strong></td>
					</tr>
					<tr>
						<td style="font-size:14px;padding: 7px;border-bottom:1px solid #ccc;" width="30%">Waktu Pemesanan</td>
						<td style="font-size:14px;padding: 7px;border-bottom:1px solid #ccc;">[message:field-order:created:custom:j F Y H.i] WIB</td>
					</tr>
					<tr>
						<td style="font-size:14px;padding: 7px;border-bottom:1px solid #ccc;">Pembeli</td>
						<td style="font-size:14px;padding: 7px;border-bottom:1px solid #ccc;">[message:field-order:commerce-customer-shipping:commerce-customer-address:name_line]</td>
					</tr>
					<tr>
						<td style="font-size:14px;padding: 7px;border-bottom:1px solid #ccc;">Alamat Pengiriman</td>
						<td style="font-size:14px;padding: 7px;border-bottom:1px solid #ccc;">[message:field-order:commerce-customer-shipping:commerce-customer-address:thoroughfare] – [message:field-order:commerce-customer-shipping:commerce-customer-address:postal_code]<br />
						No. Telp: [message:field-order:commerce-customer-shipping:commerce-customer-address:phone_number]</td>
					</tr>
				</tbody>
			</table>

			<table cellspacing="0" style="background-color: #ff9200;width: 100%;">
				<tbody>
					<tr>
						<td style="font-size:14px;padding: 7px;width: 24.95%;border: 0;font-weight:bold;text-align:center;">Produk</td>
						<td style="font-size:14px;padding: 7px;width: 54.3%;font-weight:bold;border-top: 0;border-bottom: 0;">Nama Produk</td>
						<td style="font-size:14px;padding: 7px;width:13%;border: 0;border-left:0;font-weight:bold;text-align:center;">Jumlah</td>
					</tr>
				</tbody>
			</table>

			<div>!line_items</div>
			</td>
		</tr>
		<!-- e -->
		<tr>
			<td>
			<table cellspacing="0" style="width:100%;margin-top:10px;margin-bottom: 10px;">
				<tbody>
					<tr>
						<td style="font-weight:bold;font-size:14px; text-align:right; width: 77%;border-bottom:1px solid #ddd;">Subtotal</td>
						<td style="font-size:14px;padding: 7px 7px 7px 14px;border-bottom:1px solid #ddd;">!subtotal_product</td>
					</tr>
					<tr>
						<td style="font-weight:bold;font-size:14px; text-align:right; width: 77%;border-bottom:1px solid #ddd;">Ongkir</td>
						<td style="font-size:14px;padding: 7px 7px 7px 14px;;border-bottom:1px solid #ddd;">!shipment_price</td>
					</tr>
					<tr>
						<td style="font-weight:bold;font-size:14px; text-align:right; width: 77%;border-bottom:1px solid #ddd;">Potongan</td>
						<td style="font-size:14px;padding: 7px 7px 7px 14px;;border-bottom:1px solid #ddd;">!discount</td>
					</tr>
					<tr>
						<td style="font-weight:bold;font-size:14px; text-align:right; width: 77%;border-bottom:1px solid #ddd;">Harga total Belanja</td>
						<td style="font-weight:bold;font-size:14px;padding: 7px 7px 7px 14px;border-bottom:1px solid #ddd;">!total_order_product</td>
					</tr>
					<tr>
						<td style="font-size:12px; text-align:right; width: 77%;">Kode Pembayaran&nbsp;(Hanya dibebankan kepada pembeli)</td>
						<td style="font-weight:bold;font-size:14px;padding: 7px 7px 7px 14px;">!unique_code</td>
					</tr>
					<tr>
						<td style="font-weight:bold;font-size:14px; text-align:center; width: 77%;background-color: #efefef;">TOTAL PEMBAYARAN</td>
						<td style="font-weight:bold;font-size:14px;padding: 7px 7px 7px 14px;background-color: #ff9200;">[message:field-order:commerce_order_total]</td>
					</tr>
				</tbody>
			</table>
			</td>
		</tr>
		<!-- e -->
		<tr>
			<td>
			<p style="margin:10px 0 0; font-size: 14px; line-height: normal; color: #4a4a4a;">Setelah Anda melakukan pembayaran, silahkan konfirmasi pembayaran Anda dengan cara klik tombol konfirmasi dibawah ini :</p>

			<p style="margin:30px 0; line-height: normal;text-align:center;"><a href="http://billionairestore.co.id/konfirmasi-pembayaran?invoice_number=[message:field-order:field-invoice-number]" style="background-color:#ff3500; color: #fff; padding: 8px 20px; font-size: 18px;text-decoration:none;">KONFIRMASI PEMBAYARAN</a></p>

			<p style="margin:10px 0 0; font-size: 14px; line-height: normal; color: #4a4a4a;">Kami akan memverifikasi pembayaran Anda maksimal 1 X 24 jam pada Hari Kerja. Jika Anda mengalami kesulitan dalam pembayaran, silakan langsung Hubungi Billionaire Store dengan mengklik tombol di bawah ini:</p>

			<p style="margin:30px 0; line-height: normal;text-align:center;"><a href="http://billionairestore.co.id/" style="background-color:#ff3500; color: #fff; padding: 8px 20px; font-size: 18px;text-decoration:none;">CHAT CUSTOMER CARE</a></p>
			</td>
		</tr>	  
      </td>
	</tr>
  </tbody>
</table>

@endsection