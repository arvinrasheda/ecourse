@extends('layouts.email.commerce.app')

@section('content')
		
<table cellpadding="0" cellspacing="0" style="margin: auto;" width="600">
  <tbody>
    <tr>
      <td>
      	<!-- -->
      	<tr>
			<td>
			<p style="margin:20px 0 0 0; font-size: 14px; line-height: normal; color: #4a4a4a;">Halo [message:field-order:commerce-customer-shipping:commerce-customer-address:first_name],</p>

			<p style="margin:10px 0 0 0; font-size: 14px; line-height: normal; color: #4a4a4a;">Pembayaran Anda belum kami terima.</p>

			<p style="margin:10px 0 0 0; font-size: 14px; line-height: normal; color: #4a4a4a;">Segera bayar tagihan Anda sebelum pembelian dibatalkan pada</p>

			<p style="font-weight:bold;font-size: 20px; background-color: #ff9200; padding: 5px;color: #fff;text-align:center;">!expired_date</p>

			<p style="margin:10px 0 30px; font-size: 14px; line-height: normal; color: #4a4a4a;">Jika Anda telah melakukan pembayaran namun mengalami Kesulitan dalam melakukan Konfirmasi Pembayaran, Silahkan Anda lihat 4 Cara Praktis Melakukan Konfirmasi Pembayaran berikut ini :</p>

			<p style="margin:0 0 10px; font-size: 14px; line-height: normal; color: #4a4a4a;">4 Cara Praktis Melakukan Konfirmasi Pembayaran</p>

			<p><img alt="4 Cara Praktis Melakukan Konfirmasi Pembayaran" src="http://billionairestore.co.id/static/img/email-tutorial-konfirmasi.jpg" /></p>

			<p style="margin:10px 0 0; font-size: 14px; line-height: normal; color: #4a4a4a;">Setelah Anda melakukan pembayaran, silahkan konfirmasi pembayaran Anda dengan cara klik tombol konfirmasi dibawah ini :</p>

			<p style="margin:30px 0; line-height: normal;text-align:center;"><a href="http://billionairestore.co.id/konfirmasi-pembayaran?invoice_number=[message:field-order:field-invoice-number]" style="background-color:#ff3500; color: #fff; padding: 8px 20px; font-size: 18px;text-decoration:none;">KONFIRMASI PEMBAYARAN</a></p>

			<p style="margin:10px 0 0; font-size: 14px; line-height: normal; color: #4a4a4a;">Kami akan memverifikasi pembayaran Anda maksimal 1 X 24 jam pada Hari Kerja. Jika Anda mengalami kesulitan dalam pembayaran, silakan langsung Hubungi Billionaire Store dengan mengklik tombol di bawah ini:</p>

			<p style="margin:30px 0; line-height: normal;text-align:center;"><a href="http://billionairestore.co.id/" style="background-color:#ff3500; color: #fff; padding: 8px 20px; font-size: 18px;text-decoration:none;">CHAT CUSTOMER CARE</a></p>
			</td>
		</tr>
      	<!-- -->
      </td>
	</tr>
  </tbody>
</table>

@endsection