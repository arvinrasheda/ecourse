@extends('layouts.email.auth.app')

@section('content')
<table cellpadding="0" cellspacing="0" style="margin: auto;" width="600">
	<tbody>
		<tr>
			<td align="left">
			<h2 style="margin: 40px 0 20px 0; font-weight: normal; font-size: 24px; color: #4a4a4a;">Hi {{('full name reseller')}}</h2>

			<p style="margin: 0 0 20px 0; font-size: 14px; line-height: normal; color: #4a4a4a;">Selamat! Komisi Anda sudah berhasil dicairkan oleh Billionaire Store.</p>

			<p style="margin: 0 0 20px 0; font-size: 14px; line-height: normal; color: #4a4a4a;">Berikut ini keterangan pembayaran komisi Anda :</p>

			<table>
				<tbody>
					<tr>
						<td>Nama Reseller</td>
						<td>: {{ ('') }}</td>
					</tr>
					<tr>
						<td>No Rekening</td>
						<td>: {{ ('') }}</td>
					</tr>
					<tr>
						<td>Dibayar Tanggal</td>
						<td>: {{ ('') }}</td>
					</tr>
					<tr>
						<td>Komisi</td>
						<td>: {{ ('') }}</td>
					</tr>
				</tbody>
			</table>
			</td>
		</tr>
		<tr>
			<td>
			<p>Untuk melihat statistik penjualan Anda bersama kami, silahkan tekan tombol login dibawah ini</p>
			</td>
		</tr>
		<tr>
			<td align="center"><a href="http://reseller.labs.skyshi.com/" style="display: inline-block; margin: 0 0 40px 0; padding: 20px 48px; font-size: 18px; line-height: normal; text-transform: uppercase; text-decoration: none; color: #ffffff; background-color: #ff3500; border-radius: 4px;">Login</a></td>
		</tr>
		<tr>
			<td>
			<p>Salam sukses,<br />
			Billionaire Store</p>
			</td>
		</tr>
	</tbody>
</table>

@endsection