@extends('layouts.email.auth.app')

@section('content')

<table cellpadding="0" cellspacing="0" style="margin: auto;" width="600">
	<tbody>
		<tr>
			<td>
			<p style="margin: 40px 0 10px 0; font-size: 14px; line-height: normal; color: #4a4a4a;">Halo lagi {{('customer first name')}},</p>

			<p style="margin: 0px 0 20px 0; font-size: 14px; line-height: normal; color: #4a4a4a;">No invoice : {{('invoice number')}}</p>

			<div style="background-color: #ddd;overflow:hidden;">
			<div style="font-size:14px;padding: 7px;float:left;width:496px;border: 1px solid #ccc;font-weight:bold;">Nama Produk</div>

			<div style="font-size:14px;padding: 7px;float:left;width:69px;border: 1px solid #ccc;border-left:0;font-weight:bold;text-align:center;">Jumlah</div>
			</div>

			<div>line items</div>

			<p style="margin: 20px 0 10px 0; font-size: 14px; line-height: normal; color: #4a4a4a;">Pesanan Anda sudah Kami kirim dengan detail sebagai berikut :</p>

			<table style="margin: 10px 0 10px;font-size: 14px; line-height: normal; color: #4a4a4a;">
				<tbody>
					<tr>
						<td style="padding: 5px 5px 5px 0;">No Resi</td>
						<td style="padding: 5px">: {{('[shipping number/no resi')}}</td>
					</tr>
					<tr>
						<td style="padding: 5px 5px 5px 0;">Layanan Ekpedisi</td>
						<td style="padding: 5px">: {{('courier name')}}</td>
					</tr>
				</tbody>
			</table>

			<p style="margin: 0 0 10px 0; font-size: 14px; line-height: normal; color: #4a4a4a;">Semoga bermanfaat dan jangan lupa dipraktikkan.</p>

			<p style="margin: 0 0 20px 0; font-size: 14px; line-height: normal; color: #4a4a4a;">Kami tunggu belanja Anda yang berikutnya ya ^_^</p>
			</td>
		</tr>
		<tr>
			<td>
			<p style="margin: 0 0 5px; font-size: 14px; line-height: normal; color: #4a4a4a;">Billionaire Store</p>

			<p style="margin: 0 0 5px; font-size: 14px; line-height: normal; color: #4a4a4a;">Phone : (022) 20522224</p>

			<p style="margin: 0; font-size: 14px; line-height: normal; color: #4a4a4a;">WA : 0857 2210 5777</p>
			</td>
		</tr>
	</tbody>
</table>
@endsection