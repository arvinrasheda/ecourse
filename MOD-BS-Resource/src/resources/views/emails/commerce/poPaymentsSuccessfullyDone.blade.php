@extends('layouts.email.commerce.app')

@section('content')
		
<table cellpadding="0" cellspacing="0" style="margin: auto;" width="600">
    <tr>
  <tbody>
      <td>
      	<!-- -->
      	<tr>
			<td>
			<p style="margin: 40px 0 20px 0; font-size: 14px; line-height: normal; color: #4a4a4a;">Halo [message:field-order:commerce-customer-shipping:commerce-customer-address:first_name],</p>

			<p style="margin: 0 0 20px 0; font-size: 14px; line-height: normal; color: #4a4a4a;">Terimakasih sudah melakukan pembayaran untuk pesanan berikut :</p>

			<table cellspacing="0" style="width:100%;margin-bottom:15px;">
				<tbody>
					<tr>
						<td colspan="2" style="text-align:center;background-color: #ff9200;width: 100%;padding: 7px;vertical-align:middle;" valign="middle"><strong>No. invoice</strong> <strong style="font-size:30px">[message:field-order:field-invoice-number]</strong></td>
					</tr>
					<tr>
						<td style="font-size:14px;padding: 7px;border-bottom:1px solid #ccc;" width="30%">Waktu Pemesanan</td>
						<td style="font-size:14px;padding: 7px;border-bottom:1px solid #ccc;">[message:field-order:created:custom:j F Y H.i] WIB</td>
					</tr>
					<tr>
						<td style="font-size:14px;padding: 7px;border-bottom:1px solid #ccc;">Pembeli</td>
						<td style="font-size:14px;padding: 7px;border-bottom:1px solid #ccc;">[message:field-order:commerce-customer-shipping:commerce-customer-address:name_line]</td>
					</tr>
					<tr>
						<td style="font-size:14px;padding: 7px;border-bottom:1px solid #ccc;">Alamat Pengiriman</td>
						<td style="font-size:14px;padding: 7px;border-bottom:1px solid #ccc;">[message:field-order:commerce-customer-shipping:commerce-customer-address:thoroughfare] – [message:field-order:commerce-customer-shipping:commerce-customer-address:postal_code] No. Telp: [message:field-order:commerce-customer-shipping:commerce-customer-address:phone_number]</td>
					</tr>
				</tbody>
			</table>

			<table cellspacing="0" style="background-color: #ff9200;width: 100%;">
				<tbody>
					<tr>
						<td style="font-size:14px;padding: 7px;width: 20%;border: 0;font-weight:bold;text-align:center;">Produk</td>
						<td style="font-size:14px;padding: 7px;width: 54.3%;font-weight:bold;border-top: 0;border-bottom: 0;">Nama Produk</td>
						<td style="font-size:14px;padding: 7px;width:20%;border: 0;border-left:0;font-weight:bold;text-align:center;">Tanggal Kirim</td>
					</tr>
				</tbody>
			</table>

			<div>!line_items</div>
			</td>
		</tr>
		<!--  -->
		<tr>
			<td>
			<p>Silahkan Cek kembali pesanan Anda sesuai tanggal pengiriman yang telah di tentukan, Terimakasih.</p>
			</td>
		</tr>
      	<!--  -->
      </td>
	</tr>
  </tbody>
</table>

@endsection