<?php

//load menu routing member area jika tidak sedang di member area
if(config('bssystem.cur_apps.apps_code')!='member'){
    //menu ke member area
    Route::group(['domain' => config('bssystem.url.member')], function(){
        //member area
        //MOD-MEM-Member
        Route::get('dashboard')->name('member.dashboard');
        Route::get('apps-referral')->name('member.dashboard.appsReferral');
        Route::get('order')->name('member.dashboard.appsReferral');//transaksi
        Route::get('poin')->name('member.dashboard.appsReferral');//poin
        //
        //MOD-MEM-Account
        Route::get('account-setting')->name('member.member.dashboard.appsReferral');//My Account
        Route::get('notification')->name('dashboard.appsReferral');//notifikasi
        //
        //MOD-MEM-Order
        Route::get('order')->name('member.order');
        
        //MOD-MEM-My-Library
        Route::get('my-library')->name('member.dashboard.appsReferral');//My Library 
        
        //MOD-MEM-Help-Center
        Route::get('help-center')->name('member.helpcenter');//Help Center
        //
        ///MOD-MEM-Reseller - reseller area
        Route::get('reseller')->name('reseller.dashboard');//reseller dashboar
        Route::get('leaderboard')->name('reseller.leaderboard');//leaderboard
        Route::get('link')->name('reseller.link');//link product    
        Route::get('input-order')->name('reseller.linkOrder');//input order
        Route::get('recap-omset')->name('reseller.recapOmset');//rekapitulasi omset
        Route::get('recap-product')->name('reseller.recapProduct');//rekapituasi product
        Route::get('recap-transaksi')->name('reseller.recapTransaksi');//leaderboard    
        Route::get('commision')->name('reseller.appsReferral');//leaderboard     
        Route::get('marketing-kit')->name('reseller.appsReferral');//leaderboard    
    });
}

/**
 * =============================================================================
 */


//load menu routing Account Center jika tidak sedang di Account Center
if(config('bssystem.cur_apps.apps_code')!='ac'){
    //menu ke account center
    Route::group(['domain' => config('bssystem.url.ac')], function(){
        Route::get('/{apps_code}/login')->name('auth.login');
        Route::get('/{apps_code}/register')->name('auth.register');
        Route::get('/{apps_code}/logout')->name('auth.logout');
        
        Route::get('/{apps_code}/getsession')->name('sso.getSession');
        Route::post('/{apps_code}/validate')->name('sso.validate');
    });
    
    //API
    Route::group(['domain' => config('bssystem.url.acapi')], function(){
        Route::get('/{apps_code}/login')->name('auth.api.login');
        Route::get('/{apps_code}/register')->name('auth.api.register');
        Route::get('/{apps_code}/logout')->name('auth.api.logout');
    });
}

/**
 * =============================================================================
 */

//load menu routing Market Place jika tidak sedang di Market Place
if(config('bssystem.cur_apps.apps_code')!='bs'){
    //routing menu Market Place
    Route::group(['domain' => config('bssystem.url.bs')], function(){
        // ----- Home Controller -----------------------------------------------
        Route::get('/')->name('home');
        // ----- MOD-MP-Product ------------------------------------------------
        Route::get('/product')->name('mp.product.list');        
        Route::get('/category/{categorySlug}')->name('mp.product.listpercategory');
        
        Route::get('/product/{prductSlug')->name('mp.product.detail');
        Route::get('/product/search')->name('mp.product.search');//search auto complit
                        
        Route::get('/promo')->name('mp.product.promo');
        
        Route::get('/creator')->name('mp.product.creator.list');
        Route::get('/creator/{creatorId}')->name('mp.product.creator.detail');
        
        // ----- MOD-MP-Order --------------------------------------------------
        
        //CartController - shopping cart - Ajax & Web resource
        Route::prefix('cart')->group(function(){    
            Route::get('/{cartId?}')->name('mp.order.cart');//get cart
            Route::post('/{cartId}/add')->name('mp.order.cart.add');
            Route::delete('/{cartId}/delete')->name('mp.order.cart.delete');
            Route::put('/{cartId}/update')->name('mp.order.cart.update');
        });

        //proses checkout - Web Resource
        Route::prefix('checkout')->group(function(){
            //CheckoutController
            Route::get('/')->name('mp.order.checkout');

            //DirectCheckoutController
            Route::get('/direct')->name('mp.order.directcheckout');

            //DirectOrderCheckoutController
            Route::get('/direct')->name('mp.order.directordercheckout');

            //CheckoutFinishController
            Route::post('/review')->name('mp.order.review');
            Route::post('/createorder')->name('mp.order.createOrder');
            Route::get('/done/{orderId?}')->name('mp.order.done');
        });
        
        // ----- MOD-MP-Page ---------------------------------------------------
        //PageController
        Route::prefix('page')->group(function(){    
            Route::get('/about')->name('mp.page.about');
            Route::get('/affiliate-program')->name('mp.page.affiliate');
            Route::get('/how-to-buy')->name('mp.page.howToBuy');
            Route::get('/{pageSlug}')->name('mp.page');//get cart
        });
    });
    
    //routing api marketplace
    Route::group(['domain' => config('bssystem.url.bsapi')], function(){
        // ----- MOD-MP-Product ------------------------------------------------
        //ListProductController
        Route::get('/product')->name('mp.api.product.list');

        //DetailProductController - detail produt
        Route::get('/product/{productId}')->name('mp.api.product.detail');

        //CategoryController
        Route::get('/category')->name('mp.api.product.cagetory.list');
        Route::get('/category/{categoryId}')->name('mp.api.product.cagetory.detail');

        //PromoProductController - list product yang sedang promo
        Route::get('/promo')->name('mp.api.product.promo');
        
        // ----- MOD-MP-Order --------------------------------------------------
        //CartController - api shopping cart
        Route::prefix('cart')->group(function(){
            Route::get('/{cartId?}')->name('mp.api.order.cart');//get cart
            Route::post('/{cartId}/add')->name('mp.api.order.cart.add');
            Route::delete('/{cartId}/delete')->name('mp.api.order.cart.delete');
            Route::put('/{cartId}/update')->name('mp.api.order.cart.update');
        });
        //CheckoutController - api checkout / order
        Route::post('/checkout/createorder')->name('mp.api.order.review');
        
        // ----- MOD-MP-Page ---------------------------------------------------
        //PageController
        Route::get('/page/{pageSlug}')->name('mp.api.page');
    });
}