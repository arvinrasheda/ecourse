<?php

namespace BSSystem\MODBSResource\Services;

class Breadcrumb 
{
    /**
     * The attributes all the breadcrumbs
     *
     * @var array
     */
    protected $breadcrumb = [];
    protected $index = 0;
    
    /*
     * 
     */
    public function add($title,$link,$index=null)
    {
        $var = ['link'=>$link,'title'=>$title];
        $this->index++;
        $this->breadcrumb[$this->index] = $var;
        return true;
    }
    
    /*
     * 
     */
    public function getItems()
    {
        return $this->breadcrumb;
    }
    
    public function count()
    {
        return count($this->breadcrumb);
    }
    
    /**
     * 
     */
    public function generate($class = false)
    {
        if(!(isset($class['ol'])))$class['ol'] = '';
        if(isset($class['a'])){
            $class['a'] = 'class="'.$class['a'].'"';
        }else{
            $class['a'] = '';
        }
        if(!(isset($class['active'])))$class['active'] = 'active';
        $count = $this->count();
        $result = '';
        foreach ($this->breadcrumb as $key => $value) {
            $isLast = $this->index==$key;
            $result .= '<li class="breadcrumb-item '.($isLast?$class['active']:'').'">';
            if(!$isLast)$result .= '<a '.$class['a'].' href="'.$value['link'].'">';
            $result .= $value['title'];
            if(!$isLast)$result .= '</a>';
            $result .= '</li>';
        }
        
        return '<ol class="breadcrumb '.$class['ol'].'">'.$result.'</ol>';
    }
    
}
