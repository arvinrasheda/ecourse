<?php

namespace BSSystem\MODBSResource\Middleware;

use Closure;
use Facades\BSSystem\LIBOrder\Services\ShoppingCartSrv;
use Facades\BSSystem\MODWebAuth\Services\ACAuth;
use Facades\BSSystem\LIBMember\Repositories\MemberRepo;
use Facades\BSSystem\LIBProduct\Repositories\ProductRepo;

class InitMarketplaceFrontend
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */
    public function handle($request, Closure $next)
    {
        if(ACAuth::isLogin()){
            ACAuth::setLocalUser(MemberRepo::getOne('user_id',ACAuth::user('id')));
        }
        
        ShoppingCartSrv::checkInit();
        
        // load product category
        view()->share('productCategory',[
            ['id'=>1,'name'=>'Buku','slug'=>'buku'],
            ['id'=>2,'name'=>'Membership','slug'=>'mebership'],
        ]);
        
        
        return $next($request);
    }
}
