<?php

if (!function_exists('is_route')) {
    
    function is_route($route,$class='active')
    {
        if(is_array($route)){
            $isTrue = in_array(Route::current()->getName(), $route);
        }else{
            $isTrue = Route::current()->getName()==$route;
        }
        return $isTrue?$class:'';
 
    }
}