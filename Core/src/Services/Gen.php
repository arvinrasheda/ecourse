<?php

namespace BSSystem\Core\Services;

use Illuminate\Support\Facades\File;
use Illuminate\Support\Facades\Artisan;

/**
 * BSSystem form Generator
 */
class Gen
{

    protected $_generate_mode = 'mod';// mod atau lib
    
    protected $package_name = false;//nama module / package fisik
    protected $package_namespace_name = false;// nama namespace nya (tanpa strip)
    protected $package_path = false;//path ke folder /apps/Modules atau /vendor
    protected $cur_package_path = false;//path ke folder /apps/Modules/namamodule
    protected $package_namespace = false;//namespace ke /Apps/Modules
    
    protected $is_laravel_package = true;
    
    protected $model_name = false;
    protected $table_name = false;
    protected $service_name = false;
    protected $repository_name = false;

    protected $field_exclude = [];
    
    protected $gfile;


    public function __construct()
    {        
        $this->setPackageMode();
        $this->field_exclude = config('bssystem.generate_table_field_exclude');
    }
    
    /**
     * 
     * @param boolean $is_package true maka yg digenerate adalah laravel package
     */
    public function setPackageMode($is_laravel_package=false){
        
        $this->is_laravel_package = $is_laravel_package;        
        
        $config_dev_package_path = config('bssystem.dev_package_path');
        $config_namespace = config('bssystem.vendor.namespaces');
        $path = array_values($config_namespace);
        $namspace = array_keys($config_namespace);   
        
        //library harus berbentuk laravel package, jadi walaupun tidak menyertakan --package
        //jika library akan tetap diproses sebagai laravel package
        if($this->is_laravel_package || $this->_generate_mode == 'lib'){
            $this->package_path = base_path().DIRECTORY_SEPARATOR.$config_dev_package_path;  
            $this->package_namespace = $namspace[1];  
        }else{
            $this->package_path = $path[0][0];             
            $this->package_namespace = $namspace[0];            
        }
        
    }
    
    /**
     * set nama dan path package yang akan diproses
     * 
     * @param string $module_name
     */
    public function setPackage($package_name)
    {
        if($this->_generate_mode == 'mod'){
            $config_namespace = config('bssystem.vendor.namespaces');
            if($this->is_laravel_package){                
                $prefix = $config_namespace['BSSystem'][1];
            }else{
                $prefix = '';
            }
        }else{
            $config_namespace = config('bssystem.vendor.lib_namespace');
            $prefix = $config_namespace['BSSystem'][1];                
        }
        
        $this->package_name = $prefix.ucfirst($package_name);
        $this->package_namespace_name = str_replace('-', '', $this->package_name);
        $this->package_namespace .= '\\'.$this->package_namespace_name;
        $this->cur_package_path = 
                $this->package_path . $this->package_name . DIRECTORY_SEPARATOR;
        
        //jika laravel package
        if($this->is_laravel_package){
            $this->cur_package_path .= 'src'.DIRECTORY_SEPARATOR;
        }
    }
    
    /**
     * Cek apakah package sudah ada
     * 
     * @param string $package_name
     * 
     * @return boolean
     */
    public function isPackageExists($package_name = false)
    {
        if (!$package_name) $package_name = $this->package_name;
        
        if (File::exists($this->package_path . DIRECTORY_SEPARATOR . $package_name)) {
            return true;
        }

        return false;
    }


    /*
     * 
     */
    public function addToComposerPackage()
    {
        $config_dev_package_path = config('bssystem.dev_package_path');        
        $packageName = "BS-System/".$this->package_name;
        
        $composerJson = json_decode(File::get(base_path('composer.json')),true);
        $composerJson['repositories'][] = [
            "name" => $packageName,
            "type" => "path",
            "url" => $config_dev_package_path.$this->package_name,
            "options" => [
                "symlink" => true
            ]
        ];
        
        $composerJson['require'][$packageName] = "*";// "dev-master";
        
        if (File::put(base_path('composer.json') , str_replace('\/','/',json_encode($composerJson,JSON_PRETTY_PRINT)) )) {
            $this->generate_log[] = ['info', 'PROJECT composer.json : updated'];
            shell_exec('cd "'.base_path().'" && composer require '.$packageName.' --prefer-source');
            return true;
        }
        $this->generate_log[] = ['error', 'PROJECT composer.json : FAILED'];
        return false;
    }
    

    /**
     * generate struktur utama untuk module baru yang akan digenerate
     * 
     * @param string $module_name
     */
    public function generateDirStructure($package_name=false,$is_module=true)
    {
        if (!$package_name)
            $package_name = $this->package_name;
        
        File::makeDirectory($this->package_path . $package_name);
        
        //jika laravel package
        if($this->is_laravel_package){
            $package_name .= DIRECTORY_SEPARATOR.'src';        
            File::makeDirectory($this->package_path . $package_name);
        }
            

        $package_structure = $this->_generate_mode=='mod'?
            config('bssystem.vendor.generate_module_structure'):
            config('bssystem.vendor.generate_lib_structure');
        
        $this->_generateDirRec($package_name, $package_structure);
    }

    /**
     * fungsi rekursiv generate directory module
     * 
     * @param type $dir
     * @param type $module_structure
     * 
     * @return void
     */
    private function _generateDirRec($dir = '', $package_structure)
    {
        if (empty($package_structure))
            return false;
        foreach ($package_structure as $key => $value) {
            File::makeDirectory($this->package_path . $dir . DIRECTORY_SEPARATOR . $key);
            $this->_generateDirRec($dir . DIRECTORY_SEPARATOR . $key, $value);
        }
    }

    /**
     * set nama table yang akan digenerate CRUD form nya
     * 
     * @param string $table_name nama table
     */
    public function setTable($table_name)
    {
        $this->table_name = $table_name;
        $this->model_name = implode('',array_map('ucfirst',explode('_',$table_name)));
    }
    
    /**
     * 
     * 
     * @param array $field_exclude 
     *      list field yang di exclude dari getColumnListing
     * 
     * @return boolean
     */
    public function setFieldExclude($field_exclude)
    {
        if (!is_array($field_exclude))
            return false;

        foreach ($field_exclude as $val) {
            if (!empty($val))
                $this->field_exclude[] = $val;
        }

        return true;
    }
    /**
     * Get all generate process log
     * 
     * @return string
     */
    public function generateLog()
    {
        return isset($this->gfile)?$this->gfile->generate_log:[];
    }
    
}
