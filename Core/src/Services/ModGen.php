<?php

namespace BSSystem\Core\Services;

use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\File;
use Illuminate\Support\Facades\Artisan;
use BSSystem\Core\Services\GenFile as GFile;
use BSSystem\Core\Services\Gen;

//use FluentDOM;

/**
 * BSSystem module Generator
 */
class ModGen extends Gen
{
    protected $_generate_mode = 'mod';

    private $controller_name = false;
    private $controller_route_name = false;
    
    private $table_format = [];

    /**
     * 
     * @param string $controller_name
     */
    public function setController($controller_name)
    {
        //split/explode berdasarkan huruf besar
        //$chunks = preg_split('/(?=[A-Z])/', $controller_name);
        $controller_name = ucfirst($controller_name);
        $controller_postfix = 'Controller';
        
        //jika tidak diakhiri dengan controller, maka tambahkan
        if(substr_compare( $controller_name, $controller_postfix, -strlen( $controller_postfix ) ) !== 0){
            $this->controller_route_name = strtolower($controller_name);
            $controller_name = $controller_name.$controller_postfix;
        }else{
            $this->controller_route_name = strtolower(substr($controller_name,-10));
        }
        
        $this->controller_name = $controller_name;
        
    }


    /**
     * 
     * @param array $table_format
     *      [FIEL_DNAME => FIELD_DATA_TYPE, ...]
     */
    public function setTableFormat($table_format)
    {
        $this->table_format = $table_format;
    }


    /**
     * 
     * 
     * @param string $table 
     *      nama table
     * @return array 
     *      [FIELD => DATA_TYPE, FIELD => DATA_TYPE, ...]
     */
    public function getColumnListing($table)
    {
        $columns = DB::getSchemaBuilder()->getColumnListing($table);
        $column_type = false;
        if ($columns) {
            foreach ($columns as $field) {
                if (!in_array($field, $this->field_exclude))
                    $column_type[$field] = DB::connection()->getDoctrineColumn($table, $field)->getType()->getName();
            }
        }
        return $column_type;
    }

    /*
     * =========================================================================
     *      START GENERATE ELEMENT METHOD GROUP
     */

    /**
     * Generate seluruh file
     */
    public function generate()
    {
        $return = true;
        $this->gfile = new GFile(
                $this->package_name, //package name
                $this->cur_package_path, // path to active module
                $this->package_namespace, // namspace to package folder App/Module / Vendor
                $this->package_namespace_name // nama class namespace package/module nya
                );
        
        //[SOON] untuk membedakan generate new controller atau new module
        $new_module = true;
        
        //jika module belum ada maka generate dahulu struktur direktori utamanya
        if(!$this->isPackageExists()){
            $this->gfile->generate_log[] = ['info','Generate module structure : '.$this->package_name];
            $this->generateDirStructure();
        }else{
            $new_module = false;
            $this->gfile->generate_log[] = ['info','Add new controller to exsiting module : '.$this->package_name];
        }
        
        //jika struktur module external dan belum diset sebagai module external, maka set
        if(!$this->is_laravel_package && $this->isPackageExists($this->package_name . DIRECTORY_SEPARATOR . 'src')){
            $this->is_laravel_package = true;
        }
        
        //generate dan tambah model
        if(!$this->gfile->addModel($this->table_name,$this->model_name,$this->field_exclude)){
            $return = false;
        }
        
        $repository_name = $this->model_name.'Repo';
        $service_name = $this->model_name.'Srv';
        $response_name = $this->model_name.'Response';
        
        //generate dan tambah Repository
        if(!$this->gfile->addRepository(
            $this->model_name,
            $repository_name)
            ){
            $return = false;
        }
        
        //generate dan tambah Service
        if(!$this->gfile->addService($service_name,$repository_name)){
            $return = false;
        }
        
        //generate dan tambah view
        if(!$this->gfile->addView($this->table_format,$this->controller_name,$this->controller_route_name)){
            $return = false;
        }
        
        //generate dan tambah Respose
        if(!$this->gfile->addResponses($response_name)){
            $return = false;
        }
        
        //generate dan tambah controller
        if(!$this->gfile->addController(
            $this->controller_name,
            $this->controller_route_name,
            $repository_name,
            $service_name,
            $response_name)){
            $return = false;
        }
        
        //generate dan tambah route
        if(!$this->gfile->addRoute($this->controller_route_name,$this->controller_name)){
            $return = false;
        }        
        
                
        //tambah menu dari module/controller baru ke sidebar
//        if(!$this->addSidebarMenu($this->controller_name,$this->controller_route_name)){
//            $this->gfile->generate_log[] = ['error','Add Sidebar Menu : FAILED'];
//            $return = false;
//        }else{
//            $this->gfile->generate_log[] = ['info','Add Sidebar Menu : success'];
//        }
        
        if($return && $this->is_laravel_package){
            $this->gfile->addGitignore();
            $this->gfile->addReadMe();
            $this->gfile->addPackageComposerJson();
            $this->addToComposerPackage();
        }
        
        return $return;
    }
    
    private function addSidebarMenu($controller_name,$controller_route_name)
    {
        
        $genid = config('bssystem.generate_sidebar_menu_id');
        $gentag = config('bssystem.generate_sidebar_menu_tag');
        $sidebarlayaout = str_replace('.','/',config('bssystem.generate_sidebar_layouts'));
        
        
        $data = $this->gfile->getComponent('menuitem',[
            'CAPTION' => $controller_route_name,
            'ROUTE' => $controller_route_name
            ]);
        
        $path = resource_path('views/'.$sidebarlayaout.'.blade.php');
        $fd = FluentDOM::load($path,'text/html', 
                [FluentDOM\Loader\Options::ALLOW_FILE => TRUE]);
        
        //remove <!DOCTYPE 
        $fd->removeChild($fd->doctype); 
        //remove <html><body></body></html> 
        $fd->replaceChild($fd->firstChild->firstChild->firstChild, $fd->firstChild);
        
        foreach ($fd('//'.$gentag.'[@generateid = "'.$genid.'"]') as $node) {
            $node->appendXML($data);
            break;
        }
        
        $data = $fd->saveHtml();
        
        //ubah kembali karakter2 yang terencode oleh DOM
        $data = str_replace(['%7B%7B%20','%20%7D%7D'], ['{{ ',' }}'], $data);
        
        File::put($path,$data);
        
        return true;
    }
    

    
}
