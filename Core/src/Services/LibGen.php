<?php

namespace BSSystem\Core\Services;

use Illuminate\Support\Facades\File;
use Illuminate\Support\Facades\Artisan;
use BSSystem\Core\Services\GenFile as GFile;
use BSSystem\Core\Services\Gen;

//use FluentDOM;

/**
 * BSSystem form Generator
 */
class LibGen extends Gen
{

    protected $_generate_mode = 'lib';
    
    private $library_name = false;
    
    /*
     * =========================================================================
     *      START GENERATE ELEMENT METHOD GROUP
     */

    /**
     * Generate seluruh file
     */
    public function generate()
    {
        $return = true;
        $this->gfile = new GFile(
                $this->package_name,
                $this->cur_package_path,
                $this->package_namespace,
                $this->package_namespace_name // nama class namespace package/module nya
                );
        
        //[SOON] untuk membedakan generate new class didalam package atau new package
        $new_library = true;
        
        //generate library hanya untuk library external package
        $this->is_laravel_package = true;
        
        //jika module belum ada maka generate dahulu struktur direktori utamanya
        if(!$this->isPackageExists()){
            $this->gfile->generate_log[] = ['info','Generate Libray structure : '.$this->package_name];
            $this->generateDirStructure();
        }else{
            $this->gfile->generate_log[] = ['error','Library already exist : '.$this->package_name];
            return false;
//            $new_library = false;
//            $this->gfile->generate_log[] = ['info','Add new class to exsiting libray : '.$this->package_name];
        }
        
//        //jika struktur module external dan belum diset sebagai module external, maka set
//        if(!$this->is_laravel_package && $this->isPackageExists($this->package_name . DIRECTORY_SEPARATOR . 'src')){
//            $this->is_laravel_package = true;
//        }
        
        //generate dan tambah model
        if(!$this->gfile->addModel($this->table_name,$this->model_name,$this->field_exclude)){
            $return = false;
        }
        
        $repository_name = $this->model_name.'Repo';
        $service_name = $this->model_name.'Srv';
        
        
        //generate dan tambah Repository
        if(!$this->gfile->addRepository(
            $this->model_name,
            $repository_name)
            ){
            $return = false;
        }
        
        //generate dan tambah Service
        if(!$this->gfile->addService($service_name,$repository_name)){
            $return = false;
        }
        
        if($return){
            $this->gfile->addGitignore();
            $this->gfile->addReadMe();
            $this->gfile->addPackageComposerJson();
            if($return && $this->is_laravel_package){
                $this->addToComposerPackage();
            }
        }
        
        
        return $return;
    }
    
    
}
