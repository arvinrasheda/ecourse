<?php

namespace BSSystem\Core\Services;

use Illuminate\Support\Facades\File;
use Illuminate\Support\Facades\Artisan;

/**
 * generate component file di module atau lib
 */
class GenFile
{

    private $package_name = false; //nama module / package / library
    private $package_path = false; //path ke folder /apps/module/namamdule
    private $package_namespace = false; //namespace ke /apps/module/namamodule
    private $package_namespace_name = false; //nama kelas package nya
    private $component_path = false;
    public $generate_log = [];

    /**
     * 
     * @param string $package_name
     * @param string $package_path path to /app/modules/modulename/ atau /vendor/..../modulename/src
     * @param string $package_namespace namespace module
     */
    public function __construct(
        $package_name, 
        $package_path, 
        $package_namespace, 
        $package_namespace_name)
    {
        $this->package_name = $package_name;
        $this->package_path = $package_path;
        $this->package_namespace = $package_namespace;
        $this->package_namespace_name = $package_namespace_name;

        $this->component_path = __DIR__ . DIRECTORY_SEPARATOR . '..'
                . DIRECTORY_SEPARATOR . 'resource' . DIRECTORY_SEPARATOR . 'component' . DIRECTORY_SEPARATOR;
    }

    /**
     * check file exists on module path /app/modules/modulename/
     * 
     * @param string $file
     * @return boolean
     */
    public function isFileExist($file)
    {
        if (File::exists($this->package_path . $file)) {
            return true;
        }

        return false;
    }

    /**
     * load file component template untuk dirender dengan data yang diinput
     * 
     * @param string $component_name
     *      nama component tanpa akhiran .inc
     * @param string $data
     * @return string
     */
    public function getComponent($component_name, $data)
    {
        $html = '';
        if ($this->isComponentExists($component_name)) {

            $dataFormated = array_map(array($this, 'formatKeys'), array_keys($data));
            $replace = array_values($data);

            $html = File::get($this->component_path . $component_name . '.inc');
            $html = str_replace($dataFormated, $replace, $html);
        }
        return $html;
    }
    
    /**
     * 
     * @param string $component_name
     * @return boolean
     */
    public function isComponentExists($component_name)
    {
        if (File::exists($this->component_path . $component_name . '.inc')) {
            return true;
        }
        return false;
    }

    /**
     * fungsi untuk format key di getComponent().
     * fungsi untuk memformat key agar sesuai dengan key yg digunakan di file
     * template format
     * 
     * @param string $key
     * @return string
     */
    private function formatKeys($key)
    {
        return '{{' . strtoupper(trim($key)) . '}}';
    }

    
    /*
     * tambah file composer.json ke package yang digenerate
     */
    public function addPackageComposerJson()
    {
        $composerJsonPath = '..' . DIRECTORY_SEPARATOR . 'composer.json';
        
        if ($this->isFileExist($composerJsonPath)) {
            $this->generate_log[] = ['error', 'composer.json : FAILED - file already exists'];
            return true;
        }
        //format composer package nya
        $packageComposerJson = config('bssystem.vendor.package_composer_json');
        $packageComposerJson['name'] .= $this->package_name;
        $packageComposerJson['description'] .= $this->package_name;
        $packageComposerJson['autoload']['psr-4'] = [
                "BSSystem\\".$this->package_namespace_name."\\" => "src"
            ];
        
        if (File::put($this->package_path . $composerJsonPath , str_replace('\/','/',json_encode($packageComposerJson,JSON_PRETTY_PRINT)) )) {
            $this->generate_log[] = ['info', 'composer.json : created'];
            return true;
        }
        $this->generate_log[] = ['error', 'composer.json : FAILED - file format empty'];
        return false;
    }
    
    public function addReadMe()
    {
        
        $readMePath = '..' . DIRECTORY_SEPARATOR . 'README.md';

        if ($this->isFileExist($readMePath)) {
            $this->generate_log[] = ['error', 'README.md : FAILED - file already exists'];
            return true;
        }

        $data = [
            'PACKAGE_NAME' => $this->package_name
        ];

        $file = $this->getComponent('README.md', $data);

        if ($file) {
            if (File::put($this->package_path . $readMePath, $file)) {
                $this->generate_log[] = ['info', 'README.md : created'];
                return true;
            }
            $this->generate_log[] = ['error', 'README.md : FAILED'];
        } else {
            $this->generate_log[] = ['error', 'README.md : FAILED - file format empty'];
        }

        return false;
    }
    public function addGitignore()
    {
        
        $gitignorePath = '..' . DIRECTORY_SEPARATOR . '.gitignore';

        if ($this->isFileExist($gitignorePath)) {
            $this->generate_log[] = ['error', '.gitignore : FAILED - file already exists'];
            return true;
        }

        $file = $this->getComponent('gitignore', []);

        if ($file) {
            if (File::put($this->package_path . $gitignorePath, $file)) {
                $this->generate_log[] = ['info', '.gitignore : created'];
                return true;
            }
            $this->generate_log[] = ['error', '.gitignore : FAILED'];
        } else {
            $this->generate_log[] = ['error', '.gitignore : FAILED - file format empty'];
        }

        return false;
    }
    
    /**
     * Tambah model 
     * 
     */
    public function addModel($table_name, $model_name, $field_exclude)
    {
        $modelpath = 'Models' . DIRECTORY_SEPARATOR . $model_name . '.php';

        if ($this->isFileExist($modelpath)) {
            $this->generate_log[] = ['error', 'Model : FAILED - model already exists'];
            return false;
        }

        $field_exclude = "'" . implode("','", $field_exclude) . "'";
        $data = [
            'NAMESPACE' => $this->package_namespace,
            'CLASS_NAME' => $model_name,
            'TABLE_NAME' => $table_name,
            'FIELD_EXCLUDE' => $field_exclude
        ];

        $file = $this->getComponent('model', $data);

        if ($file) {
            if (File::put($this->package_path . $modelpath, $file)) {
                $this->generate_log[] = ['info', 'Model : created'];
                return true;
            }
            $this->generate_log[] = ['error', 'Model : FAILED'];
        } else {
            $this->generate_log[] = ['error', 'Model : FAILED - file format empty'];
        }

        return false;
    }
    
    
    /**
     * Tambah Repository 
     * 
     */
    public function addRepository($model_name,$repository_name)
    {
        $modelpath = 'Repositories' . DIRECTORY_SEPARATOR . $repository_name . '.php';
        
        if ($this->isFileExist($modelpath)) {
            $this->generate_log[] = ['error', 'Repository : FAILED - file already exists'];
            return false;
        }

        $data = [
            'NAMESPACE' => $this->package_namespace,
            'MODEL_NAME' => $model_name,
            'CLASS_NAME' => $repository_name
        ];

        $file = $this->getComponent('repository', $data);

        if ($file) {
            if (File::put($this->package_path . $modelpath, $file)) {
                $this->generate_log[] = ['info', 'Repository : created'];
                return true;
            }
            $this->generate_log[] = ['error', 'Repository : FAILED'];
        } else {
            $this->generate_log[] = ['error', 'Repository : FAILED - file format empty'];
        }

        return false;
    }
    /**
     * Tambah Service 
     * 
     */
    public function addService($service_name, $repository_name)
    {
        $modelpath = 'Services' . DIRECTORY_SEPARATOR . $service_name . '.php';

        if ($this->isFileExist($modelpath)) {
            $this->generate_log[] = ['error', 'Service : FAILED - service already exists'];
            return false;
        }

        $data = [
            'NAMESPACE' => $this->package_namespace,
            'CLASS_NAME' => $service_name,
            'REPOSITORY_NAME' => $repository_name
        ];

        $file = $this->getComponent('service', $data);

        if ($file) {
            if (File::put($this->package_path . $modelpath, $file)) {
                $this->generate_log[] = ['info', 'Service : created'];
                return true;
            }
            $this->generate_log[] = ['error', 'Service : FAILED'];
        } else {
            $this->generate_log[] = ['error', 'Service : FAILED - file format empty'];
        }

        return false;
    }
    
    /**
     * Tambah Responses 
     * 
     */
    public function addResponses($responsable_name)
    {
        $modelpath = 'Responses' . DIRECTORY_SEPARATOR . $responsable_name . '.php';

        if ($this->isFileExist($modelpath)) {
            $this->generate_log[] = ['error', 'Responsable : FAILED - file already exists'];
            return false;
        }

        $data = [
            'NAMESPACE' => $this->package_namespace,
            'CLASS_NAME' => $responsable_name
        ];

        $file = $this->getComponent('response', $data);

        if ($file) {
            if (File::put($this->package_path . $modelpath, $file)) {
                $this->generate_log[] = ['info', 'Responsable : created'];
                return true;
            }
            $this->generate_log[] = ['error', 'Responsable : FAILED'];
        } else {
            $this->generate_log[] = ['error', 'Responsable : FAILED - file format empty'];
        }

        return false;
    }

    /**
     * generate file controller (sementara hanya untuk grup Admin)
     * 
     * @param type $controller_name
     * @param type $controller_route_name
     * @param type $model_name
     * @return boolean
     */
    public function addController(
        $controller_name, 
        $controller_route_name, 
        $repository_name, 
        $service_name, 
        $response_name)
    {
        $controllerpath = 'Controllers' . DIRECTORY_SEPARATOR . $controller_name . '.php';

        if ($this->isFileExist($controllerpath)) {
            $this->generate_log[] = ['info', 'Controller : FAILED - controller already exists'];
            return false;
        }
        $data = [
            'NAMESPACE' => $this->package_namespace,
            'CLASS_NAME' => $controller_name,
            'ROUTE_NAME' => $controller_route_name,
            'REPOSITORY_NAME' => $repository_name,
            'SERVICE_NAME' => $service_name,
            'RESPONSABLE_NAME' => $response_name
        ];

        $file = $this->getComponent('controller', $data);

        if ($file) {
            if (File::put($this->package_path . $controllerpath, $file)) {
                $this->generate_log[] = ['info', 'Controller : created'];
                return true;
            }
            $this->generate_log[] = ['error', 'Controller : FAILED'];
        } else {
            $this->generate_log[] = ['error', 'Controller : FAILED - file format empty'];
        }

        return false;
    }

    /**
     * Generate atau append route ke module
     * 
     * @param string $controller_route_name
     *      nama routing utamanya
     * @param string $controller_name
     *      nama controller file dan class nya
     * @return boolean
     */
    public function addRoute($controller_route_name, $controller_name)
    {

        $data = [
            'ROUTE_NAME' => $controller_route_name,
            'CONTROLLER_NAME' => $controller_name
        ];

        $file = $this->getComponent('routes', $data);

        //create baru atau tambah script route
        if (!$this->isFileExist('routes.php')) {

            if (File::put($this->package_path . 'routes.php', "<?php\n\n" . $file)) {
                $this->generate_log[] = ['info', 'Route : created'];
            } else {
                $this->generate_log[] = ['error', 'Route : FAILED'];
                return false;
            }
        } else {
            if (File::append($this->package_path . 'routes.php', "\n\n" . $file)) {
                $this->generate_log[] = ['info', 'Route : updated'];
            } else {
                $this->generate_log[] = ['error', 'Route : FAILED'];
                return false;
            }
        }
        
        
        $file = $this->getComponent('routes_api', $data);
        
        //create baru atau tambah script route
        if (!$this->isFileExist( 'routes_api.php')) {

            if (File::put($this->package_path . 'routes_api.php', "<?php\n\n" . $file)) {
                $this->generate_log[] = ['info', 'Route api : created'];
            } else {
                $this->generate_log[] = ['error', 'Route protected : FAILED'];
                return false;
            }
        } else {
            if (File::append($this->package_path . 'routes_api.php', "\n\n" . $file)) {
                $this->generate_log[] = ['info', 'Route api : updated'];
            } else {
                $this->generate_log[] = ['error', 'Route api : FAILED'];
                return false;
            }
        }

        return true;
    }

    /**
     * generate CRUD view di admin
     * 
     * @param array $table_format
     *      list nama field dan tipe data dari table yang akan digenerate
     *          [FIELD => DATA_TYPE, FIELD => DATA_TYPE, ...]
     * @param type $controller_name
     * @param type $controller_route_name
     * @return boolean
     */
    public function addView($table_format, $controller_name, $controller_route_name)
    {

        //path ke /App/Modules/MODULE_NAME/resource/view/CONTROLLER_NAME
        $view_path = 'resources'.DIRECTORY_SEPARATOR.'views'. DIRECTORY_SEPARATOR . $controller_route_name . DIRECTORY_SEPARATOR;

        if ($this->isFileExist($view_path)) {
            $this->generate_log[] = ['error', 'View : FAILED - controller already exists'];
            return false;
        } else {
            File::makeDirectory($this->package_path . $view_path);
            
        }

        /*
         * ---------------------------------------------------------------------
         */

        $data = [
            'LAYOUT' => config('bssystem.generate_default_layout'),
            'MODULE_NAME' => $this->package_name,
            'CONTROLLER_NAME' => $controller_name,
            'CONTROLLER_ROUTE_NAME' => $controller_route_name
        ];
        
        //----cek default layout jika tidak ada maka generate
        $path = resource_path('views'.DIRECTORY_SEPARATOR. str_replace('.', '/',$data['LAYOUT']).'.blade.php'); 
        if (!File::exists($path)) {
            
            $layout_dir = explode('.', $data['LAYOUT']);
            array_pop($layout_dir);
            $layout_dir = implode(DIRECTORY_SEPARATOR, $layout_dir);
            if(!File::exists(resource_path('views'.DIRECTORY_SEPARATOR. $layout_dir))){
                File::makeDirectory(resource_path('views'.DIRECTORY_SEPARATOR.$layout_dir));
            }
            
            $def_layout = $this->getComponent('view.layout', []);
            File::put($path,$def_layout);
        }

        //----GENERATE view list

        $data_view_list = $this->generateViewList($table_format);
        $data['VIEW_COMPONENT_TABLE_HEADER'] = $data_view_list['header'];
        $data['VIEW_COMPONENT_TABEL_BODY'] = $data_view_list['body'];
        $file_list = $this->getComponent('view.list', $data);

        if ($file_list) {
            if (File::put($this->package_path . $view_path . 'list.blade.php', $file_list)) {
                $this->generate_log[] = ['info', 'View - list : created'];
            } else {
                $this->generate_log[] = ['error', 'View - list : FAILED'];
            }
        } else {
            $this->generate_log[] = ['error', 'View - list : FAILED - file format empty'];
        }

        //---GENERATE view form

        $data['VIEW_COMPONENT'] = $this->generateViewForm($table_format);
        $file_form = $this->getComponent('view.form', $data);

        if ($file_form) {
            if (File::put($this->package_path . $view_path . 'form.blade.php', $file_form)) {
                $this->generate_log[] = ['info', 'View - form : created'];
            } else {
                $this->generate_log[] = ['error', 'View - form : FAILED'];
            }
        } else {
            $this->generate_log[] = ['error', 'View - form : FAILED - file format empty'];
        }

        //---GENERATE view detail

        $data['VIEW_COMPONENT'] = $this->generateViewDetail($table_format);
        $file_form = $this->getComponent('view.detail', $data);

        if ($file_form) {
            if (File::put($this->package_path . $view_path . 'detail.blade.php', $file_form)) {
                $this->generate_log[] = ['info', 'View - detail : created'];
            } else {
                $this->generate_log[] = ['error', 'View - detail : FAILED'];
            }
        } else {
            $this->generate_log[] = ['error', 'View - detail : FAILED - file format empty'];
        }

        return true;
    }

    /**
     * 
     * @param array $table_format
     *      list nama field dan tipe data dari table yang akan digenerate
     *          [FIELD => DATA_TYPE, FIELD => DATA_TYPE, ...]
     * @return string
     */
    private function generateViewList($table_format)
    {
        $component = ['header'=>'','body'=>''];
        foreach ($table_format as $name => $datatype) {
            $component['header'] .= '<td>'.$this->captionify($name).'</td>';
            $component['body'] .= '<td>{{ $item->'.$name.' }}</td>';
        }
        return $component;
    }

    /**
     * 
     * @param array $table_format
     *      list nama field dan tipe data dari table yang akan digenerate
     *          [FIELD => DATA_TYPE, FIELD => DATA_TYPE, ...]
     * @return string
     */
    private function generateViewForm($table_format)
    {
        $component = '';
        foreach ($table_format as $name => $datatype) {
            $component_name = 'view.form.'.$datatype;
            if($this->isComponentExists($component_name)){
                $component .= $this->getComponent($component_name,['NAME'=>$name,'CAPTION'=>$this->captionify($name)]);
            }
        }
        return $component;
    }

    /**
     * 
     * @param array $table_format
     *      list nama field dan tipe data dari table yang akan digenerate
     *          [FIELD => DATA_TYPE, FIELD => DATA_TYPE, ...]
     * @return string
     */
    private function generateViewDetail($table_format)
    {
        $component = '';
        foreach ($table_format as $name => $datatype) {
            $component_name = 'view.detail.'.$datatype;
            if($this->isComponentExists($component_name)){
                $component .= $this->getComponent($component_name,['NAME','CAPTION'=>$this->captionify($name)]);
            }
        }
        return $component;
    }

    /**
     * mengubah format nama field menjadi judul kolom
     * 
     * @param string $field_name
     *      nama field table
     * @return string
     */
    public function captionify($field_name)
    {
        return ucwords(str_replace('_',' ',strtolower($field_name)));
    }

}
