<?php

namespace BSSystem\Core\Console\Command;

use Illuminate\Console\Command;
use BSSystem\Core\Services\ModGen;

class ModuleGenerator extends Command
{

    protected $sysGen;

    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'bssystem:modgen '
        . '{module? : nama module yang akan digenerate, jika module sudah ada maka akan dimasukan sebagai controller tambahan didalamnya} '
        . '{--controller= : [OPTIONAL] nama controller} '
        . '{--table= : [OPTIONAL] source table yang akan digenerate} '
        . '{--service= : [OPTIONAL] [BELUM DIIMPLEMENTASIKAN] nama service} '
        . '{--repository= : [OPTIONAL] [BELUM DIIMPLEMENTASIKAN] nama repository} '
        . '{--field_exclude= : [OPTIONAL] format : FIELDNAME1,FIELDNAME2,... (dipisah dengan koma). nama field  yang tidak dimasukan kedalam form yang digenerate} '
        . '{--package : [OPTIONAL] jika disertakan maka module yang digenerate adalah module laravel package}';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'BSSystem - Module generator';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct(ModGen $Gen)
    {
        parent::__construct();
        $this->sysGen = $Gen;
    }

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle()
    {

        $module = $this->argument('module');
        $controller = $this->option('controller');
        $table = $this->option('table');
        $service = $this->option('service');
        $repository = $this->option('repository');
        $field_exclude = $this->option('field_exclude');
        $package = $this->option('package');

        //pastikan mengisi parameter nama module         
        if (is_null($module)) {
            $module = $this->ask('Module name ?');
        }
        if (is_null($module)) {
            $this->error('Generator abort : module name cannot be empty');
            return false;
        }

        $this->sysGen->setPackageMode($package?true:false);
        $this->sysGen->setPackage($module);

        //jika tidak mencantumkan nama controller maka gunakan nama module sebagai
        //nama controller nya
        if (is_null($controller)) {
            $controller = $module;
        }
        
        $controller = str_replace('-', '', $controller);
        
        $this->sysGen->setController($controller);

        //jika tidak mencantumkan table maka generate dari default module
        if (is_null($table)) {
            $table = strtolower($controller);
            $table_detail = config('bssystem.generate_table_default');
            $this->info('Table not defined : auto generate from default format');
        } else {

            //jika menyertakan field_exclude
            if (!is_null($field_exclude)) {
                $field_exclude = array_map('trim', explode(',', $field_exclude));
                $this->sysGen->setFieldExclude($field_exclude);
            }

            //get detail field format dari table yang diinput
            if (!($table_detail = $this->sysGen->getColumnListing($table))) {
                $this->error('Generator abort : table "' . $table . '" not found');
                return false;
            }
        }

        $this->sysGen->setTable($table); //digunakan untuk generate nama model
        $this->sysGen->setTableFormat($table_detail); //digunakan sebagai item yg akan digenerate di form nya
        
        //---start proses generate        
        $return_sysgen = $this->sysGen->generate();
        $logs = $this->sysGen->generateLog();
        foreach ($logs as $log) {
            if ($log[0] == 'info') {
                $this->info($log[1]);
            } else {
                $this->error($log[1]);
            }
        }

        if ($return_sysgen) {
            
            $this->info('Generate Complete !');
        } else {
            $this->error('Generate Inclomplete !');
        }
    }

}
