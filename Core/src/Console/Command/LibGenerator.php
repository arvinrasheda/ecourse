<?php

namespace BSSystem\Core\Console\Command;

use Illuminate\Console\Command;
use BSSystem\Core\Services\LibGen;

class LibGenerator extends Command
{

    protected $sysGen;
    
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'bssystem:libgen '
        . '{library? : nama library yang akan digenerate} '
        . '{--table= : [OPTIONAL] source table yang akan digenerate} '
        . '{--service= : [OPTIONAL] nama service} '
        . '{--repository= : [OPTIONAL] nama repository} ';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'BSSystem - Library generator';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct(LibGen $Gen)
    {
        parent::__construct();
        $this->sysGen = $Gen;
    }

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle()
    {
        $library = $this->argument('library');
        $table = $this->option('table');
        $service = $this->option('service');
        $repository = $this->option('repository');
        
        //pastikan mengisi parameter nama module         
        if (is_null($library)) {
            $library = $this->ask('Library name ?');
        }
        
        if (is_null($library)) {
            $this->error('Generator abort : library name cannot be empty');
            return false;
        }
        
        $this->sysGen->setPackageMode(true);
        $this->sysGen->setPackage($library);    
        
        $library = str_replace('-', '', $library);
        
        //jika tidak mencantumkan table maka generate dari default module
        if (is_null($table)) {
            $table = strtolower($library);
            $table_detail = config('bssystem.generate_table_default');
            $this->info('Table not defined : auto generate from default format');
        }
        
        $this->sysGen->setTable($table); //digunakan untuk generate nama model
        //
        //---start proses generate        
        $return_sysgen = $this->sysGen->generate();
        $logs = $this->sysGen->generateLog();
        foreach ($logs as $log) {
            if ($log[0] == 'info') {
                $this->info($log[1]);
            } else {
                $this->error($log[1]);
            }
        }

        if ($return_sysgen) {
            
            $this->info('Generate Complete !');
        } else {
            $this->error('Generate Inclomplete !');
        }
        
    }
}