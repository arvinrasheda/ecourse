<?php

namespace BSSystem\Core\Base;

use Illuminate\Contracts\Support\Responsable;

abstract class BaseResponse implements Responsable
{

    protected $data;
    protected $isApiCall=false;
    protected $view;

    public function __construct($data=false,$view='list')
    {
        $this->data = $data;
        $this->view = $view;
    }
    
    abstract protected function prepare();
    
    /*
     * Detect apakah request untuk output ke API atau View
     */
    protected function isApiCall($request)
    {
        if($request->wantsJson() || $request->ajax()){
            $this->isApiCall = true;
        }
        
        return $this->isApiCall;
    }
    
    /**
     * Response output ajax
     */
    protected function apiResponse()
    {
        return response()->json($this->data);
    }
    
    /**
     * Response output view layer
     */
    protected function viewResponse()
    {
        if(is_string($this->view)){
            return response()->view($this->view, $this->data);
        }else{
            return $this->view;
        }
    }

    public function toResponse($request)
    {
        $this->prepare();
        return $this->isApiCall($request) ? $this->apiResponse() : $this->viewResponse();
    }

}
