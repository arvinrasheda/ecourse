<?php

namespace BSSystem\Core\Base;

abstract class BaseRepository
{

    protected $model;

    public function getModel()
    {
        return $this->model;
    }

    public function setModel($model)
    {
        $this->model = $model;
    }

    public function all()
    {
        return $this->model->all();
    }

    public function create(array $data)
    {
        return $this->model->create($data);
    }

    public function update(array $data, $id)
    {
        $record = $this->model->find($id);
        return $record->update($data);
    }

    public function delete($id)
    {
        return $this->model->destroy($id);
    }

    public function find($id)
    {
        return $this->model->find($id);
    }
    
    public function getOne($key,$value=false)
    {
        if(!$value){
            $value = $key;
            $key = 'id';            
        }
        $data = $this->model->where($key,$value)->first();
        if(!$data)return false;
        
        return $data->toArray();
    }

    public function with($relations)
    {
        return $this->model->with($relations);
    }

}
