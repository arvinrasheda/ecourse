<div class="form-group">
    <label for="input-{{NAME}}">{{CAPTION}}</label>
    <textarea name="{{NAME}}" id="input-{{NAME}}"  class="form-control">{{ $data ? $data->{{NAME}} : old('{{NAME}}') }}</textarea>
</div>