<div class="form-group">
    <label for="input-{{NAME}}">{{CAPTION}}</label>
    <textarea id="input-{{NAME}}"  class="form-control" readonly>{{ $data ? $data->{{NAME}} : '' }}</textarea>
</div>