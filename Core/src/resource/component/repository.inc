<?php

namespace {{NAMESPACE}}\Repositories;

use {{NAMESPACE}}\Models\{{MODEL_NAME}};

use BSSystem\Core\Base\BaseRepository;

class {{CLASS_NAME}} extends BaseRepository
{
    public function __construct({{MODEL_NAME}} $model)
    {
        $this->model = $model;
    }
}