<?php

namespace {{NAMESPACE}}\Models;

use Illuminate\Database\Eloquent\Model;

class {{CLASS_NAME}} extends Model
{
    /**
     * The table associated with the model.
     *
     * @var string
     */
    protected $table = '{{TABLE_NAME}}';
    
    /**
     * The attributes that aren't mass assignable.
     *
     * @var array
     */
    protected $guarded = [{{FIELD_EXCLUDE}}];
}