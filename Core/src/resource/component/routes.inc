Route::prefix('{{ROUTE_NAME}}')->group(function(){
    Route::get('/', '{{CONTROLLER_NAME}}@index')->name('{{ROUTE_NAME}}.index');
    Route::post('/', '{{CONTROLLER_NAME}}@store')->name('{{ROUTE_NAME}}.store');
    Route::get('/create', '{{CONTROLLER_NAME}}@create')->name('{{ROUTE_NAME}}.create');
    Route::delete('/{id}', '{{CONTROLLER_NAME}}@destroy')->name('{{ROUTE_NAME}}.destroy');
    Route::put('/{id}', '{{CONTROLLER_NAME}}@update')->name('{{ROUTE_NAME}}.update');
    Route::get('/{id}', '{{CONTROLLER_NAME}}@show')->name('{{ROUTE_NAME}}.show');
    Route::get('/{id}/edit', '{{CONTROLLER_NAME}}@edit')->name('{{ROUTE_NAME}}.edit');
});