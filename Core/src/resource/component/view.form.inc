@extends('{{LAYOUT}}')

@section('content')
<div class="container">
    <section class="content-header">
        <h1>
            FORM
        </h1>
        {{Breadcrumb::generate()}}
    </section>
    <section class="content">
        <div class="row">
            <div class="col-md-12">
                <!-- general form elements -->
                <div class="box box-primary">
                    <div class="box-header with-border">
                        <h3 class="box-title">{{ $mode=='create'?'Tambah Baru':'Edit Data' }}</h3>
                    </div>
                    <!-- /.box-header -->
                    <!-- form start -->
                    <form role="form" method="post" action="{{ $mode=='create'?route('{{CONTROLLER_ROUTE_NAME}}.store'):route('{{CONTROLLER_ROUTE_NAME}}.update',$data->id) }}">
                        {{ csrf_field() }}
                        {{ $mode=='edit'?method_field('PATCH'):'' }}
                        <div class="box-body">
                            {{VIEW_COMPONENT}}
                        </div>
                        <!-- /.box-body -->

                        <div class="box-footer">
                            <button type="submit" class="btn btn-primary">Save</button>
                        </div>
                    </form>
                </div>
            </div>
        </div>
    </section>
    
</div>
@endsection