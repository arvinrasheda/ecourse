<div class="form-group">
    <label for="input-{{NAME}}">{{CAPTION}}</label>
    <input type="text"  class="form-control" id="input-{{NAME}}" value="{{ $data ? $data->{{NAME}} : '' }}" readonly>
</div>