@extends('{{LAYOUT}}')

@section('content')
<div class="container">
    <section class="content-header">
        {{Breadcrumb::generate()}}
    </section>
    <section class="content">
        <div class="row">
            <div class="col-md-12">
                <!-- general form elements -->
                <div class="box box-primary">
                    <div class="box-header with-border">
                        <h3 class="box-title">Detail</h3>
                    </div>
                    <!-- /.box-header -->
                    <!-- form start -->
                    <div>
                        <div class="box-body">
                            {{VIEW_COMPONENT}}
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </section>
    
</div>
@endsection