<?php

namespace BSSystem\Core\Providers;

use Illuminate\Contracts\Events\Dispatcher as DispatcherContract;
use Illuminate\View\ViewServiceProvider as BaseViewServiceProvider;
use BSSystem\Core\Utilities;

class ViewServiceProvider extends BaseViewServiceProvider
{

    /**
     * Load configuration
     */
    private function loadConfig()
    {
        
        /** @var \Illuminate\Config\Repository $config */
        $config = $this->app['config'];
        
        //jika config sudah diset di aplikasi
        if(isset($config['bssystem'])){
            
            $config1 = require __DIR__ . '/../config/bssystem.php';
            $config2 = $config['bssystem'];
            $config->set('bssystem', array_merge($config1,$config2));            
            $config->set('bssystem.vendor', require __DIR__ . '/../config/config.php');            
        }else{
            $config1 = require __DIR__ . '/../config/bssystem.php';
            $config2['vendor'] = require __DIR__ . '/../config/config.php';        
            $config->set('bssystem', array_merge($config1,$config2));
            
        }
    }

    public function register()
    {
        $this->loadConfig();

        $config = $this->app['config']['bssystem']['vendor'];
        
        $paths = Utilities::findNamespaceResources(
            $config['namespaces'], $config['view_folder_name'], $config['resource_namespace']
        );
        $this->app['config']['view.paths'] = array_merge($paths,$this->app['config']['view.paths']);
        parent::register();
    }

}
