<?php

namespace BSSystem\Core\Providers;

//use Illuminate\Routing\Router;
use Illuminate\Support\Facades\Route;
use Illuminate\Foundation\Support\Providers\RouteServiceProvider as ServiceProvider;
use BSSystem\Core\Utilities;
use BSSystem\Core\Console\Command\ModuleGenerator;
use BSSystem\Core\Console\Command\LibGenerator;
use BSSystem\Core\Console\Command\EnvMode;

class RouteServiceProvider extends ServiceProvider
{
    private $config;
    /**
     * Bootstrap any application services.
     *
     * @param  \Illuminate\Routing\Router  $router
     * @return void
     */
    public function boot()
    {
        //Schema::defaultStringLength(191);
        //view()->share('page', '');
        if ($this->app->runningInConsole()) {
            $this->commands([
                ModuleGenerator::class,
                EnvMode::class,
                LibGenerator::class
            ]);
        }
        
        $this->bootMigration();
        parent::boot();
    }
    
    /*
     * tambah migration path disetiap module
     */
    private function bootMigration()
    {
        $mainPath = database_path('migrations');       
        
        $modulePath = Utilities::listModulePath($this->app['config']['bssystem']['vendor']['namespaces'], function($namespace,$pathToModule){            
            $pathToModule .= DIRECTORY_SEPARATOR.'migrations';
            if(file_exists($pathToModule)){
                return $pathToModule;
            }
        });
        
        $paths = array_merge([$mainPath], $modulePath);
        $this->loadMigrationsFrom($paths);
    }

    /**
     * Register
     */
    public function register()
    {
        $this->registerAssets();
        
//        $this->app->singleton('breadcrumb', function ($app) {
//            return new \BSSystem\Core\Services\Breadcrumb();
//        });
    }

    /**
     * Register assets
     */
    private function registerAssets()
    {
        
//        $this->app->singleton('breadcrumb', function ($app) {
//            return new \App\Breadcrumb();
//        });
        
//        $this->publishes([
//            __DIR__.'/../config/bssystem.php', config_path('bssystem.php')
//        ]);
        
        $this->mergeConfigFrom(
            __DIR__.'/../config/bssystem.php', config_path('bssystem.php')
        );        
        
    }

    /**
     * Define the routes for the application.
     *
     * @param  \Illuminate\Routing\Router  $router
     * @return void
     */
    public function map()//(Router $router)
    {
        $config = $this->app['config']['bssystem'];
        $middleware = $config['protection_middleware'];
        
        if(isset($config['protection_middleware'])){
            $middleware = array_merge($middleware,$config['protection_middleware']);
        }
        
        Utilities::listModulePath($config['vendor']['namespaces'], function($namespace,$pathToModule) use ($middleware){
            
            $fileNames = [
                'routes' => false,
                'routes_api' => true
            ];
            $namespace .= 'Controllers';
            //load seluruh routes yg ada di setiap module
            foreach ($fileNames as $fileName => $isApi) {
                $path = sprintf('%s/%s.php', $pathToModule, $fileName);

                if (!file_exists($path)) {
                    continue;
                }

                Route::middleware($isApi ? ['api'] : ['web'])
                    ->namespace($namespace)
                    ->group($path);
            }
        });
        
    }
}
