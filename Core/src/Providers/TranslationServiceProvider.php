<?php

namespace BSSystem\Core\Providers;

use Illuminate\Translation\TranslationServiceProvider as BaseTranslationServiceProvider;
use BSSystem\Core\Translation\DistributedFileLoader;
use BSSystem\Core\Utilities;

class TranslationServiceProvider extends BaseTranslationServiceProvider
{

    /**
     * Register the translation line loader.
     *
     * @return void
     */
    protected function registerLoader()
    {

        $config = $this->app['config']['bssystem']['vendor'];

        $paths = Utilities::findNamespaceResources(
                $config['namespaces'], $config['language_folder_name'], $config['resource_namespace']
        );
        $paths = array_merge([resource_path('lang')],$paths);        
        $this->app->singleton('translation.loader', function ($app) use ($paths) {
            return new DistributedFileLoader($app['files'], $paths);
        });
    }

}
