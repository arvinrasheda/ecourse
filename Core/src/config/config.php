<?php

return [
    /*
     * namespace ke path lokasi module
     *  NAMESPACE => [path_to_module_group, FILTER PREFIX
     */
    'namespaces' => [
        'App\\Modules' => [app_path('Modules') . DIRECTORY_SEPARATOR, false],
        'BSSystem' => [base_path('vendor' . DIRECTORY_SEPARATOR . 'BS-System') . DIRECTORY_SEPARATOR, 'MOD-']
    ],
    'lib_namespace' => ['BSSystem' => [base_path('vendor' . DIRECTORY_SEPARATOR . 'BS-System'), 'LIB-']],
    
    'resource_namespace' => 'resources',
    
    'language_folder_name' => 'lang',
    
    'view_folder_name' => 'views',
    
    'package_composer_json' => [
        "name" => "BS-System/",
        "description" => "BS system - ",
        "autoload" => [
            "psr-4" => [
                "BSSystem\\Core\\" => "src"
            ]
        ],
        "require" => [
            "php" => "~7.1"
        ],
        "require-dev" => [
            "php" => "~7.1"
        ],
        "minimum-stability" => "dev"
    ],
    /*
     * struktur direktori module
     */
    'generate_module_structure' => [
        'Console' => [
            'Command' => []
        ],
        'Controllers' => [],
        'Events' => [],
        'Exceptions' => [],
        'Models' => [],
        'Repositories' => [],
        'Requests' => [],
        'Responses' => [],
        'Services' => [],
        'migrations' => [],
        'resources' => [
            'lang' => [
                'en' => [],
                'id' => []
            ],
            'views' => []
        ]
    ],
    /*
     * struktur direktori lib
     */
    'generate_lib_structure' => [        
        'Models' => [],
        'Repositories' => [],
        'Services' => []
    ],
];

