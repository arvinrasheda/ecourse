<?php

namespace BSSystem\MODMember\Models;

use Illuminate\Database\Eloquent\Model;

class Member extends Model
{
    /**
     * The table associated with the model.
     *
     * @var string
     */
    protected $table = 'member';
    
    /**
     * The attributes that aren't mass assignable.
     *
     * @var array
     */
    protected $guarded = ['id','created_at','updated_at'];

    public function ecourse()
    {
        return $this->belongsTo('BSSystem\MODEcourse\Models\Ecourse');
    }
    public function user()
    {
        return $this->belongsTo('BSSystem\MODUser\Models\Users');
    }
}