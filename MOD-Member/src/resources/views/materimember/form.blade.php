@extends('layouts-ecourse.home')
@section('styles')
<link rel="stylesheet" href="{{ asset('/vendor/libs/bootstrap-markdown/bootstrap-markdown.css') }}">
<link rel="stylesheet" href="{{ asset('/vendor/libs/dropzone/dropzone.css') }}">

@endsection

@section('scripts')
<!-- Dependencies -->
<script src="{{ asset('/vendor/libs/markdown/markdown.js') }}"></script>
<script src="{{ asset('/vendor/libs/bootstrap-markdown/bootstrap-markdown.js') }}"></script>
<script src="{{ asset('/vendor/libs/dropzone/dropzone.js') }}"></script>
<script src="{{ asset('/js/forms_editors.js') }}"></script>
<script src="{{ asset('/js/dropzone.js') }}"></script>
@endsection

@section('content')
<?php echo \Breadcrumb::generate(); ?>
<div class="nav-tabs-top">
    <ul class="nav nav-tabs">
        <li class="nav-item">
            <a class="nav-link active" data-toggle="tab" href="#navs-top-satuan">Satuan</a>
        </li>
        <li class="nav-item">
            <a class="nav-link" data-toggle="tab" href="#navs-top-bulk">Bulk</a>
        </li>
    </ul>
    <div class="tab-content">
        <div class="tab-pane fade show active" id="navs-top-satuan">
            <div class="card-body">
                <form enctype="multipart/form-data" role="form" method="POST" action="{{ $mode=='create'?route('materimember.store'):route('materimember.update',$data->id) }}">
                    {{ csrf_field() }}
                    {{ $mode=='edit'?method_field('PUT'):'' }}
                    <div class="form-group">
                        <label class="form-label">Name</label>
                        <input type="text" name="name" class="form-control">
                    </div>
                    <div class="form-group">
                        <label class="form-label">Email</label>
                        <input type="text" name="email" class="form-control">
                    </div>
                    <div class="form-group">
                        <label class="form-label">Phone Number</label>
                        <input type="number" name="phone" class="form-control">
                    </div>
                    <div class="form-group">
                        <label for="input-materi_id">Materi</label>
                        <div class="form-group">
                            <select class="custom-select" name="materi_id" id="input-materi_id">
                                <option value="">Select Materi</option>
                                @foreach ($materi as $item)
                                <option value="{{ $item->id }}">{{ $item->tittle }}</option>
                                @endforeach
                            </select>
                        </div>
                    </div>
                    <input type="hidden" name="status" value="0">
                    <div class="box-footer">
                        <button type="submit" class="btn btn-primary">Save</button>
                    </div>
                </form>
            </div>
        </div>
        <div class="tab-pane fade" id="navs-top-bulk">
            <div class="card-body">
                <form enctype="multipart/form-data" role="form" method="POST" action="{{ $mode=='create'?route('materimember.storeData'):route('materimember.update',$data->id) }}">
                    {{ csrf_field() }}
                    {{ $mode=='edit'?method_field('PUT'):'' }}
                    <div class="form-group">
                        <label for="input-file">Download Template Excel</label><br>
                        <a href="" class="btn btn-lg btn-secondary"><span class="oi oi-cloud-download"></span>&nbsp;&nbsp;Download</a>
                        <small class="form-text text-muted">Download Template Excel Diatas .</small>
                    </div>
                    <div class="form-group">
                        <label for="input-file">File</label><br>
                        <input type="file" name="file" id="input-file">
                        <small class="form-text text-muted">Harap Masukan Data Excel Member Yang Ingin DiUpload.</small>
                    </div>
                    <div class="form-group">
                        <label for="input-materi_id">Materi</label>
                        <div class="form-group">
                            <select class="custom-select" name="materi_id" id="input-materi_id">
                                <option value="">Select Materi</option>
                                @foreach ($materi as $item)
                                <option value="{{ $item->id }}">{{ $item->tittle }}</option>
                                @endforeach
                            </select>
                        </div>
                    </div>
                    <div class="box-footer">
                        <input type="hidden" name="status" value="0">
                        <button type="submit" class="btn btn-primary">Save</button>
                    </div>
                </form>
            </div>
        </div>
    </div>
</div>
@endsection
