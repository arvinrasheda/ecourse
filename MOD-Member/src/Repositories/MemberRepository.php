<?php

namespace BSSystem\MODMember\Repositories;

use BSSystem\MODMember\Models\Member;
use BSSystem\MODEcourse\Models\Ecourse;
use BSSystem\MODUser\Models\Users;
use Illuminate\Support\Facades\Session;
use BSSystem\LIBGroup\Models\GroupMember;
use BSSystem\LIBGroup\Models\Group;
use BSSystem\LIBProfile\Models\UserProfiles;
use BSSystem\LIBMateriMember\Models\MateriMember;

use BSSystem\Core\Base\BaseRepository;
use BSSystem\LIBMateri\Models\Materi;

class MemberRepository extends BaseRepository
{
    public function __construct(Member $model){
        $this->model = $model;
    }

    public function getEcourse(){
        return Ecourse::all();
    }

    public function createMember($data){
        $findUser = Users::where('email', $data['email'])->first();

        if(!empty($findUser)){
            $userId = $findUser->id;
            $member = new Member;
            $member->user_id = $userId;
            $member->ecourse_id = $data['ecourse_id'];
            $member->save();

            $materi = Materi::where('ecourse_id', $data['ecourse_id'])->get();
            foreach ($materi as $key => $materiId) {
               $memberMateri = new MateriMember;
               $memberMateri->user_id = $userId;
               $memberMateri->materi_id = $materiId->id;
               $memberMateri->ecourse_id = $data['ecourse_id'];
               $memberMateri->save();
            }
            
            return true;
        }
        elseif(empty($findUser)){
            $user = new Users;
            $user->name = $data['name'];
            $user->email = $data['email'];
            $user->phone = $data['phone'];
            $user->password = bcrypt(substr($data['phone'], -5));
            $user->status = $data['status'];
            $user->save();

            $findUser = Users::where('email', $data['email'])->first();
            $userId = $findUser->id;

            $profiles = new UserProfiles;
            $profiles->name = $data['name'];
            $profiles->user_id = $userId;
            $profiles->email2 = $data['email'];
            $profiles->phone2 = $data['phone'];
            $profiles->save();

            $member = new Member;
            $member->user_id = $userId;
            $member->ecourse_id = $data['ecourse_id'];
            $member->save();  

            $materi = Materi::where('ecourse_id', $data['ecourse_id'])->get();
            foreach ($materi as $key => $materiId) {
               $memberMateri = new MateriMember;
               $memberMateri->user_id = $userId;
               $memberMateri->materi_id = $materiId->id;
               $memberMateri->ecourse_id = $data['ecourse_id'];
               $memberMateri->save();
            }

            return true;
        }
    }

    public function insertData($data, $ecourse_id,  $status){
        foreach ($data as $key => $value) {
            $findUser = Users::where('email', $value->email)->first();
           
            if(!empty($findUser)){
                $userId = $findUser->id;
                $member = new Member;
                $member->user_id = $userId;
                $member->ecourse_id = $ecourse_id;
                $member->save();

                $materi = Materi::where('ecourse_id', $ecourse_id)->get();
                foreach ($materi as $key => $materiId) {
                    $memberMateri = new MateriMember;
                    $memberMateri->user_id = $userId;
                    $memberMateri->materi_id = $materiId->id;
                    $memberMateri->ecourse_id = $ecourse_id;
                    $memberMateri->save();
                }
            }
            elseif(empty($findUser)){
                $user = new Users;
                $user->name = $value->name;
                $user->email = $value->email;
                $user->phone = $value->phone;
                $user->password = bcrypt(substr($value->phone, -5));
                $user->status = $status;
                $user->save();

                $findUser = Users::where('email', $value->email)->first();
                $userId = $findUser->id;

                $profiles = new UserProfiles;
                $profiles->name = $value->name;
                $profiles->user_id = $userId;
                $profiles->email2 = $value->email;
                $profiles->phone2 = $value->phone;
                $profiles->save();

                $member = new Member;
                $member->user_id = $userId;
                $member->ecourse_id = $ecourse_id;
                $member->save();

                $materi = Materi::where('ecourse_id', $ecourse_id)->get();
                foreach ($materi as $key => $materiId) {
                    $memberMateri = new MateriMember;
                    $memberMateri->user_id = $userId;
                    $memberMateri->materi_id = $materiId->id;
                    $memberMateri->ecourse_id = $ecourse_id;
                    $memberMateri->save();
                }
            }
        }
        return true;
    }
}