<?php

Route::prefix('member')->group(function(){
    Route::get('/', 'MemberController@index')->name('member.index');
    Route::post('/', 'MemberController@store')->name('member.store');
    Route::post('/storedata', 'MemberController@storeData')->name('member.storeData');
    Route::get('/create', 'MemberController@create')->name('member.create');
    Route::delete('/{id}', 'MemberController@destroy')->name('member.destroy');
    Route::put('/{id}', 'MemberController@update')->name('member.update');
    Route::get('/{id}', 'MemberController@show')->name('member.show');
    Route::get('/{id}/edit', 'MemberController@edit')->name('member.edit');
});

Route::prefix('materimember')->group(function(){
    Route::get('/', 'MateriMemberController@index')->name('materimember.index');
    Route::post('/', 'MateriMemberController@store')->name('materimember.store');
    Route::post('/storedata', 'MateriMemberController@storeData')->name('materimember.storeData');
    Route::get('/create', 'MateriMemberController@create')->name('materimember.create');
    Route::delete('/{id}', 'MateriMemberController@destroy')->name('materimember.destroy');
    Route::put('/{id}', 'MateriMemberController@update')->name('materimember.update');
    Route::get('/{id}', 'MateriMemberController@show')->name('materimember.show');
    Route::get('/{id}/edit', 'MateriMemberController@edit')->name('materimember.edit');
});