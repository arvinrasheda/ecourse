<?php

namespace BSSystem\MODMember\Controllers;

use Illuminate\Http\Request;
use BSSystem\MODMember\Repositories\MemberRepository;
use BSSystem\MODMember\Responses\MemberResponse;
use Excel;
use Illuminate\Support\Facades\Session;
use Illuminate\Support\Facades\File;
use Illuminate\Support\Facades\Auth;

use BSSystem\Core\Base\BaseController;

class MemberController extends BaseController
{
    
    private $repo;
    
    public function __construct(MemberRepository $repo)
    {
        \Breadcrumb::add('Member',route('member.index'));
        
        $this->repo = $repo;

        $this->middleware('auth');
    }
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        if(Auth::user()->is_admin == 1){
            $data['data'] = $this->repo->all();
            return new MemberResponse($data,'member.list');
        }else{
            return back();
        }
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        \Breadcrumb::add('Tambah Baru',route('member.create'));        
        
        $data['mode'] = 'create';
        $data['data'] = [];
        $data['ecourse'] = $this->repo->getEcourse();

        return view('member.form', $data);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $data = $request->all();
//        $request->validate([
//            'name' => 'required|string|max:255'
//        ]);        
        $this->repo->createMember($data);
        return redirect()->route('member.index')->with('alert', ['type' => 'success', 'message' => 'Data inserted successfully']);
    }

    public function storeData(Request $request)
    {
        //validate the xls file
        // $this->validate($request, array(
        //     'file'      => 'required|mimes:xls,xlsx,csv'
        // ));
        $ecourse_id = $request->ecourse_id;
        $status = $request->status;
        if($request->hasFile('file')){
            $extension = File::extension($request->file->getClientOriginalName());
            if ($extension == "xlsx" || $extension == "xls" || $extension == "csv") {
     
                $path = $request->file->getRealPath();
                $data = Excel::load($path, function($reader) {

                })->get();
                if(!empty($data) && $data->count()){
                    $this->repo->insertData($data, $ecourse_id, $status);
                }
                Session::flash('success', 'Your Data has successfully imported');
                return redirect()->route('member.index');
     
            }else {
                Session::flash('error', 'File is a '.$extension.' file.!! Please upload a valid xls/csv file..!!');
                return redirect()->route('member.index');
            }
        }
    }
    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $data['data'] = $this->repo->find($id);
        
        \Breadcrumb::add('View id : <b>'.$data['data']['ig'].'</b>',route('member.show',$data['data']['id']));
        
        return new MemberResponse($data,'member.detail');
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $data['data'] = $this->repo->find($id);
        
        \Breadcrumb::add('Edit di : <b>'.$data['data']['id'].'</b>',route('member.edit',$data['data']['id']));
        
        $data['mode'] = 'edit';

        return view('member.form', $data);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request,$id)
    {
        $input = $request->all();
        
//        //pastikan sudah
//        if($request->input('name')){
//            $validate['name'] = 'required|string|max:255';
//        }
//        
//        if(isset($validate)){
//            $request->validate($validate);
//        }
        
        $this->repo->update($input,$id);
        
        return redirect()->route('member.index')->with('alert', ['type' => 'success', 'message' => 'Data updated successfully']);

    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $this->repo->delete($id);
        return redirect()->route('member.index')->with('alert', ['type' => 'success', 'message' => 'Data Deleted !']);

    }    
    
}
