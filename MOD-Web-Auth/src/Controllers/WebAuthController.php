<?php

namespace BSSystem\MODWebAuth\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Facades\BSSystem\MODWebAuth\Services\SSOService;
use Facades\BSSystem\LIBAccount\Repositories\UserRepo;
use BSSystem\MODWebAuth\Events\UserRegistered;

use BSSystem\Core\Base\BaseController;

class WebAuthController extends BaseController
{
    /**
     * proses semua initiasi
     * @param Request $request
     *      token
     *      backlink
     *      reff :
     *      isNotRegistered : dsertakan jika user tidak terdaftar di aplikasi
     *      isRegisterd
     *      isValid :
     *      isLogin
     * @return type
     */
    public function grabSession (Request $request)
    {        
        //jika saat divalidasi, session tidak valid berarti ada yang error
        //hampir tidak mungkin bisa masuk sini kecuali ada error
        if(!SSOService::validateSession($request->input('token'),$request->input('isAppsToken'))){            
            
            //batasi proses regrab ini hanya 2 kali (via grabAttempt)
            if(!SSOService::grabAttempts()){
                SSOService::clearGrabAttempts();
                return redirect()->route('webauth.warningPage',['warning_type' => 'SSOFailed'])->with('alert', ['type' => 'danger', 'message' => 'SSO Failed.']);
            }
            return SSOService::grabSession();            
        }
                
        //jika user berhasil login tapi tidak memiliki akses ke aplikasi yang dimaksud
        //atau jika tidak terdaftar diaplilkasi ini maka tolak juga
        if($request->input('isNotRegistered') || SSOService::isForbidden()){            
            SSOService::setForbbiden();//dieksekusi jika belum set forbidden (jika baru dapet isNotRegisterd aja
            return redirect()->route('webauth.warningPage',['warning_type' => 'NotRegisterd'])->with('alert', ['type' => 'danger', 'message' => 'User Not Registerd.']);
        }
        
        if($request->input('isAppsToken')){
            $userData = SSOService::getUserData();            
        }
        
        //jika di action selain registrasi ada proses registrasi
        if($request->input('isRegistered')){
            event(new UserRegistered(SSOService::getUserData()));
        }
        
        //jika di server sso login tapi di client belum, maka loginkan ulang
        if($request->input('isLogin')){            
            if(!Auth::check() || Auth::user()->id != SSOService::getUserData('id')){
                Auth::logout();
                $userModel = UserRepo::getUserModel(SSOService::getUserData('id'));
                Auth::login($userModel);
            }
            
        //jika sudah tidak login tapi masih terdetek login
        }else if(Auth::check()){
            Auth::logout();
        }
        
        switch ($request->input('reff')) {       
            case 'login':
                //....
                break;
            case 'logout':
                //...
                break;
            case 'register':
                //...
                break;
            case 'registersocialauth':
                //....
                break;
            case 'init'://saat pertama kali grab session atau revalidate session
            case 'reinit'://saat revalidate
            default:
                //...              
                break;
        }
        
        SSOService::clearGrabAttempts();
        if($request->input('backlink'))return redirect($request->input('backlink'));        
        
        return redirect(config('bssystem.cur_apps.home_url'));
    }
    
    /**
     * Menampilkan halaman warning
     * 
     * @param Request $request
     * @param string $warning_type
     * @return type
     */
    public function warningPage (Request $request, $warning_type)
    {
        $data = [];
        switch ($warning_type) {
            case 'SSOFailed':
                
                break;
            case 'NotRegisterd':
            default:
                $warning_type = 'NotRegisterd';
                break;
        }
        return view('sso.warning_'.$warning_type, $data);
    }
    
    /**
     * 
     * @param Request $request
     * @return WebAuthResponse
     */
    public function updateData (Request $request)
    {
        switch ($request->input('action')) {            
            case 'reinit'://saat revalidate
            case 'init'://saat pertama kali grab session atau revalidate session
                if(SSOService::isLogin()){
                    
                }
                
                break;
            case 'login':
                Auth::login();
                break;
            case 'logout':
                Auth::logout();
                break;
            case 'register':
                
                break;
            case 'registersocialauth':
            default:
                break;
        }
        return new WebAuthResponse($data,'webauth.list');
    }    
    
}
