<?php

Route::prefix('webauth')->group(function(){
    Route::get('/grab', 'WebAuthController@grabSession')->name('webauth.grabSession');
    Route::get('/warning/{warning_type}', 'WebAuthController@warningPage')->name('webauth.warningPage');
});