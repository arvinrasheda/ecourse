<?php

namespace BSSystem\MODWebAuth\Events;

use Illuminate\Queue\SerializesModels;

class UserRegistered
{
    use SerializesModels;

    public $userData;
    /**
     * Create a new event instance.
     *
     * @return void
     */
    public function __construct($userData)
    {
        $this->userData = $userData;
    }
}
