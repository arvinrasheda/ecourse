<?php

namespace BSSystem\MODWebAuth\Providers;

use Illuminate\Support\Facades\Event;
use Illuminate\Foundation\Support\Providers\EventServiceProvider as ServiceProvider;

use BSSystem\MODWebAuth\Events\ClientUserUpdated;
use BSSystem\MODWebAuth\Listeners\ClienUserUpdatedListener;

class EventServiceProvider extends ServiceProvider
{
    /**
     * The event listener mappings for the application.
     *
     * @var array
     */
    protected $listen = [
        ClientUserUpdated::class => [
            ClienUserUpdatedListener::class,
        ]
    ];

    /**
     * Register any events for your application.
     *
     * @return void
     */
    public function boot()
    {
        parent::boot();

        //
    }
}
