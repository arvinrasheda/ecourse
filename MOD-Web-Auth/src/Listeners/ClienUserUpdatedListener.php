<?php

namespace BSSystem\MODWebAuth\Listeners;

use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Contracts\Queue\ShouldQueue;

use BSSystem\MODWebAuth\Events\ClientUserUpdated;

class ClienUserUpdatedListener// implements ShouldQueue
{
    protected $queue = 'high';
    /**
     * Create the event listener.
     *
     * @return void
     */
    public function __construct()
    {
        //
    }

    /**
     * Handle the event.
     *
     * @param  ClientUserUpdated  $event
     * @return void
     */
    public function handle(ClientUserUpdated $event)
    {
        
    }
}
