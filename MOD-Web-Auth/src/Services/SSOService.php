<?php

namespace BSSystem\MODWebAuth\Services;

use Facades\BSSystem\LIBAccount\Repositories\AppsRepo;
use Facades\BSSystem\LIBAccount\Repositories\UserRepo;
use GuzzleHttp\Client;
use Carbon\Carbon;
//use Carbon\CarbonInterval;

/**
 * SSOSession structure :
 *      
 */
class SSOService
{    
    /**
     * Cek apakah sudah memiliki session SSO
     * @param boolean $validCheck jika false maka tidak akan cek valid tidak nya session
     * @return boolean
     */
    public function hasSession($validCheck=true)
    {        
        if(session('SSOSession')){
            if($validCheck && !$this->isSessionValid()){
                return false;
            }
            return true;            
        }
        return false;
    }
        
    /**
     * cek apakah usir dalam poisisi login
     * @return boolean
     */
    public function isLogin()
    {
        if(is_array(session('SSOSession.userData'))){
            return true;
        }
        return false;
    }    
    
    /**
     * isForbidden digunakan untuk menandai user login session di suatu aplikasi
     * yang user tersebut tidak terdaftar didalamanya, dan aplikasi tersebut allow_register
     * 
     * cek apakah usir dalam poisisi forbidden (user login tapi bukan user yg terdaftar disini)
     * 
     * @return boolean
     */
    public function isForbidden()
    {
        if(session('SSOSession.isForbidden')==1){
            return true;
        }
        return false;
    }
    public function setForbidden()
    {
        $this->setSession(['isForbidden' => 1]);
    }
    
    public function unsetForbidden()
    {
        $this->setSession(['isForbidden' => 0]);
    }
    
    /**
     * diproses saat user logout
     */
    public function unsetUser()
    {
        $this->setSession([
            'isLogin' => 0,
            'userToken' => '',
            'userData' => '',
            'userRole' => ''
        ]);        
        
    }
    
    public function getUserData($field=false)
    {
        if($field && session('SSOSession.userData.'.$field)){
            $data = session('SSOSession.userData.'.$field);
        }else{
            $data = session('SSOSession.userData');
        }
        return $data;
    }
    
    public function getUserRole($field=false)
    {
        if($field && session('SSOSession.userRole.'.$field)){
            $data = session('SSOSession.userRole.'.$field);
        }else{
            $data = session('SSOSession.userRole');
        }
        return $data;
    }
    
    
    public function getAppsToken($appsId)
    {
        return session('SSOSession.appsToken');
    }
    
    public function getUserToken()
    {
        return session('SSOSession.userToken');
    }
    
    /**
     * cek apakah masih valid
     * @return boolean
     */
    public function isSessionValid()
    {
        if(session('SSOSession.validUntil') < Carbon::now()){
            return false;
        }
        return true;
    }
    
    /**
     * cek apakah session sudah saat nya divalidasi (validasi 5 menit sekali)
     * @return boolean 
     */
    public function isTimeToValidate()
    {        
        //jika lastupdate sudah lebih dari 1 jam
        if(session('SSOSession.lastUpdate') < Carbon::now()->subMinutes(1)){
            return true;
        }
        return false;
    }
    
    /**
     * redirect ke halaman account center untuk grab session
     */
    public function grabSession($passingParam=false)
    {
        if(!isset($passingParam['backlink']))$passingParam['backlink'] = url()->full();
        $passingParam['apps_code'] = config('bssystem.cur_apps.apps_code');
        return redirect()->route('sso.getSession',$passingParam);
    }    
    
    /**
     * limit baranya melakukan grabSession karena session invalid, jika sudah
     * melebihi limit maka heuntikan proses grab dan munculkan halaman error
     * 
     * @return boolean true jika masih dibawah limit, false jika grab attempts nya
     *      melebihi limit
     */
    public function grabAttempts()
    {        
        $attempt = ((int) session('SSOSessionGrabAttemp')) + 1;
        session()->put('SSOSessionGrabAttemp',$attempt);
        session()->save();
        
        if($attempt > 2){
            return false;
        }
        
        return true;
    }
    
    public function clearGrabAttempts()
    {
        session()->forget('SSOSessionGrabAttemp');
    }
    
    /**
     * set session saat pertama kali dapet dari account center
     * @param array $apiData variable session SSO yg akan diubah
     */
    public function setSession(Array $data)
    {
//        $availableKey = ['id', 'isLogin','lastUpdate','validUntil','appsToken','userToken','userData','isForbidden'];
//        foreach ($availableKey as $value) {
//            if(isset($data[$value])){
//                session()->put('SSOSession.'.$value,$data[$value]);
//            }
//        }        
        foreach ($data as $key => $value) {
            session()->put('SSOSession.'.$key,$value);
        }  
        session()->save();
    }
    
    /**
     * cek validasi session, digunakan saat grab session dan validasi SSO cek
     */
    public function validateSession($token=false,$isAppsToken=false)
    {
        $firstInit = $this->hasSession();
        
        if(!$token){
            if(session('SSOSession.userToken')){
                $token = session('SSOSession.userToken');
            }else{
                $token = session('SSOSession.appsToken');
                $isAppsToken = true;
            }
        }
        
        $apiUrl = route('sso.validate',['apps_code' => config('bssystem.cur_apps.apps_code')]);
        $headers = [
            'Authorization' => 'Bearer ' . $token,        
            'Accept'        => 'application/json',
        ];
        
        $client = new Client();
//        $client->setDefaultOption('verify', false);
        $response = $client->request('POST', $apiUrl, [
            'headers' => $headers,
            'http_errors' => false,
            'verify' => false
        ]);
        
        //$statusCode = $response->getStatusCode();
        
        $apiData = json_decode($response->getBody()->getContents(),true);
        
        //jika invalid berarti harus grab ulang session atau sudah logout
        if(!isset($apiData['isValid']) || !$apiData['isValid']){
            
            //jika yg diinput apps token berarti session invalid, dan harus grab ulang
            if($isAppsToken){
                return false;
            
            //jika user token berarti user telah logout, maka pastikan logout
            }else{
                //jika sedang login, maka logoukan
                $this->unsetUser();
            }
            
        }        
        
        if($isAppsToken && !isset($apiData['appsToken'])){
            $apiData['appsToken'] = $token;
        }
        
        $this->setSession($apiData);
        return true;        
    }
    
    public function applyValidatedSSOToApps()
    {
        
    }
    
    /**
     * TIDAK JADI
     * perbaharui session yang sudah invalid
     */
    public function revalidateSession($token=false)
    {
        if(!$token)$token = session('SSOSession.token');
        
        $apiUrl = route('sso.revalidate',['apps_code' => config('bssystem.cur_apps.apps_code')]);
        
//        $headers = [
//            'Authorization' => 'Bearer ' . $token,        
//            'Accept'        => 'application/json',
//        ];
        
        $form_param = [
            'apps_id' => config('bssystem.cur_apps.apps_code'),
            'api_key' => config('bssystem.cur_apps.api_key'),
            'token' => $token
        ];
        
        $client = new Client();
        
        $response = $client->request('POST', $apiUrl, [
            'form_params' => $form_param
        ]);
        
        $apiData = json_decode($response->getBody()->getContents(),true);
        
        if(isset($apiData['token'])){
            $apiData['token'] = $token;
            $this->setSession($apiData);
        }
    }
}