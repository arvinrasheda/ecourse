<?php

namespace BSSystem\MODWebAuth\Services;

use BSSystem\LIBAccount\Repositories\AppsRepo;
use GuzzleHttp\Client;

use Illuminate\Foundation\Auth\User as Authenticatable;

class UserAuth extends Authenticatable
{    
    public function setUser($user)
    {
        $this->user = $user;
    }
    
    /**
     * @return  string
     */
    public function getAuthIdentifierName()
    {
        // Return the name of unique identifier for the user (e.g. "id")
        return 'id';
    }

    /**
     * @return  mixed
     */
    public function getAuthIdentifier()
    {
        // Return the unique identifier for the user (e.g. their ID, 123)
        return $this->user['id'];
    }

    /**
     * @return  string
     */
    public function getAuthPassword()
    {
        // Returns the (hashed) password for the user
        return $this->user['password'];
    }

    /**
     * @return  string
     */
    public function getRememberToken()
    {
        // Return the token used for the "remember me" functionality
        return $this->user['remember_token'];
    }

    /**
     * @param    string  $value
     * @return  void
     */
    public function setRememberToken($value)
    {
        // Store a new token user for the "remember me" functionality
        return $this->user['remember_token'] = $value;
    }

    /**
     * @return  string
     */
    public function getRememberTokenName()
    {
        // Return the name of the column / attribute used to store the "remember me" token
        return 'remeber_tokent';
    }
}