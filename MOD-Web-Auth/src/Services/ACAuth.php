<?php

namespace BSSystem\MODWebAuth\Services;

use Facades\BSSystem\MODWebAuth\Services\SSOService;
use Illuminate\Support\Facades\Auth;

/**
 * SSOSession structure :
 *      
 */
class ACAuth
{    
    protected $localUser;
    
    public function __construct()
    {
        $this->setLocalUser(SSOService::getUserData());
    }
    public function setLocalUser($localUser)
    {
        $this->localUser = $localUser;
    }
    public function isLogin()
    {
        return Auth::check() && SSOService::isLogin();
    }
    
    public function user($field=false)
    {
        return SSOService::getUserData($field);
    }    
    /**
     * get data dari table user per aplikasi (misal member, reseller, ecourse member, dll
     * 
     * @param type $field
     * @param type $default
     * @return type
     */
    public function localUser($field,$default=false)
    {
        return isset($this->localUser[$field])?$this->localUser[$field]:$default;
    }
    /**
     * cek apakah user yang online memiliki akses role "rolde_code"
     * 
     * @param type $roleCode
     * @return boolean
     */
    public function is($roleCode)
    {
        if(strpos(SSOService::getUserData('role'),':'.$roleCode.';')){
            return true;
        }
        return false;
    }
    public function isReseller()
    {
        //10 level reseller
        if(SSOService::getUserRole(10)){
            return true;
        }
        return false;
    }
    
    public function isSuperAdmin()
    {
        if(SSOService::getUserData('is_admin')&&SSOService::getUserRole(1)){
            return true;
        }
        return false;
    }
    public function isAdmin()
    {
        if(SSOService::getUserData('is_admin')){
            return true;
        }
        return false;
    }
    
    public function isMerchant()
    {        
        //11 level mechant
        if(SSOService::getUserData('is_merchant')){
            return true;
        }
        return false;
    }
}