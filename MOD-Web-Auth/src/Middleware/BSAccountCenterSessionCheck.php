<?php

namespace BSSystem\MODWebAuth\Middleware;

use Closure;
use Illuminate\Support\Facades\Auth;
use Facades\BSSystem\LIBAccount\Repositories\AppsRepo;
use Facades\BSSystem\LIBAccount\Repositories\UserRepo;
use Facades\BSSystem\MODWebAuth\Services\SSOService;

class BSAccountCenterSessionCheck
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */
    public function handle($request, Closure $next)
    {
//        session()->invalidate();die();
        //jika selain saat grabsession dan saat menampilkan halaman warning
        //maka bypass dulu cek sessionnya
        if($request->route()->getName()!='webauth.grabSession' && $request->route()->getName() != 'webauth.warningPage'){
            
            //jika belum punya session maka grab
            if(!SSOService::hasSession()){
                return SSOService::grabSession();
            }else{
                //jika sudah lebih dari 5 maka validasi ulang
                
                if(true){//SSOService::isTimeToValidate()){ 
                    
                    //validais ulang
                    if(SSOService::validateSession()){
                        
                        $return = $this->SSOProcess();
                        
                        //jika tidak true berarti hasilnya redirect
                        if($return !== true){
                            return $return;
                        }
                    //jika hasil validasi sudah tidak validate maka grab ulang session
                    }else{
                        return SSOService::grabSession();
                    }
                    
                }
            }
           
        }
        
        return $next($request);
    }
    
    public function SSOProcess()
    {
        //jika posisi login
        if(Auth::check()){            
            //jika di account center sudah logout maka logoutkan juga di client
            if(!SSOService::isLogin()){
                Auth::logout();
                SSOService::unsetUser();
            }
        }else{
            //jika di account center sudah login maka login kan juga
            if(SSOService::isLogin()){
                $userModel = UserRepo::getUserModel(SSOservice::getUserData('id'));
                Auth::login($userModel);
            }
        }
        return true;
    }
}
