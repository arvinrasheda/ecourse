<?php

namespace BSSystem\MODWebAuth\Facades;

/*
 * TIDAK DIGUNAKAN, HANYA UNTUK REFERENSI SAJA
 */
class SSOService extends \Illuminate\Support\Facades\Facade {
    
    protected static function getFacadeAccessor()
    {
        return 'SSOService'; // This one is fine
    }
}