@extends('layouts.auth.app')

@section('content')
@include('alert')
<div class="card">
    <div class="p-4 p-sm-5">

        <!-- Logo -->
        <div class="d-flex justify-content-center align-items-center mb-4">
            <a href="{!! config('bssystem.cur_apps.home_url') !!}">
                <img src="{{cdn_asset('img/logo-billionairestore.png')}}" alt="" style="width:200px; height:auto">
            </a>
        </div>
        <!-- / Logo -->
        <!-- Form -->
        
            <h5 class="text-center font-weight-bold mb-4">User Not Regsitered</h5>

            <hr class="mt-0 mb-4">

            <p>
                Anda tidak terdaftar di Aplikasi <b>{{ $cur_apps['name'] }}</b>. Jika Anda yakin sudah terdaftar, silahkan hubungi Admin.
            </p>
                        
            <p class="text-center mt-5 mb-0">
                Kembali ke halaman <a href="<?= route('auth.login', ['apps_code' => config('bssystem.cur_apps.apps_code')]); ?>">Login</a>
                
            </p>
            
        
        <!-- / Form -->

    </div>
</div>
@endsection