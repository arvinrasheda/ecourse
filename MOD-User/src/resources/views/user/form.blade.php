@extends('layouts-ecourse.home')
@section('styles')
<link rel="stylesheet" href="{{ asset('/vendor/libs/bootstrap-markdown/bootstrap-markdown.css') }}">

@endsection

@section('scripts')
<!-- Dependencies -->
<script src="{{ asset('/vendor/libs/markdown/markdown.js') }}"></script>
<script src="{{ asset('/vendor/libs/bootstrap-markdown/bootstrap-markdown.js') }}"></script>
<script src="{{ asset('/js/forms_editors.js') }}"></script>
@endsection

@section('content')
<?php echo \Breadcrumb::generate(); ?>
<div class="card mb-4">
    <h6 class="card-header">
        Form
        {{ $mode=='create'?'Tambah Baru':'Edit Data' }}</h6>
    <div class="card-body">
        <form enctype="multipart/form-data" role="form" method="POST" action="{{ $mode=='create'?route('userEcourse.store'):route('userEcourse.update',$data->id) }}">
            {{ csrf_field() }}
            {{ $mode=='edit'?method_field('PUT'):'' }}
            <div class="form-group">
                <label for="input-name">Name</label>
                <input type="text" name="name" id="input-name" class="form-control" value="{{ $data ? $data->name : old('name') }}"
                    required>
            </div>
            <div class="form-group">
                <label for="input-email">Email</label>
                <input type="text" name="email" id="input-email" class="form-control" value="{{ $data ? $data->email : old('email') }}"
                    required>
            </div>
            <div class="form-group">
                    <label for="input-password">Password</label>
                    <input type="password" name="password" id="input-password" class="form-control" required>
                </div>
            <div class="form-group">
                <label for="input-phone">Phone Number</label><br>
                <input type="text" name="phone" id="input-phone" class="form-control" value="{{ $data ? $data->phone : old('phone') }}"
                    required>
            </div>
            <div class="form-group">
                <label for="input-is_admin">Role</label>
                <div class="form-group">
                    <select class="custom-select" name="is_admin" id="input-is_admin">
                        <option value="">Select Role</option>
                        @if ($mode == 'edit')
                        <option value="1" @if ($data->is_admin == 1) selected='selected' @endif>Admin</option>
                        <option value="0" @if ($data->is_admin == 0) selected='selected' @endif>Member</option>
                        @else
                        <option value="1">Admin</option>
                        <option value="0">Member</option>
                        @endif
                    </select>
                </div>
            </div>
            <div class="box-footer">
                <input type="hidden" name="status" value="0">
                <button type="submit" class="btn btn-primary">Save</button>
            </div>
        </form>
    </div>
</div>
@endsection
