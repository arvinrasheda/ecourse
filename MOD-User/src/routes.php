<?php

Route::prefix('userecourse')->group(function(){
    Route::get('/', 'UserController@index')->name('userEcourse.index');
    Route::post('/', 'UserController@store')->name('userEcourse.store');
    Route::get('/create', 'UserController@create')->name('userEcourse.create');
    Route::delete('/{id}', 'UserController@destroy')->name('userEcourse.destroy');
    Route::put('/{id}', 'UserController@update')->name('userEcourse.update');
    Route::get('/{id}', 'UserController@show')->name('userEcourse.show');
    Route::get('/{id}/edit', 'UserController@edit')->name('userEcourse.edit');
});