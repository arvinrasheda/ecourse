<?php

namespace BSSystem\MODUser\Controllers;

use Illuminate\Http\Request;
use BSSystem\MODUser\Repositories\UsersRepository;
use BSSystem\MODUser\Responses\UsersResponse;
use Illuminate\Support\Facades\Auth;

use BSSystem\Core\Base\BaseController;

class UserController extends BaseController
{
    
    private $repo;
    
    public function __construct(UsersRepository $repo)
    {
        \Breadcrumb::add('User',route('userEcourse.index'));
        
        $this->repo = $repo;

        $this->middleware('auth');
    }
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        if(Auth::user()->is_admin == 1){
            $data['data'] = $this->repo->all();
            return new UsersResponse($data,'user.list');
        }else{
            return redirect()->route('dashboard.index');
        }
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        \Breadcrumb::add('Tambah Baru',route('userEcourse.create'));        
        
        $data['mode'] = 'create';
        $data['data'] = [];

        return view('user.form', $data);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $data = $request->all();
        $data['password'] = bcrypt($request->password);
//        $request->validate([
//            'name' => 'required|string|max:255'
//        ]);        
        $this->repo->create($data);
        $this->repo->createProfile($request);
        return redirect()->route('userEcourse.index')->with('alert', ['type' => 'success', 'message' => 'Data inserted successfully']);
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $data['data'] = $this->repo->find($id);
        
        \Breadcrumb::add('View id : <b>'.$data['data']['ig'].'</b>',route('userEcourse.show',$data['data']['id']));
        
        return new UsersResponse($data,'user.detail');
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $data['data'] = $this->repo->find($id);
        
        \Breadcrumb::add('Edit di : <b>'.$data['data']['id'].'</b>',route('userEcourse.edit',$data['data']['id']));
        
        $data['mode'] = 'edit';

        return view('user.form', $data);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request,$id)
    {
        $input = $request->all();
        
//        //pastikan sudah
//        if($request->input('name')){
//            $validate['name'] = 'required|string|max:255';
//        }
//        
//        if(isset($validate)){
//            $request->validate($validate);
//        }
        
        $this->repo->update($input,$id);
        
        return redirect()->route('userEcourse.index')->with('alert', ['type' => 'success', 'message' => 'Data updated successfully']);

    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $this->repo->delete($id);
        return redirect()->route('userEcourse.index')->with('alert', ['type' => 'success', 'message' => 'Data Deleted !']);

    }    
    
}
