<?php

namespace BSSystem\MODUser\Models;

use Illuminate\Database\Eloquent\Model;

class Users extends Model
{
    /**
     * The table associated with the model.
     *
     * @var string
     */
    protected $table = 'users';
    
    /**
     * The attributes that aren't mass assignable.
     *
     * @var array
     */
    protected $guarded = ['id','created_at','updated_at'];

    public function ecourse()
    {
        return $this->belongsToMany('BSSystem\MODEcourse\Models\Ecourse', 'member');
    }

    public function profile()
    {
        return $this->hasOne('BSSystem\LIBProfile\Models\UserProfiles');
    }
}