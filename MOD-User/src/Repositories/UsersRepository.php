<?php

namespace BSSystem\MODUser\Repositories;

use BSSystem\MODUser\Models\Users;

use BSSystem\Core\Base\BaseRepository;

class UsersRepository extends BaseRepository
{
    public function __construct(Users $model)
    {
        $this->model = $model;
    }
}