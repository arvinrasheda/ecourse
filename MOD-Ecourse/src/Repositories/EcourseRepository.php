<?php

namespace BSSystem\MODEcourse\Repositories;

use BSSystem\MODEcourse\Models\Ecourse;
use BSSystem\LIBGroup\Models\Group;

use BSSystem\Core\Base\BaseRepository;

class EcourseRepository extends BaseRepository
{
    public function __construct(Ecourse $model)
    {
        $this->model = $model;
    }
}