<?php

namespace BSSystem\MODEcourse\Controllers;

use Illuminate\Http\Request;
use BSSystem\MODEcourse\Repositories\EcourseRepository;
use BSSystem\MODEcourse\Responses\EcourseResponse;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Session;

use BSSystem\Core\Base\BaseController;

class EcourseController extends BaseController
{
    
    private $repo;
    
    public function __construct(EcourseRepository $repo)
    {        
        \Breadcrumb::add('Ecourse',route('ecourse.index'));
        
        $this->repo = $repo;

        $this->middleware('auth');

    }
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        if(Auth::user()->is_admin == 1){
            $data['data'] = $this->repo->all();
            return new EcourseResponse($data,'ecourse.list');
        }
        else {
            return redirect()->route('dashboard.index');
        }
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        \Breadcrumb::add('Tambah Baru',route('ecourse.create'));        
        
        $data['mode'] = 'create';
        $data['data'] = [];

        return view('ecourse.form', $data);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $data = $request->all();
        $data['thumbnail'] = $request->file('thumbnail')->store('uploads/thumbnail','public');
//        $request->validate([
//            'name' => 'required|string|max:255'
//        ]);        
        $this->repo->create($data);
        Session::flash('success', 'Your Data has successfully imported');
        return redirect()->route('ecourse.index')->with('alert', ['type' => 'success', 'message' => 'Data inserted successfully']);
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $data['data'] = $this->repo->find($id);
        
        \Breadcrumb::add('View id : <b>'.$data['data']['id'].'</b>',route('ecourse.show',$data['data']['id']));
        
        return new EcourseResponse($data,'ecourse.detail');
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $data['data'] = $this->repo->find($id);
        
        \Breadcrumb::add('Edit di : <b>'.$data['data']['id'].'</b>',route('ecourse.edit',$data['data']['id']));
        
        $data['mode'] = 'edit';

        return view('ecourse.form', $data);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request,$id)
    {
        $input = $request->all();
        $input['thumbnail'] = $request->file('thumbnail')->store('uploads/thumbnail','public');
//        //pastikan sudah
//        if($request->input('name')){
//            $validate['name'] = 'required|string|max:255';
//        }
//        
//        if(isset($validate)){
//            $request->validate($validate);
//        }
        
        $this->repo->update($input,$id);
        Session::flash('success', 'Your Data has successfully imported');
        return redirect()->route('ecourse.index')->with('alert', ['type' => 'success', 'message' => 'Data updated successfully']);

    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $this->repo->delete($id);
        return redirect()->route('ecourse.index')->with('alert', ['type' => 'success', 'message' => 'Data Deleted !']);

    }    
    
}
