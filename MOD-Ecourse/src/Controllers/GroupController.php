<?php

namespace BSSystem\MODEcourse\Controllers;

use Illuminate\Http\Request;
use BSSystem\LIBGroup\Repositories\GroupRepository;
use BSSystem\MODEcourse\Responses\GroupResponse;
use Illuminate\Support\Facades\Auth;

use BSSystem\Core\Base\BaseController;

class GroupController extends BaseController
{
    
    private $repo;
    
    public function __construct(GroupRepository $repo)
    {
        \Breadcrumb::add('Group',route('group.index'));
        
        $this->repo = $repo;
        
        $this->middleware('auth');
    }
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        if(Auth::user()->is_admin == 1){
            $data['data'] = $this->repo->all();
            return new GroupResponse($data,'group.list');
        }else{
            return redirect()->route('dashboard.index');
        }
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        \Breadcrumb::add('Tambah Baru',route('group.create'));        
        
        $data['mode'] = 'create';
        $data['data'] = [];
        $data['ecourse'] = $this->repo->getEcourse();

        return view('group.form', $data);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $data = $request->all();
        // $data['thumbnail'] = $request->file('thumbnail')->store('uploads/thumbnail','public');
//        $request->validate([
//            'name' => 'required|string|max:255'
//        ]);        
        $this->repo->create($data);
        return redirect()->route('group.index')->with('alert', ['type' => 'success', 'message' => 'Data inserted successfully']);
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $data['data'] = $this->repo->find($id);
        
        \Breadcrumb::add('View id : <b>'.$data['data']['id'].'</b>',route('group.show',$data['data']['id']));
        
        return new GroupResponse($data,'group.detail');
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $data['data'] = $this->repo->find($id);
        
        \Breadcrumb::add('Edit di : <b>'.$data['data']['id'].'</b>',route('group.edit',$data['data']['id']));
        
        $data['mode'] = 'edit';
        $data['ecourse'] = $this->repo->getEcourse();

        return view('group.form', $data);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request,$id)
    {
        $input = $request->all();
        // $input['thumbnail'] = $request->file('thumbnail')->store('uploads/thumbnail','public');
//        //pastikan sudah
//        if($request->input('name')){
//            $validate['name'] = 'required|string|max:255';
//        }
//        
//        if(isset($validate)){
//            $request->validate($validate);
//        }
        
        $this->repo->update($input,$id);
        
        return redirect()->route('group.index')->with('alert', ['type' => 'success', 'message' => 'Data updated successfully']);

    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $this->repo->delete($id);
        return redirect()->route('group.index')->with('alert', ['type' => 'success', 'message' => 'Data Deleted !']);

    }    
    
}
