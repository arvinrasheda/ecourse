<?php

namespace BSSystem\MODEcourse\Controllers;

use Illuminate\Http\Request;
use BSSystem\LIBModul\Repositories\ModulRepository;
use BSSystem\MODEcourse\Responses\ModulResponse;
use Illuminate\Support\Facades\Session;
use Illuminate\Support\Facades\Auth;

use BSSystem\Core\Base\BaseController;

class ModulController extends BaseController
{
    
    private $repo;
    
    public function __construct(ModulRepository $repo)
    {
        \Breadcrumb::add('Modul',route('modul.index'));
        
        $this->repo = $repo;

        $this->middleware('auth');
    }
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        if(Auth::user()->is_admin == 1){
            $data['data'] = $this->repo->all();
            return new ModulResponse($data,'modul.list');
        }else{
            return redirect()->route('dashboard.index');
        }
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        \Breadcrumb::add('Tambah Baru',route('modul.create'));        
        
        $data['mode'] = 'create';
        $data['materi'] = $this->repo->getMateri();
        $data['data'] = [];

        return view('modul.form', $data);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $data = $request->all();

        if($request->file != 0){
            $data['file'] = $request->file('file')->store('uploads/modul','public');
            //        $request->validate([
            //            'name' => 'required|string|max:255'
            //        ]);   
            
            $this->repo->create($data); 
            Session::flash('success', 'Your Modul has successfully imported');
            return redirect()->route('modul.index');
        }else{
            $link = $request->link;
			$auth = strpos($link, "youtube");
			$auth1 = strpos($link, "youtu.be");

			$yt_id = substr($link, -11);

			if($auth == 0 && $auth1 == 0 ){
                Session::flash('error', 'Gagal mengupload link. Link yang ditambahkan harus merupakan link youtube (format link : www.youtube.com/). silakan masukkan kembali link anda');
                return back();
			}else{
                $data['link'] = $yt_id;
                $this->repo->create($data);
                Session::flash('success', 'Your Data has successfully imported');
                return redirect()->route('modul.index');
            }
        }
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $data['data'] = $this->repo->find($id);
        
        \Breadcrumb::add('View id : <b>'.$data['data']['id'].'</b>',route('modul.show',$data['data']['id']));
        
        return new ModulResponse($data,'modul.detail');
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $data['data'] = $this->repo->find($id);
        
        \Breadcrumb::add('Edit di : <b>'.$data['data']['id'].'</b>',route('modul.edit',$data['data']['id']));
        $data['materi'] = $this->repo->getMateri();
        $data['mode'] = 'edit';

        return view('modul.edit', $data);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request,$id)
    {
        $input = $request->all();
        $input['file'] = $request->file('file')->store('uploads/modul','public');
//        //pastikan sudah
//        if($request->input('name')){
//            $validate['name'] = 'required|string|max:255';
//        }
//        
//        if(isset($validate)){
//            $request->validate($validate);
//        }
        
        $this->repo->update($input,$id);
        
        return redirect()->route('modul.index')->with('alert', ['type' => 'success', 'message' => 'Data updated successfully']);

    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $this->repo->delete($id);
        return redirect()->route('modul.index')->with('alert', ['type' => 'success', 'message' => 'Data Deleted !']);

    }    
    
}
