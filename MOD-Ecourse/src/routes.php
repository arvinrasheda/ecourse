<?php

Route::prefix('ecourse')->group(function(){
    Route::get('/', 'EcourseController@index')->name('ecourse.index');
    Route::post('/', 'EcourseController@store')->name('ecourse.store');
    Route::get('/create', 'EcourseController@create')->name('ecourse.create');
    Route::delete('/{id}', 'EcourseController@destroy')->name('ecourse.destroy');
    Route::put('/{id}', 'EcourseController@update')->name('ecourse.update');
    Route::get('/{id}', 'EcourseController@show')->name('ecourse.show');
    Route::get('/{id}/edit', 'EcourseController@edit')->name('ecourse.edit');
});

Route::prefix('materi')->group(function(){
    Route::get('/', 'MateriController@index')->name('materi.index');
    Route::post('/', 'MateriController@store')->name('materi.store');
    Route::get('/create', 'MateriController@create')->name('materi.create');
    Route::delete('/{id}', 'MateriController@destroy')->name('materi.destroy');
    Route::put('/{id}', 'MateriController@update')->name('materi.update');
    Route::get('/{id}', 'MateriController@show')->name('materi.show');
    Route::get('/{id}/edit', 'MateriController@edit')->name('materi.edit');
});

Route::prefix('group')->group(function(){
    Route::get('/', 'GroupController@index')->name('group.index');
    Route::post('/', 'GroupController@store')->name('group.store');
    Route::get('/create', 'GroupController@create')->name('group.create');
    Route::delete('/{id}', 'GroupController@destroy')->name('group.destroy');
    Route::put('/{id}', 'GroupController@update')->name('group.update');
    Route::get('/{id}', 'GroupController@show')->name('group.show');
    Route::get('/{id}/edit', 'GroupController@edit')->name('group.edit');
});

Route::prefix('modul')->group(function(){
    Route::get('/', 'ModulController@index')->name('modul.index');
    Route::post('/', 'ModulController@store')->name('modul.store');
    Route::get('/create', 'ModulController@create')->name('modul.create');
    Route::delete('/{id}', 'ModulController@destroy')->name('modul.destroy');
    Route::put('/{id}', 'ModulController@update')->name('modul.update');
    Route::get('/{id}', 'ModulController@show')->name('modul.show');
    Route::get('/{id}/edit', 'ModulController@edit')->name('modul.edit');
});