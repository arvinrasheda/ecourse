<?php

namespace BSSystem\MODEcourse\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\Storage;


class Ecourse extends Model
{
    /**
     * The table associated with the model.
     *
     * @var string
     */
    protected $table = 'ecourse';
    
    /**
     * The attributes that aren't mass assignable.
     *
     * @var array
     */
    protected $guarded = ['id','created_at','updated_at'];

    public function getThumbnailAttribute()
    {
        return Storage::url($this->thumbnail);
    }

    public function materi()
    {
        return $this->hasMany('BSSystem\LIBMateri\Models\Materi');
    }
    public function group()
    {
        return $this->hasMany('BSSystem\LIBGroup\Models\Group');
    }
    public function user()
    {
        return $this->belongsToMany('BSSystem\MODUser\Models\Users', 'member');
    }
}