@extends('layouts-ecourse.home')
@section('styles')
<link rel="stylesheet" href="{{ asset('/vendor/libs/bootstrap-markdown/bootstrap-markdown.css') }}">

@endsection

@section('scripts')
<!-- Dependencies -->
<script src="{{ asset('/vendor/libs/markdown/markdown.js') }}"></script>
<script src="{{ asset('/vendor/libs/bootstrap-markdown/bootstrap-markdown.js') }}"></script>
<script src="{{ asset('/js/forms_editors.js') }}"></script>
@endsection

@section('content')
<?php echo \Breadcrumb::generate(); ?>
<div class="card mb-4">
    <h6 class="card-header">
        Form
        {{ $mode=='create'?'Tambah Baru':'Edit Data' }}</h6>
    <div class="card-body">
        <form enctype="multipart/form-data" role="form" method="POST" action="{{ $mode=='create'?route('group.store'):route('group.update',$data->id) }}">
            {{ csrf_field() }}
            {{ $mode=='edit'?method_field('PUT'):'' }}
            <div class="form-group">
                <label for="input-tittle">Tittle</label>
                <input type="text" name="tittle" id="input-tittle" class="form-control" value="{{ $data ? $data->tittle : old('tittle') }}"
                    required>
            </div>
            <div class="form-group">
                <label for="input-facebook">Facebook</label>
                <input type="text" name="facebook" id="input-facebook" class="form-control" value="{{ $data ? $data->facebook : old('facebook') }}"
                    required>
            </div>
            <div class="form-group">
                <label for="input-telegram">Telegram</label>
                <input type="text" name="telegram" id="input-telegram" class="form-control" value="{{ $data ? $data->telegram : old('telegram') }}"
                    required>
            </div>
            <div class="form-group">
                <label for="input-ecourse_id">Ecourse</label>
                <div class="form-group">
                    <select class="custom-select" name="ecourse_id" id="input-ecourse_id">
                        <option value="">Select Ecourse</option>
                        @if ($mode=='edit')
                        @foreach ($ecourse as $item)
                        <option value="{{ $item->id }}" @if ($data->ecourse->id == $item->id) selected='selected'
                            @endif>{{ $item->tittle }}</option>
                        @endforeach
                        @else
                        @foreach ($ecourse as $item)
                        <option value="{{ $item->id }}">{{ $item->tittle }}</option>
                        @endforeach
                        @endif
                    </select>
                </div>
            </div>
            <div class="box-footer">
                <button type="submit" class="btn btn-primary">Save</button>
            </div>
        </form>
    </div>
</div>
@endsection
