@extends('layouts-ecourse.home')
@section('styles')
<link rel="stylesheet" href="{{ asset('/vendor/libs/bootstrap-markdown/bootstrap-markdown.css') }}">

@endsection

@section('scripts')
<!-- Dependencies -->
<script src="{{ asset('/vendor/libs/markdown/markdown.js') }}"></script>
<script src="{{ asset('/vendor/libs/bootstrap-markdown/bootstrap-markdown.js') }}"></script>
<script src="{{ asset('/js/forms_editors.js') }}"></script>
@endsection

@section('content')
<?php echo \Breadcrumb::generate(); ?>

@if ($message = Session::get('success'))
<div class="alert alert-success alert-block">
    <button type="button" class="close" data-dismiss="alert">×</button>
    <strong>{{ $message }}</strong>
</div>
@endif


@if ($message = Session::get('update'))
<div class="alert alert-success alert-block">
    <button type="button" class="close" data-dismiss="alert">×</button>
    <strong>{{ $message }}</strong>
</div>
@endif

@if ($message = Session::get('error'))
<div class="alert alert-warning alert-block">
    <button type="button" class="close" data-dismiss="alert">×</button>
    <strong>{{ $message }}</strong>
</div>
@endif

@if ($message = Session::get('delete'))
<div class="alert alert-warning alert-block">
    <button type="button" class="close" data-dismiss="alert">×</button>
    <strong>{{ $message }}</strong>
</div>
@endif

<div class="nav-tabs-top">
    <ul class="nav nav-tabs">
        <li class="nav-item">
            <a class="nav-link active" data-toggle="tab" href="#navs-top-pdf">PDF</a>
        </li>
        <li class="nav-item">
            <a class="nav-link" data-toggle="tab" href="#navs-top-video">Video</a>
        </li>
    </ul>
    <div class="tab-content">
        <div class="tab-pane fade show active" id="navs-top-pdf">
            <div class="card-body">
                <div class="card mb-4">
                    <h6 class="card-header">
                        Form
                        {{ $mode=='create'?'Tambah Baru':'Edit Data' }}</h6>
                    <div class="card-body">
                        <form enctype="multipart/form-data" role="form" method="POST" action="{{ $mode=='create'?route('modul.store'):route('modul.update',$data->id) }}">
                            {{ csrf_field() }}
                            {{ $mode=='edit'?method_field('PUT'):'' }}
                            <div class="form-group">
                                <label for="input-tittle">Tittle</label>
                                <input type="text" name="tittle" id="input-tittle" class="form-control" value="{{ $data ? $data->tittle : old('tittle') }}"
                                    required>
                            </div>
                            <div class="form-group">
                                <label for="input-file">File</label><br>
                                <input type="file" name="file" id="input-file">
                                <small class="form-text text-muted">Harap Masukan Thumbnail Yang Ingin Digunakan.</small>
                            </div>
                            <div class="form-group">
                                <label for="input-materi_id">Materi</label>
                                <div class="form-group">
                                    <select class="custom-select" name="materi_id" id="input-materi_id">
                                        <option value="">Select Materi</option>
                                        @if ($mode=='edit')
                                        @foreach ($materi as $item)
                                        <option value="{{ $item->id }}" @if ($data->materi->id == $item->id)
                                            selected='selected'
                                            @endif>{{ $item->tittle }}</option>
                                        @endforeach
                                        @else
                                        @foreach ($materi as $item)
                                        <option value="{{ $item->id }}">{{ $item->tittle }}</option>
                                        @endforeach
                                        @endif
                                    </select>
                                </div>
                            </div>
                            <div class="box-footer">
                                <input type="hidden" name="link" value="0">
                                <button type="submit" class="btn btn-primary">Save</button>
                            </div>
                        </form>
                    </div>
                </div>
            </div>
        </div>
        <div class="tab-pane fade" id="navs-top-video">
            <div class="card-body">
                <div class="card mb-4">
                    <h6 class="card-header">
                        Form
                        {{ $mode=='create'?'Tambah Baru':'Edit Data' }}</h6>
                    <div class="card-body">
                        <form enctype="multipart/form-data" role="form" method="POST" action="{{ $mode=='create'?route('modul.store'):route('modul.update',$data->id) }}">
                            {{ csrf_field() }}
                            {{ $mode=='edit'?method_field('PUT'):'' }}
                            <div class="form-group">
                                <label for="input-tittle">Tittle</label>
                                <input type="text" name="tittle" id="input-tittle" class="form-control" value="{{ $data ? $data->tittle : old('tittle') }}"
                                    required>
                            </div>
                            <div class="form-group">
                                <label for="input-link">Link</label>
                                <input type="text" name="link" id="input-link" class="form-control" value="{{ $data ? $data->link : old('link') }}"
                                    required>
                                <small class="form-text text-muted">Harap Masukan Link Format https://www.youtube.com </small>
                            </div>
                            <div class="form-group">
                                <label for="input-materi_id">Materi</label>
                                <div class="form-group">
                                    <select class="custom-select" name="materi_id" id="input-materi_id">
                                        <option value="">Select Materi</option>
                                        @if ($mode=='edit')
                                        @foreach ($materi as $item)
                                        <option value="{{ $item->id }}" @if ($data->materi->id == $item->id)
                                            selected='selected'
                                            @endif>{{ $item->tittle }}</option>
                                        @endforeach
                                        @else
                                        @foreach ($materi as $item)
                                        <option value="{{ $item->id }}">{{ $item->tittle }}</option>
                                        @endforeach
                                        @endif
                                    </select>
                                </div>
                            </div>
                            <div class="box-footer">
                                <input type="hidden" name="file" value="0">
                                <button type="submit" class="btn btn-primary">Save</button>
                            </div>
                        </form>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
@endsection
