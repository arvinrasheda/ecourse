@extends('layouts-ecourse.home')
@section('styles')
<link rel="stylesheet" href="{{ asset('/vendor/libs/bootstrap-markdown/bootstrap-markdown.css') }}">

@endsection

@section('scripts')
<!-- Dependencies -->
<script src="{{ asset('/vendor/libs/markdown/markdown.js') }}"></script>
<script src="{{ asset('/vendor/libs/bootstrap-markdown/bootstrap-markdown.js') }}"></script>
<script src="{{ asset('/js/forms_editors.js') }}"></script>
@endsection

@section('content')
<?php echo \Breadcrumb::generate(); ?>
<div class="card mb-4">
    <h6 class="card-header">
        Form
        {{ $mode=='create'?'Tambah Baru':'Edit Data' }}</h6>
    <div class="card-body">
        <form enctype="multipart/form-data" role="form" method="POST" action="{{ $mode=='create'?route('ecourse.store'):route('ecourse.update',$data->id) }}">
            {{ csrf_field() }}
            {{ $mode=='edit'?method_field('PUT'):'' }}
            <div class="form-group">
                <label for="input-tittle">Tittle</label>
                <input type="text" name="tittle" id="input-tittle" class="form-control" value="{{ $data ? $data->tittle : old('tittle') }}"
                    required>
            </div>
            <div class="form-group">
                <label for="input-price">Price</label>
                <input type="text" name="price" id="input-price" class="form-control" value="{{ $data ? $data->price : old('price') }}"
                    required>
            </div>
            <div class="form-group">
                <label for="input-facebook">Link Facebook</label>
                <input type="text" name="facebook" id="input-facebook" class="form-control" value="{{ $data ? $data->facebook : old('facebook') }}"
                    required>
            </div>
            <div class="form-group">
                <label for="input-telegram">Link Telegram</label>
                <input type="text" name="telegram" id="input-telegram" class="form-control" value="{{ $data ? $data->telegram : old('telegram') }}"
                    required>
            </div>
            <div class="form-group">
                <label for="input-description">Description</label>
                <textarea name="description" id="input-description" class="form-control">{{ $data ? $data->description : old('description') }}</textarea>
            </div>
            <div class="form-group">
                <label for="input-thumbnail">Thumbnail</label><br>
                <input type="file" name="thumbnail" id="input-thumbnail">
                <small class="form-text text-muted">Harap Masukan Thumbnail Yang Ingin Digunakan.</small>
            </div>
            <div class="box-footer">
                <button type="submit" class="btn btn-primary">Save</button>
            </div>
        </form>
    </div>
</div>
@endsection
