@extends('layouts-ecourse.home')


@section('styles')
<link rel="stylesheet" href="{{ asset('/vendor/libs/datatables/datatables.css') }}">
@endsection

@section('scripts')
<!-- Dependencies -->
<script src="{{ asset('/vendor/libs/datatables/datatables.js') }}"></script>
<script src="{{ asset('/js/tables_datatables.js') }}"></script>
@endsection

@section('content')
<h4 class="d-flex justify-content-between align-items-center w-100 font-weight-bold py-3 mb-4">
    <div>
        <?php echo \Breadcrumb::generate(); ?>
    </div>
    <a href="{{ route('ecourse.create') }}" class="btn btn-primary btn-round d-block"><span class="ion ion-md-add"></span>&nbsp;
        Create Ecourse</a>
</h4>
@if ($message = Session::get('success'))
<div class="alert alert-success alert-block">
    <button type="button" class="close" data-dismiss="alert">×</button>
    <strong>{{ $message }}</strong>
</div>
@endif

@if ($message = Session::get('update'))
<div class="alert alert-success alert-block">
    <button type="button" class="close" data-dismiss="alert">×</button>
    <strong>{{ $message }}</strong>
</div>
@endif

@if ($message = Session::get('delete'))
<div class="alert alert-warning alert-block">
    <button type="button" class="close" data-dismiss="alert">×</button>
    <strong>{{ $message }}</strong>
</div>
@endif

<div class="card">
    <div class="card-datatable table-responsive">
        <table class="datatables-demo table table-striped table-bordered">
            <thead>
                <tr>
                    <th width="20">No</th>
                    <th width="70">Tittle</th>
                    <th width="70">Price</th>
                    <th>Facebook</th>
                    <th>Telegram</th>
                    <th width="70">Published</th>
                    <th width="70">Action</th>
                </tr>
            </thead>
            <tbody>
                @php(
                $no = 1 {{-- buat nomor urut --}}
                )
                @foreach($data as $item)
                <tr class="odd">
                    <td>{{ $no++ }}</td>
                    <td>{{($item->tittle)}}</td>
                    <td>{{($item->price)}}</td>
                    <td>{{ $item->facebook }}</td>
                    <td>{{ $item->telegram }}</td>
                    <td>{{ $item->created_at->format('d M Y') }}</td>
                    <td>
                        <form action="{{ route('ecourse.destroy',$item->id) }}" class="delete-form{{$loop->iteration}}"
                            method="POST">
                            {{ csrf_field() }}
                            {{ method_field('DELETE') }}
                            <a href="{{ route('ecourse.edit', $item->id) }}" class="btn btn-default btn-xs icon-btn md-btn-flat article-tooltip"
                                data-toggle="tooltip" title data-original-title="Edit"><i class="ion ion-md-create"></i></a>
                            <a data-toggle="tooltip" class="btn btn-default btn-xs icon-btn md-btn-flat article-tooltip delete-button"
                                data-id="{{ $loop->iteration }}" id="delete{{ $loop->iteration }}" title
                                data-original-title="Delete">
                                <i class="ion ion-md-trash"></i></a>
                        </form>
                    </td>
                </tr>
                @endforeach
            </tbody>
        </table>
    </div>
</div>
<script>
    $(document).ready(function () {
        $('.delete-button').on('click', function () {
            if (confirm('Yakin Hapus Ecourse Ini ?')) {
                $('.delete-form' + $(this).data('id')).submit();
            }
        });
    });
</script>
@endsection
