<?php

namespace BSSystem\LIBModul\Repositories;

use BSSystem\LIBModul\Models\Modul;
use BSSystem\LIBMateri\Models\Materi;

use BSSystem\Core\Base\BaseRepository;

class ModulRepository extends BaseRepository
{
    public function __construct(Modul $model)
    {
        $this->model = $model;
    }

    public function getMateri(){
        return Materi::all();
    }
}