<?php

namespace BSSystem\LIBModul\Models;

use Illuminate\Database\Eloquent\Model;

class Modul extends Model
{
    /**
     * The table associated with the model.
     *
     * @var string
     */
    protected $table = 'modul';
    
    /**
     * The attributes that aren't mass assignable.
     *
     * @var array
     */
    protected $guarded = ['id','created_at','updated_at'];

    public function materi()
    {
        return $this->belongsTo('BSSystem\LIBMateri\Models\Materi');
    }

}